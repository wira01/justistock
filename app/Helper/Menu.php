<?php 

namespace App\Helper;

class Menu{	
	public static function buildTree($table,$p_id=null) {
      $tree = array();
      foreach($table as $row){ 

          if($row->parent_id == $p_id){          	
              $_this = new self;
              $tmp = $_this->buildTree($table,$row->id);              
              if($tmp){
                  $row->children=$tmp;
              }else{
                  $row->leaf = true;
              }
              $tree[]=$row;
          }          
      }
      return $tree;
  }

}

