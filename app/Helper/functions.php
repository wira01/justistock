<?php 

use Illuminate\Support\Str;

use App\Models\Master\Item;
use App\Models\Transaction\LembarKerja;
use App\Models\Transaction\DetailLembarKerja;
use Illuminate\Support\Facades\DB;


	// for helper function

	/**
		 * Format string to rupiah format
		 *
		 *
		 * @param string|int $money
		 * @return string
		 */
		function formatRupiah($money){
			return "Rp ".number_format($money, 2,',','.');
		}

		function getNoPesanan(){
			$date=strval(now()->format('Ymd'));			
			$random = strtoupper(Str::random(5));			
			return $date.$random;
			
		}

		function getQtyByModel($model,$size){
			$item = Item::withSum('detail','qty')->where('model_id',$model)->whereHas('detail',function($query)use($size){
				$query->where('size_id',$size);
			})->groupBy('model_id')->first();
			return $item->detail_sum_qty;
		}

		function gotQuantity($marketplace,$tanggal,$type = 'jual'){
			$detail = DetailLembarKerja::select('qty','lembar_kerja_id');
			$lembar_kerja = DB::table('lembar_kerjas')->select(DB::Raw('SUM(qty) as sum_qty'))->joinSub($detail,'detail',function($join){
				$join->on('lembar_kerjas.id','=','detail.lembar_kerja_id');
			})->whereDate('tanggal',$tanggal)->where('marketplace_id',$marketplace)->where('keterangan',$type)->groupBy('lembar_kerjas.marketplace_id');

			return $lembar_kerja->first();
		}