<?php

namespace App\Http\Livewire\Layout;

use Livewire\Component;
use App\Models\Master\Menu;
use App\Helper\Menu as HelperMenu;
use Auth;
use Cache;
use DB;
use Route;


class Sidebar extends Component
{    

  public $menus, $menurole, $test;
  public $mmenu = "dashboard", $submenu="", $submenu2="";

    public function mount(){
        $id_role = Auth::user()->role->id; 
        Cache::flush();
        $this->menurole = Cache::remember('menurole'.$id_role, 60*60*24, function () use ($id_role) {            
            return DB::table('menus')
                    ->join('permissions', 'menus.id', '=', 'permissions.menu_id')
                    ->join('role_permissions', 'permissions.id', '=', 'role_permissions.permission_id')
                    ->join('roles', 'roles.id', '=', 'role_permissions.role_id')
                    ->where('roles.id', $id_role)
                    ->where('permissions.permission', 'access')
                    ->select('menus.*')
                    ->orderBy('menus.index', 'asc')                    
                    ->get();
        });        

        $this->menus = HelperMenu::buildTree($this->menurole);

    $route = Route::current();
    $route = explode("/", $route->uri);    

    if (count($route) < 2) {
      $this->submenu = "";
      $this->mmenu = $route[0];
    }elseif (count($route) < 3) {        
      $this->submenu = $route[1];
      $this->mmenu = $route[0];
    }else {
      $this->submenu2 = $route[2];
      $this->submenu = $route[1];
      $this->mmenu = $route[0];
    }

        

    }
    public function render()
    {
        return view('livewire.layout.sidebar');
    }
}
