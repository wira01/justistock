<?php

namespace App\Http\Livewire\Master\Customer;

use App\Http\Livewire\Extender\Ancestor;

use App\Models\Master\Customer;
use Symfony\Component\HttpFoundation\Response;
use Gate;

class Index extends Ancestor
{

    public function destroy(Customer $customer){
        $customer->delete();   
        $this->emit('flash_message', 'Data has been deleted!');
    }

    public function mount(){
        abort_if(Gate::denies('access_menu_customer'), Response::HTTP_FORBIDDEN, 'You Not Authorized');
              
    }

    public function render()
    {
        $customers = Customer::orderByDesc('id');
        if($this->q){            
            $customers = $customers->where('name','like','%'.$this->q.'%');  
        }
        $customers = $customers->paginate(10);
        return view('livewire.master.customer.index',[
            'customers'=>$customers
        ]);
    }
}
