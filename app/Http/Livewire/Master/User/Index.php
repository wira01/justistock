<?php

namespace App\Http\Livewire\Master\User;


use App\Http\Livewire\Extender\Ancestor;

use App\Models\User;
use App\Models\Master\Role;

use Symfony\Component\HttpFoundation\Response;
use Gate;


class Index extends Ancestor
{
    public $name,$username,$role,$email,$password,$password_confirmation;
    public $idUser = '';

    public function store(){        
        
        if(!$this->idUser){
            $this->validate([
            'name'=>'required',
            'username'=>'required|unique:users',            
            'role'=>'required',
            'password'=>'required|confirmed'
        ]);

            User::create([
            'name'=>$this->name,
            'username'=>$this->username,
            'email'=>$this->email??'',
            'role_id'=>$this->role,
            'password'=>bcrypt($this->password)
        ]);
        }else{
            $data = [
                'name'=>$this->name,
                'username'=>$this->username,
                'email'=>$this->email??'',
                'role_id'=>$this->role,            
            ];
        if($this->password){
            $data['password']=bcrypt($this->password);
        }
             $this->validate([
                'name'=>'required',
                'username'=>'required',            
                'role'=>'required',
            ]);

            User::find($this->idUser)->update($data);
        }            
        $this->showCard = 'table';
        $this->resetData();        
        $this->emit('flash_message', 'Data has been saved!');
    }

    public function getData($id){        
        $this->idUser = $id;
        $user = User::find($this->idUser);
        if($user){
            $this->name = $user->name;
            $this->username = $user->username;
            $this->email = $user->email;
            $this->role =$user->role_id;        
        }
        
    }

    public function resetData(){
        $this->reset();
    }

    public function mount(){
        abort_if(Gate::denies('access_menu_users'), Response::HTTP_FORBIDDEN, 'You Not Authorized');
    }

    public function render()
    {

        $users = User::orderByDesc('id');
        if($this->q){            
            $users = $users->where(function($query){
                $query->orWhere('name','like','%'.$this->q.'%')
                ->orWhere('username','like','%'.$this->q.'%')
                ->orWhere('email','like','%'.$this->q.'%');
            });  
        }

        $roles = Role::pluck('title', 'id');

        $users = $users->paginate(10);
                
        return view('livewire.master.user.index',[
            'users'=>$users,
            'roles'=>$roles
        ]);
    }
}
