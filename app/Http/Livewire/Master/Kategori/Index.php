<?php

namespace App\Http\Livewire\Master\Kategori;

use App\Http\Livewire\Extender\Ancestor;


use App\Models\Master\Category;
use Symfony\Component\HttpFoundation\Response;
use Gate;
use Auth;

class Index extends Ancestor
{
    public $name;
    public $idCategory = '';

    public function store(){        
        
        $name = Auth::user()->username;
        $this->validate([
            'name'=>'required',
        ]);

        if(!$this->idCategory){
            Category::create([
            'name'=>$this->name,                        
            'created_by'=>$name,           
        ]);    
        }else{
            Category::find($this->idCategory)->update([
                'name'=>$this->name,                 
                'updated_by'=>$name,   
            ]);
        }

        $this->showCard = 'table';
        $this->resetData();        
        $this->emit('flash_message', 'Data has been saved!');
    }

    public function getData($id){        
        $this->idCategory = $id;
        $category = Category::find($this->idCategory);
        if($category){
            $this->name = $category->name;     
        }
        
    }

    public function mount(){
        abort_if(Gate::denies('access_menu_categories'), Response::HTTP_FORBIDDEN, 'You Not Authorized');
    }

    public function resetData(){
        $this->reset();
    }

    public function destroy(Category $category){
        $category->delete();   
        $this->emit('flash_message', 'Data has been deleted!');     
    }

    public function render()
    {

        $categories = Category::orderByDesc('id');
        if($this->q){            
            $categories = $categories->where(function($query){
                $query->orWhere('name','like','%'.$this->q.'%');                
            });  
        }

        $categories = $categories->paginate(10);

        return view('livewire.master.kategori.index',[
            'categories'=>$categories
        ]);
    }
}
