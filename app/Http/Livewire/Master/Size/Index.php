<?php

namespace App\Http\Livewire\Master\Size;

use App\Http\Livewire\Extender\Ancestor;


use App\Models\Master\Size;
use Symfony\Component\HttpFoundation\Response;
use Gate;
use Auth;

class Index extends Ancestor
{
    public $name,$index;
    public $idSize = '';

    public function store(){        
        
        $name = Auth::user()->username;
        $this->validate([
            'name'=>'required',
        ]);

        if(!$this->idSize){
            Size::create([
            'name'=>$this->name,            
            'index'=>$this->index??1, 
            'created_by'=>$name,           
        ]);    
        }else{
            Size::find($this->idSize)->update([
                'name'=>$this->name,
                'index'=>$this->index??1, 
                'updated_by'=>$name,   
            ]);
        }

        $this->showCard = 'table';
        $this->resetData();        
        $this->emit('flash_message', 'Data has been saved!');
    }

    public function getData($id){        
        $this->idSize = $id;
        $size = Size::find($this->idSize);
        if($size){
            $this->name = $size->name;
            $this->index = $size->index;        
        }
        
    }

    public function mount(){
        abort_if(Gate::denies('access_menu_sizes'), Response::HTTP_FORBIDDEN, 'You Not Authorized');
    }

    public function resetData(){
        $this->reset();
    }

    public function destroy(Size $size){
        $size->delete();   
        $this->emit('flash_message', 'Data has been deleted!');     
    }

    public function render()
    {
         $sizes = Size::orderBy('index');
        if($this->q){            
            $sizes = $sizes->where(function($query){
                $query->orWhere('name','like','%'.$this->q.'%');                
            });  
        }

        $sizes = $sizes->paginate(10);
        return view('livewire.master.size.index',[
            'sizes'=>$sizes
        ]);
    }
}
