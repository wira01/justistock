<?php

namespace App\Http\Livewire\Master\Role;

use Livewire\Component;
use App\Http\Livewire\Extender\Ancestor;

use App\Models\Master\Role;
use App\Models\Master\Permission;

use Symfony\Component\HttpFoundation\Response;
use Gate;
use Auth;


class Index extends Ancestor
{

    public $idRole = '';
    public $name,$permission;

    public function store()
    {

        $name = Auth::user()->username;

        $this->validate([
            'name'=>'required'
        ]);

        if(!$this->idRole){
            $role = Role::create([
            'title'=>$this->name??'',
            'created_by'=>$name,            
        ]);    
            $role->permissions()->sync($this->permission);
        }else{
            $role = Role::find($this->idRole);
            
            if($role){
                $role->update([
                'title'=>$this->name??'',                
                'updated_by'=>$name,
            ]);
                $role->permissions()->sync($this->permission);    
            }
            
        }        
        $this->showCard = 'table';
        $this->resetData();        
        $this->emit('flash_message', 'Data has been saved!');

    }


    public function getData($id){        
        $this->idRole = $id;
        $data = Role::find($this->idRole);
        if($data){
            $this->name = $data->title;
            $this->permission = array();            
        foreach ($data->permissions as $pms) {
              array_push($this->permission, $pms->id);
          }

          
        }
        
    }

    public function resetData(){
        $this->reset();
    }

    public function destroy(Role $role){
        $role->delete();   
        $this->emit('flash_message', 'Data has been deleted!');     
    }

    public function mount(){
        abort_if(Gate::denies('access_menu_roles'), Response::HTTP_FORBIDDEN, 'You Not Authorized');
    }


    public function render()
    {     

        $roles = Role::orderByDesc('id');
        if($this->q){            
            $roles = $roles->where('title','like','%'.$this->q.'%');  
        }

        $permissions = Permission::pluck('name', 'id');

        $roles = $roles->paginate(10);
        
        return view('livewire.master.role.index',[
            'roles'=>$roles,
            'permissions'=>$permissions
        ]);
    }
}
