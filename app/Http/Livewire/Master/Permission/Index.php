<?php

namespace App\Http\Livewire\Master\Permission;

use Livewire\Component;
use App\Http\Livewire\Extender\Ancestor;

use App\Models\Master\Permission;
use App\Models\Master\Menu;
use Symfony\Component\HttpFoundation\Response;
use Gate;

use Auth;

class Index extends Ancestor
{
    public $permission = [], $name ,$menu_id;
    public $idPermission = '';

    public function store(){
        $name = Auth::user()->username;
        $this->validate([
            'permission'=>'required',
            'name'=>'required',                        
        ]);



        if(!$this->idPermission){
            foreach($this->permission as $permission){
                Permission::create([
                'name'=>$permission.'_'.$this->setSlug($this->name)??'',
                'permission'=>$permission,
                'menu_id'=>$this->menu_id,
                'created_by'=>$name,
        ]);        
            }
            
        }else{
            Permission::find($this->idPermission)->update([
                'name'=>$this->name??'',
                'menu_id'=>$this->menu_id,                                        
                'updated_by'=>$name,
            ]);
        }
        $this->showCard = 'table';
        $this->resetData();        
        $this->emit('flash_message', 'Data has been saved!');
        
    }

    public function resetData(){
        $this->reset();
    }

    public function getData($id){        
        $this->idPermission = $id;
        $data = Permission::find($this->idPermission);
        $this->name = $data->name;
        $this->permission = $data->permission;
        $this->menu_id = $data->menu_id;
        
        if($this->menu_id){            
            $menu = Menu::where('id',$this->menu_id)->select('id','name')->first();
            $this->emit('getMenu',$menu);            
        }

    }

     public function destroy(Permission $permission){
        $permission->delete();   
        $this->emit('flash_message', 'Data has been deleted!');     
    }

    public function mount(){
        abort_if(Gate::denies('access_menu_permission'), Response::HTTP_FORBIDDEN, 'You Not Authorized');
    }

    public function render()
    {
    
        $permissions = Permission::orderByDesc('id');

        if($this->q){            
            $permissions->where('name','like','%'.$this->q.'%');  
        }

        $permissions = $permissions->paginate(10);

        return view('livewire.master.permission.index',[
            'permissions'=>$permissions
        ]);
    }
}
