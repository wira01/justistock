<?php

namespace App\Http\Livewire\Master\Warehouse;

use App\Http\Livewire\Extender\Ancestor;

use App\Models\Master\Warehouse;
use Symfony\Component\HttpFoundation\Response;
use Gate;
use Auth;

class Index extends Ancestor
{
    public $name,$rack;
    public $idWarehouse = '';

    public function store(){        
        
        $name = Auth::user()->username;

        $this->validate([
            'name'=>'required',
        ]);
        $rack = explode(',', $this->rack);
        

        if(!$this->idWarehouse){
            Warehouse::create([
            'name'=>$this->name,            
            'detail_warehouse'=>json_encode($rack), 
            'created_by'=>$name,           
        ]);    
        }else{
            Warehouse::find($this->idWarehouse)->update([
                'name'=>$this->name,
                'detail_warehouse'=>json_encode($rack), 
                'updated_by'=>$name,   
            ]);
        }

        $this->showCard = 'table';
        $this->resetData();        
        $this->emit('flash_message', 'Data has been saved!');
    }

    public function getData($id){        
        $this->idWarehouse = $id;
        $warehouse = Warehouse::find($this->idWarehouse);
        $racks = implode(',', json_decode($warehouse->detail_warehouse));

        if($warehouse){
            $this->name = $warehouse->name;
            $this->rack = $racks;
        }
        
    }

    public function resetData(){
        $this->reset();
    }

    public function mount(){
        abort_if(Gate::denies('access_menu_warehouse'), Response::HTTP_FORBIDDEN, 'You Not Authorized');
    }

    public function destroy(Warehouse $warehouse){
        $warehouse->delete();   
        $this->emit('flash_message', 'Data has been deleted!');     
    }


    public function render()
    {
         $warehouses = Warehouse::orderByDesc('id');

        if($this->q){            
            $warehouses = $warehouses->where(function($query){
                $query->orWhere('name','like','%'.$this->q.'%');                
                $query->orWhere('detail_warehouse','like','%'.$this->q.'%');                
            });  
        }

        $warehouses = $warehouses->paginate(10);

        return view('livewire.master.warehouse.index',[
            'warehouses'=>$warehouses
        ]);
    }
}
