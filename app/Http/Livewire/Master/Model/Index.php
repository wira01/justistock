<?php

namespace App\Http\Livewire\Master\Model;

use App\Http\Livewire\Extender\Ancestor;


use App\Models\Master\Models;
use App\Models\Master\Category;
use Symfony\Component\HttpFoundation\Response;
use Gate;
use Auth;

class Index extends Ancestor
{
    public $name,$category_id,$category;
    public $idModel = '';

    public function store(){        
        
        $name = Auth::user()->username;
        $this->validate([
            'name'=>'required',
        ]);

        if(!$this->idModel){
            Models::create([
            'name'=>$this->name,                        
            'category_id'=>$this->category_id,                        
            'created_by'=>$name,           
        ]);    
        }else{
            Models::find($this->idModel)->update([
                'name'=>$this->name,   
                'category_id'=>$this->category_id,                
                'updated_by'=>$name,   
            ]);
        }

        $this->showCard = 'table';
        $this->resetData();        
        $this->emit('flash_message', 'Data has been saved!');
    }

    public function getData($id){        
        $this->idModel = $id;
        $model = Models::find($this->idModel);
        if($model){
            $this->name = $model->name;     
            $this->category_id = $model->category_id;                
            $category = Category::where('id',$this->category_id)->select('id','name')->first();
            $this->emit('getCategory',$category);            
           
        }
        
    }

    public function mount(){
        abort_if(Gate::denies('access_menu_models'), Response::HTTP_FORBIDDEN, 'You Not Authorized');
    }

    public function resetData(){
        $this->reset();
    }

    public function destroy(Models $model){
        $model->delete();   
        $this->emit('flash_message', 'Data has been deleted!');     
    }

    public function render()
    {
        $models = Models::orderByDesc('id');
        if($this->category){
            $models->where('category_id',$this->category);
        }
        if($this->q){            
            $models->where(function($query){                                
                $query->orWhere('name','like','%'.$this->q.'%');                                                                     
            });  
        }

        $models = $models->paginate(10);
        return view('livewire.master.model.index',[
            'models'=>$models
        ]);
    }
}
