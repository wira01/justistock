<?php

namespace App\Http\Livewire\Master\Marketplace;

use App\Http\Livewire\Extender\Ancestor;

use App\Models\Master\Marketplace;
use Symfony\Component\HttpFoundation\Response;
use Gate;
use Auth;

class Index extends Ancestor
{
    public $name,$type;
    public $idMarketplace = '';

    public function store(){        
        
        $name = Auth::user()->username;
        $this->validate([
            'name'=>'required',
            'type'=>'required',
        ]);

        if(!$this->idMarketplace){
            Marketplace::create([
            'name'=>$this->name,
            'type'=>$this->type,            
            'created_by'=>$name,
        ]);    
        }else{
            Marketplace::find($this->idMarketplace)->update([
                'name'=>$this->name,
                'type'=>$this->type,                            
                'updated_by'=>$name,
            ]);
        }

        $this->showCard = 'table';
        $this->resetData();        
        $this->emit('flash_message', 'Data has been saved!');
    }

    public function getData($id){        
        $this->idMarketplace = $id;
        $marketplace = Marketplace::find($this->idMarketplace);
        if($marketplace){
            $this->name = $marketplace->name;
            $this->type = $marketplace->type;        
        }
        
    }

    public function mount(){
        abort_if(Gate::denies('access_menu_marketplaces'), Response::HTTP_FORBIDDEN, 'You Not Authorized');
    }

    public function resetData(){
        $this->reset();
    }

    public function destroy(Marketplace $marketplace){
        $marketplace->delete();   
        $this->emit('flash_message', 'Data has been deleted!');     
    }

    public function render()
    {
        $marketplaces = Marketplace::orderByDesc('id');
        if($this->q){            
            $marketplaces = $marketplaces->where(function($query){
                $query->orWhere('name','like','%'.$this->q.'%');                
                $query->orWhere('type','like','%'.$this->q.'%');                
            });  
        }

        $marketplaces = $marketplaces->paginate(10);

        return view('livewire.master.marketplace.index',[
            'marketplaces'=>$marketplaces
        ]);
    }
}
