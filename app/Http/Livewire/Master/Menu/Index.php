<?php

namespace App\Http\Livewire\Master\Menu;

use Livewire\Component;

use App\Models\Master\Menu;
use App\Http\Livewire\Extender\Ancestor;
use Auth;
use Symfony\Component\HttpFoundation\Response;
use Gate;

class Index extends Ancestor
{
    public $title ,
           $alias , 
           $url , 
           $icon , 
           $position , 
           $parent_id,          
           $parent_check = false;          
    public $idMenu = '';

    public function store(){
        $name = Auth::user()->username;
        $this->validate([
            'title'=>'required',
            'alias'=>'required',
            'url'=>'required',
            'icon'=>'required',
            'position'=>'required',            
        ]);

        if(!$this->idMenu){
            Menu::create([
            'name'=>$this->title??'',
            'alias'=>$this->setSlug($this->alias)??'',
            'url'=>$this->url??'',
            'icon'=>$this->icon??'',
            'index'=>$this->position??'',
            'parent_id'=>$this->parent_id && $this->parent_check?$this->parent_id:0,
            'created_by'=>$name,
        ]);    
        }else{
            Menu::find($this->idMenu)->update([
                'name'=>$this->title??'',
                'alias'=>$this->setSlug($this->alias)??'',
                'url'=>$this->url??'',
                'icon'=>$this->icon??'',
                'position'=>$this->icon??'',
                'parent_id'=>$this->parent_id??0,                
                'updated_by'=>$name,
            ]);
        }
        $this->showCard = 'table';       
        $this->resetData(); 
        $this->emit('flash_message', 'Data has been saved!');
    }

    public function getData($id){        
        $this->idMenu = $id;
        $data = Menu::find($this->idMenu);
        $this->title = $data->name;
        $this->alias = $data->alias;
        $this->url = $data->url;
        $this->icon = $data->icon;
        $this->position = $data->index;
        $this->parent_id = $data->parent_id;
        if($this->parent_id <> 0){
            $this->parent_check = true;
            $menu = Menu::where('id',$this->parent_id)->select('id','name')->first();
            $this->emit('getMenu',$menu);            
        }else{
            $this->parent_check = false;    
            $this->parent_id  = null;    
        }

    }

    public function resetData(){
        $this->reset();
    }

    public function destroy(Menu $menu){
        $menu->delete();   
        $this->emit('flash_message', 'Data has been deleted!');     
    }

    public function mount(){
        abort_if(Gate::denies('access_menu_menus'), Response::HTTP_FORBIDDEN, '403 Forbidden');
    }

    public function render()
    {   
        
        $menus = Menu::orderByDesc('id');

        if($this->q){            
            $menus->where('name','like','%'.$this->q.'%');  
        }

        $menus = $menus->paginate(10);
        return view('livewire.master.menu.index',[
            'menus'=>$menus
        ]);
    }
}
