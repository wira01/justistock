<?php

namespace App\Http\Livewire\Master\Item;

use App\Http\Livewire\Extender\Ancestor;

use App\Models\User;
use App\Models\Master\Role;
use App\Models\Master\Size;
use App\Models\Master\Item;
use App\Models\Master\DetailItem;
use App\Models\Master\Warehouse;
use App\Models\Master\Models;
use App\Models\Master\Marketplace;

use Symfony\Component\HttpFoundation\Response;
use Gate;
use Auth;
use DB;
use Livewire\WithFileUploads;

class Index extends Ancestor
{
    use WithFileUploads;
    
    public $name,$sku,$model,$unit,$image,$image_temp,$marketplace,$warehouse,$rack,$sku_other,$qty,$min_qty,$modal_price,$sales_price,$size,$size_text,$detailId,$detailTable = [],$detailItems = [],$index = 0,$indexDetail = 0,$sizes,$warehouses = [],$racks = [],$notes,$tempItems,$iqty,$stok_awal; 
    // filtering
    public $sku_filter,$name_filter,$category_filter,$model_filter,$qty_filter,$qty_cacl,$size_filter,$modal_price_filter,$sales_price_filter,$unit_filter,$modal_cacl,$sales_cacl;
    public $idItem = ''; 

    public $selectedItem = [];

    public function clicktable($id){
        if(in_array($id,$this->selectedItem)){
            unset($this->selectedItem[array_search($id,$this->selectedItem)]);
        }else{
            array_push($this->selectedItem,$id);
        }
        
    }  

    public function resetData(){ 
        $this->emit('resetDetail');       
        $this->reset();
    }

    public function update(){

        $name = Auth::user()->username;

         $this->validate([
            'name'=>'required',
            'sku'=>'required',
            'model'=>'required',            
            'unit'=>'required',
            'image' => 'nullable|image|max:1024',
        ]);        


         try {
            DB::beginTransaction();
             $item = Item::find($this->idItem);  

             $item_array = [
                'name'=>$this->name,   
                'sku'=>$this->sku,   
                'notes'=>$this->notes,      
                'updated_by'=>$name,
                'warehouse'=>$this->warehouse,                                                        
                'rack'=>$this->rack
             ];
             if($this->image){
                $item_array['images'] = $this->image?$this->image->store('stock','public'):'';
             }

             // check not same
             
             $item_same = Item::where('id','<>',$this->idItem)->where('sku',$this->sku)->whereNull('deleted')->first();
             
             
             if($item_same){
                return $this->addError('sku','The Sku field has duplicate');
             }

             $item->update($item_array);           
             if($item->first()){
                // add new size
                if($this->detailItems){
                foreach($this->detailItems as $key => $data){
                        $item->detail()->create([
                        'item_id'=>$this->idItem,
                        'size_id'=>$data['size'],
                        'qty'=>$data['qty'],
                        'min_qty'=>$data['min_qty']??0,
                        'iQty'=>$data['iqty']??0,
                        'stok_awal'=>$data['qty']??0,
                        'modal_price'=>$data['modal_price'],
                        'sales_price'=>$data['sales_price'],
                    ]);
                    }
                }
                // update old size
                if($this->tempItems){
                    foreach($this->tempItems as $key => $item){                        
                    DetailItem::find($key)->update([                        
                        'min_qty'=>$item['min_qty']??0,
                        'iQty'=>$item['iqty']??0,
                        'modal_price'=>$item['price'],
                        'sales_price'=>$item['sales_price'],
                    ]);
                    }
                }
             }
             DB::commit();
             $this->showCard = 'table';
            $this->resetData();        
            $this->emit('flash_message', 'Data has been saved!');

         } catch (\Exception $e) {
             $this->emit('flash_error', $e->getMessage());
            DB::rollBack();
         }
        

    }

    public function store(){
        
        $name = Auth::user()->username;

        $this->validate([
            'name'=>'required',
            'model'=>'required',
            'sku'=>'required',
            'unit'=>'required',
            'detailTable'=>'required',
            'image' => 'nullable|image|max:1024',
        ]);    

        // check not same
             $item_same = Item::whereSku($this->sku)->whereNull('deleted')->first();
             if($item_same){
                return $this->addError('sku','The Sku field has duplicate');
             }


        try {
            DB::beginTransaction();

            if(!$this->idItem){
            $item = Item::create([
            'name'=>$this->name,                        
            'model_id'=>$this->model,                                                        
            'sku'=>$this->sku,                                                        
            'warehouse'=>$this->warehouse,                                                        
            'rack'=>$this->rack,                                                        
            'unit'=>$this->unit,                                                        
            'images'=>$this->image?$this->image->store('stock','public'):'',                                                                             
            'notes'=>$this->notes,                                                        
            'created_by'=>$name,           
        ]);   

            if($item){
                foreach($this->detailTable as $data){                    
                    $item->detail()->create([
                        'item_id'=>$item->id,
                        'size_id'=>$data['size'],
                        'qty'=>$data['qty'],
                        'min_qty'=>$data['min_qty']??0,
                        'iQty'=>$data['iqty']??0,
                        'stok_awal'=>$data['qty']??0,
                        'modal_price'=>$data['modal_price'],
                        'sales_price'=>$data['sales_price'],
                    ]);
                }
            }

        }else{
            Item::find($this->idItem)->update([
                'name'=>$this->name,   
                'warehouse'=>$this->warehouse,                                                        
                'rack'=>$this->rack, 
                'images'=>$this->image_temp??$this->image->store('stock','public'),  
                'notes'=>$this->notes,                
                'updated_by'=>$name,   
            ]);
        }

        DB::commit();

        $this->showCard = 'table';
        $this->resetData();        
        $this->emit('flash_message', 'Data has been saved!');

        } catch (\Exception $e) {
            $this->emit('flash_error', $e->getMessage());
            DB::rollBack();
        }
        
    }
    public function mount(){
        abort_if(Gate::denies('access_menu_items'), Response::HTTP_FORBIDDEN, 'You Not Authorized');
        $this->sizes = Size::orderBy('index')->pluck('name','id');          
    }

     public function hydrate(){
        $this->warehouses = Warehouse::pluck('name','id');          
    }

    public function addDetail(){
        $this->validate([            
            'size'=>'required',
            'qty'=>'required',            
        ]);


        $dataDetail = [
            'index'=>$this->indexDetail,
            'size'=>$this->size,
            'size_text'=>$this->size_text,
            'qty'=>$this->qty,
            'min_qty'=>$this->min_qty,
            'iqty'=>$this->iqty,
            'stok_awal'=>$this->stok_awal,
            'modal_price'=>$this->modal_price,
            'sales_price'=>$this->sales_price,
            'item_id'=>'',
            'delete'=>false,
        ];                
        if(isset($this->detailTable[$this->size])){
            $this->detailTable[$this->size] = $dataDetail;
        }else{
            $this->detailTable[$this->size] = $dataDetail;
            $this->indexDetail += 1; 
        }
        
              
    }



    public function updatedWarehouse($name){
        $warehouse = Warehouse::whereName($name)->first();            
        if($name && $warehouse){
            $this->racks = json_decode($warehouse->detail_warehouse);   
        }
    }

    public function deleteDetail($index){                  
        unset($this->detailTable[$index]);
        // $this->detailTable[$index] = array_values($this->detailTable);  

    }   

    public function getData($id){
        $this->idItem = $id;
        $data = Item::find($this->idItem);
        
        $this->name = $data->name;
        $this->sku = $data->sku;
        $this->image_temp = $data->images;
        $this->model = $data->model_id;
        if($this->model){    
            $model = Models::whereId($this->model)->select('id','name')->first();        
            $this->emit('getData',$model,'model');
        }
        $this->warehouse = $data->warehouse;
        if($this->warehouse){
            $warehouse = Warehouse::whereName($this->warehouse)->first();            
            if($warehouse){
                $this->racks = json_decode($warehouse->detail_warehouse);   
            }
        }
        $this->rack = $data->rack;        
        $this->unit = $data->unit;
        if($data->detail){
            foreach($data->detail as $key => $detail){
                array_push($this->detailTable, [
                    'index'=>$key,
                    'size'=>$detail->size_id,
                    'size_text'=>$detail->size->name??'-',
                    'qty'=>$detail->qty,
                    'min_qty'=>$detail->min_qty,
                    'iqty'=>$detail->iQty,
                    'stok_awal'=>$detail->stok_awal,
                    'modal_price'=>$detail->modal_price,
                    'sales_price'=>$detail->sales_price,
                    'item_id'=>$detail->id,
                    'delete'=>false,
                ]);

                $this->tempItems[$detail->id]['min_qty'] = $detail->min_qty??0;
                $this->tempItems[$detail->id]['iqty'] = $detail->iQty??0;
                $this->tempItems[$detail->id]['stok_awal'] = $detail->stok_awal??0;
                $this->tempItems[$detail->id]['price'] = $detail->modal_price??0;
                $this->tempItems[$detail->id]['sales_price'] = $detail->sales_price??0;                
            }

        }
       
    }

    public function updatedImage(){
        $this->image_temp = null;
    }

    public function destroy(Item $item){
        $item->update([
            'deleted'=>1,
        ]);   
        $this->emit('flash_message', 'Data has been deleted!');
    }

    public function modelSize(){
        $this->emit('callAddSize');
    }

    public function closeModalSize(){
        $this->emit('closeModalSize');
    }

    public function addSize(){
        $this->validate([            
            'size'=>'required',
            'qty'=>'required',            
        ]);

        $detail = Item::where('id',$this->idItem)->whereHas('detail',function($query){
            $query->where('size_id',$this->size);
        })->first();

        if($detail){
            return $this->addError('size','The Size field has duplicate');
        }

        $dataDetail = [
            'index'=>$this->indexDetail,
            'size'=>$this->size,
            'size_text'=>$this->size_text,
            'qty'=>$this->qty,
            'min_qty'=>$this->min_qty,
            'iqty'=>$this->iqty,
            'stok_awal'=>$this->stok_awal,
            'modal_price'=>$this->modal_price,
            'sales_price'=>$this->sales_price,            
            'delete'=>false,
        ];        

        if(isset($this->detailItems[$this->size])){
            $this->detailItems[$this->size] = $dataDetail;
        }else{
            $this->detailItems[$this->size] = $dataDetail;
            $this->indexDetail += 1; 
        }        

        $this->closeModalSize();
        
    }

    public function deleteItem($index){                  
        unset($this->detailItems[$index]);
        // $this->detailTable[$index] = array_values($this->detailTable);  

    }   

    public function templateOpaname(){
        
    }

        
    public function render()
    {
        $items = Item::with('detail')->withSum('detail','qty')->orderByDesc('id');

        if($this->q){            
            $items->where('name','like','%'.$this->q.'%');  
        }
        if($this->sku_filter){            
            $items->where('sku','like','%'.$this->sku_filter.'%');  
        }
        if($this->unit_filter){            
            $items->where('unit','like',$this->unit_filter);  
        }
        if($this->category_filter){                       
            $items->whereHas('model',function($query){
                $query->whereHas('category',function($query){
                    $query->where('id',$this->category_filter);
                });
            });  
        }

        if($this->model_filter){                       
            $items->where('model_id',$this->model_filter);  
        }

        if($this->size_filter){
           $items->whereHas('detail',function($query){
                $query->where('size_id',$this->size_filter);
           });
        }

        if($this->qty_filter){
            $items->whereHas('detail',function($query){
                $query->where('qty',$this->qty_cacl,$this->qty_filter);
           });
        }
        
        if($this->modal_price_filter){
            $items->whereHas('detail',function($query){
                $query->where('modal_price',$this->modal_cacl,$this->modal_price_filter);
           });
        }

        if($this->sales_price_filter){
            $items->whereHas('detail',function($query){
                $query->where('sales_price',$this->sales_cacl,$this->sales_price_filter);
           });
        }

        if($this->size_filter && $this->qty_filter){                       
            $items->whereHas('detail',function($query){
                $query1 = $query->where('size_id',$this->size_filter);
                if($this->qty_cacl && $this->qty_filter){
                    $query1->where('qty',$this->qty_cacl,$this->qty_filter);    
                }   
            });  
        }   

        if($this->size_filter && $this->modal_price_filter){                       
            $items->whereHas('detail',function($query){
                $query1 = $query->where('size_id',$this->size_filter);
                if($this->modal_cacl && $this->modal_price_filter){
                   $query1->where('modal_price',$this->modal_cacl,$this->modal_price_filter);    
                }   
            });  
        }  

        if($this->size_filter && $this->sales_price_filter){                       
            $items->whereHas('detail',function($query){
                $query1 = $query->where('size_id',$this->size_filter);
                if($this->sales_cacl && $this->sales_price_filter){
                   $query1->where('sales_price',$this->sales_cacl,$this->sales_price_filter);    
                }   
            });  
        }    

        $items = $items->whereNull('deleted')->paginate(10);



        return view('livewire.master.item.index',[
            'items'=>$items
        ]);
    }
}
