<?php

namespace App\Http\Livewire\Import\LembarKerja;

use App\Http\Livewire\Extender\Ancestor;

use App\Models\Master\Marketplace;
use App\Models\Master\Item;
use App\Models\Master\Size;
use App\Models\Master\DetailItem;
use App\Models\Master\Customer;
use App\Models\Transaction\LembarKerja;
use App\Models\Transaction\Stock;
use App\Imports\LembarKerjaImport;
use Livewire\WithFileUploads;
use App\Models\DetailStock;

use Symfony\Component\HttpFoundation\Response;
use Gate;
use Auth;
use DB;
use Carbon\Carbon;

class Index extends Ancestor
{
    use WithFileUploads;

    public $import;

    public function importNow(){ 
        $this->validate([            
            'import'=>'required|file|mimes:xls,xlsx'
        ]);
        $name = Auth::user()->username;
        $datas = (new LembarKerjaImport)->toCollection($this->import);
        $duplicateData = [];
        $count = 0;
        $total = 0;
        

        // validation
        try {
            DB::beginTransaction();

            foreach ($datas[0] as $key => $data) {
            $lembar_kerja = '';
            $marketplace = Marketplace::where('name',$data['marketplace'])->first();  
            
            $sizes = Size::select('id','name');   
            $detail = DetailItem::select('id','item_id','size_id','qty','modal_price');   
            $item = Item::select('items.id','detail.size_id','detail.id as detail_id','detail.qty','size.name','detail.modal_price')->JoinSub($detail,'detail',function($join){
                $join->on('detail.item_id','=','items.id');
            })->JoinSub($sizes,'size',function($join){
                $join->on('size.id','=','detail.size_id');
            });
            $item = $item->whereNull('items.deleted')->where('sku',$data['sku'])->where('size.name',$data['size'])->first();

            $no_pesanan = LembarKerja::whereNotNull('no_pesanan')->where('no_pesanan',$data['no_pesanan'])->first();

                if(in_array($data['no_pesanan'], $duplicateData)){
                    $count++;
                }else{
                    $count = 0;
                }


                if(!$data['tanggal']){
                    return $this->addError('errors','Tanggal is required on row '.intval($key+1));
                }else if(!in_array($data['keterangan'], ['jual','beli','retur penjualan','retur pembelian'])) {
                    return $this->addError('errors','Keterangan only (jual,beli,retur pembelian,retur penjualan) on row '.intval($key+1));
                }else if(!$data['marketplace']){
                    return $this->addError('errors','Marketplace field is required on row '.intval($key+1));
                }else if(!$marketplace){
                    return $this->addError('errors','Marketplace not found on row '.intval($key+1));    
                }else if(!$data['no_pesanan']){
                    return $this->addError('errors','No Pesanan is required on row '.intval($key+1)); 
                }else if($no_pesanan && $count < 1){
                    return $this->addError('errors','No Pesanan is duplicate on row '.intval($key+1)); 
                }else if(!$item){
                    return $this->addError('errors','SKU not found on row '.intval($key+1)); 
                }else if(!$item->name){
                    return $this->addError('errors','Size not found on row '.intval($key+1));
                }else if($marketplace->type == 'offline' && !$data['contact']){
                    return $this->addError('errors','Contact is require on row '.intval($key+1));
                }else if(in_array($data['keterangan'], ['jual','retur pembelian']) && $data['qty'] > $item->qty){
                    return $this->addError('errors','Qty larger than stock qty on row '.intval($key+1));
                }else{                                        
                        
                        $exists_lembar_kerja = LembarKerja::whereNotNull('no_pesanan')->where('no_pesanan',$data['no_pesanan'])->first();  
                                         
                        if($marketplace->type == 'offline'){
                            $customer = Customer::where('name',$data['contact'])->first();
                            if(!$customer && $data['contact']){
                                Customer::create([
                                'name'=>$data['contact'],
                                'created_by'=>$name
                            ]);
                            }
                        }

                        if($data['keterangan'] == 'jual'  || $data['keterangan'] == 'retur pembelian'){
                            $total = $data['qty']*$data['harga'];
                        }

                        if($data['keterangan'] == 'beli'  || $data['keterangan'] == 'retur penjualan'){
                            $total = $data['qty']*$item->modal_price;
                        }


                        if($exists_lembar_kerja){
                            $exists_lembar_kerja->detail()->create([
                                'lembar_kerja_id'=>$exists_lembar_kerja->id,
                                'detail_item_id'=>$item->detail_id,
                                'qty'=>$data['qty'],
                                'discount'=>$data['discount']??0,
                                'modal_price'=>$item->modal_price,
                                'sales_price'=>$data['harga'],
                                'total'=>$total,
                            ]);

                            $this->addStockDetail($item->detail_id,$data['keterangan'],$data['qty'],$name,$data['tanggal']);
                        }else{
                            $lembar_kerja = LembarKerja::create([
                                'no_pesanan'=>$data['no_pesanan'],                        
                                'keterangan'=>$data['keterangan'],                                                        
                                'tanggal'=>Carbon::parse($data['tanggal']),                                                        
                                'marketplace_id'=>$marketplace->id,                                                        
                                'contact'=>$data['contact'],                                     
                                'created_by'=>$name,           
                            ]);  
                            $lembar_kerja->detail()->create([
                                'lembar_kerja_id'=>$lembar_kerja->id,
                                'detail_item_id'=>$item->detail_id,
                                'qty'=>$data['qty'],
                                'discount'=>$data['discount']??0,
                                'modal_price'=>$item->modal_price,
                                'sales_price'=>$data['harga'],
                                'total'=>$total,
                            ]);

                            $this->addStockDetail($item->detail_id,$data['keterangan'],$data['qty'],$name,$data['tanggal']);

                        }                        
                    
                } 
                array_push($duplicateData,$data['no_pesanan']);        
        } 
        DB::commit();        
        $this->emit('flash_message', 'Data has been saved!'); 
        } catch (\Exception $e) {
            DB::rollBack();
            $this->emit('flash_error', $e->getMessage()); 
        }
                    

}

    private function addStockDetail($item,$keterangan,$qty,$created_by,$tanggal){
             $detail = DetailItem::find($item);

             if($keterangan == 'jual'  || $keterangan == 'retur pembelian'){
                DetailStock::create([
                            'detail_item_id'=>$item,
                            'size'=>$detail->size->name,
                            'model'=>$detail->item->model->name,
                            'stok_awal'=>$detail->qty,
                            'stok_akhir'=>$detail->qty-$qty,
                            'keterangan'=>$keterangan,
                            'created_at'=>Carbon::parse($tanggal)
                ]);
                $detail->update([
                    'qty'=>$detail->qty-$qty
                ]);
             }

             if($keterangan == 'beli'  || $keterangan == 'retur penjualan'){
                DetailStock::create([
                            'detail_item_id'=>$item,
                            'size'=>$detail->size->name,
                            'model'=>$detail->item->model->name,
                            'stok_awal'=>$detail->qty,
                            'stok_akhir'=>$detail->qty+$qty,
                            'keterangan'=>$keterangan,
                            'created_at'=>Carbon::parse($tanggal)
                        ]);
                $detail->update([
                    'qty'=>$detail->qty+$qty
                ]);
             }

             Stock::create([
                'detail_item_id'=>$item,
                'out'=>
                        ($keterangan == 'jual'  || $keterangan == 'retur pembelian' && $qty)?$qty:0, 
                'in'=>
                        ($keterangan == 'beli'  || $keterangan == 'retur penjualan' && $qty)?$qty:0, 
                'created_by'=>$created_by, 
             ]);
    }

    public function render()
    {
        return view('livewire.import.lembar-kerja.index');
    }
}
