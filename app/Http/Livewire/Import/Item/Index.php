<?php

namespace App\Http\Livewire\Import\Item;

use App\Http\Livewire\Extender\Ancestor;

use App\Imports\ItemsImport;
use Livewire\WithFileUploads;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\Models\Master\Item;
use App\Models\Master\DetailItem;
use App\Models\Master\Size;
use App\Models\Master\Models;
use Excel;
use Auth;
use DB;


class Index extends Ancestor
{
    use WithFileUploads;

    public $import;

    public function importNow(){        
        $this->validate([
            'import'=>'required|file|mimes:xls,xlsx'
        ]);

        $name = Auth::user()->username;
        $rows = (new ItemsImport)->toCollection($this->import);
        $duplicateData = [];
        $count = 0;        

        // dd($rows);

        try {
            DB::beginTransaction();

            foreach($rows[0] as $key => $data){

            

            $store = '';            
            $detail_item = DetailItem::select('id','item_id','size_id');
            $size_select = Size::select('id','name');
            $sku = Item::where('sku',$data['sku'])->whereNull('deleted')->first();
            
            $size = Size::where('name',$data['size'])->first();

            if(!$size){
                $size = Size::create([
                    'name'=>$data['size'],
                    'created_by'=>$name
                ]);
            }

            $model = Models::where('name',$data['model'])->first();
            $size_exist = DB::table('items')
                ->join('detail_items','detail_items.item_id','=','items.id')
                ->join('sizes','sizes.id','=','detail_items.size_id')
                ->whereNull('items.deleted_at')
                ->where('sku',(string)$data['sku'])->whereNull('items.deleted')->where('sizes.name',$data['size'])->first();            
            

            if(in_array($data['sku'], $duplicateData)){
                    $count++;
                }else{
                    $count = 0;
                }

            if(!$data['nama']){
                return $this->addError('errors','The Nama field is required on row '.intval($key+1));
            }else if(!$data['sku']){
                return $this->addError('errors','The SKU field is required on row '.intval($key+1));
            }else if(!$data['model']){
                return $this->addError('errors','The Model field is required on row '.intval($key+1));
            }else if(!$model){
                return $this->addError('errors','Model field not found on row '.intval($key+1));
            }else if($size_exist){
                return $this->addError('errors',$data['sku'].' has duplicate size field on row '.intval($key+1));
            }else if(!$data['size']){
                return $this->addError('errors','Size field is required on row '.intval($key+1));
            }else if(!$data['harga_modal']){
                return $this->addError('errors','Harga Modal field is required on row '.intval($key+1));
            }else{
                if($data['images']){
                $image = public_path('temp').'/'.$data['images'];
                $store = Storage::putFile('public/stock', new File($image));
                $store = substr($store,7);
                // unlink($image);
            }

            $existing_item = Item::whereNotNull('sku')->where('sku',$data['sku'])->whereNull('deleted')->first();

            if(!$existing_item){
                $item = Item::create([
                    'name'=>$data['nama'],
                    'model_id'=>$model->id,
                    'sku'=>$data['sku'],
                    'warehouse'=>$data['warehouse'],
                    'rack'=>$data['rack'],
                    'unit'=>$data['unit'],
                    'images'=>$store,
                    'created_by'=>$name
                    
                ]);

                $item->detail()->create([
                    'size_id'=>$size->id,
                    'qty'=>$data['qty'],
                    'iQty'=>$data['iqty'],
                    'min_qty'=>$data['minimum_qty'],
                    'modal_price'=>$data['harga_modal']??0,
                    'sales_price'=>$data['harga_jual']??0,
                ]);
            }else{
                $existing_item->detail()->create([
                     'size_id'=>$size->id,
                    'qty'=>$data['qty'],
                    'iQty'=>$data['iqty'],
                    'min_qty'=>$data['minimum_qty'],
                    'modal_price'=>$data['harga_modal']??0,
                    'sales_price'=>$data['harga_jual']??0,
                ]);
            }
            }
        
            array_push($duplicateData,$data['sku']); 


        }
            DB::commit();
            $this->emit('flash_message', 'Data has been saved!');
        } catch (\Exception $e) {
            DB::rollBack();
            $this->emit('flash_error', $e->getMessage());
        }
    
    }

    public function render()
    {
        return view('livewire.import.item.index');
    }
}
