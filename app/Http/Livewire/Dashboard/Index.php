<?php

namespace App\Http\Livewire\Dashboard;

use Livewire\Component;

use App\Models\Master\DetailItem;
use App\Models\Master\Marketplace;
use App\Models\Transaction\LembarKerja;
use App\Models\Transaction\DetailLembarKerja;

use Livewire\WithPagination;
use Carbon\Carbon;
use DB;

class Index extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $filtering_by;

    public function mount(){
        $this->filtering_by = 'Today';
    }
    public function render()
    {                
        $marketplace = Marketplace::select('id','name');
        $detailLembarKerja = DetailLembarKerja::select('id','qty','lembar_kerja_id','sales_price','modal_price');

        $products = DetailItem::where('qty','<',DB::raw('min_qty'))->whereHas('item',function($query){
            $query->whereNull('deleted');
        });   


        $sales = DB::table('lembar_kerjas')->joinSub($detailLembarKerja,'detail',function($join){
            $join->on('detail.lembar_kerja_id','=','lembar_kerjas.id');
        })->where('keterangan','jual')->whereNull('deleted_at');

        $purchase = DB::table('lembar_kerjas')->joinSub($detailLembarKerja,'detail',function($join){
            $join->on('detail.lembar_kerja_id','=','lembar_kerjas.id');
        })->where('keterangan','beli')->whereNull('deleted_at');

        $sales_by_payment = LembarKerja::where('keterangan','jual')->whereNull('status');
        

        $marketplaces = DB::table('lembar_kerjas')->select('marketplace.name',DB::raw('SUM(detail.qty) as total_qty'))
        ->joinSub($detailLembarKerja,'detail',function($join){
            $join->on('detail.lembar_kerja_id','=','lembar_kerjas.id');
        })->joinSub($marketplace,'marketplace',function($join){
            $join->on('marketplace.id','=','lembar_kerjas.marketplace_id');
        })->where('keterangan','jual')->groupBy('marketplace_id'); 

        if($this->filtering_by == 'Weekly'){            
            $marketplaces->whereBetween('tanggal',[Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
            $sales->whereBetween('lembar_kerjas.tanggal',[Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
            $purchase->whereBetween('lembar_kerjas.tanggal',[Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
            $sales_by_payment = $sales_by_payment->whereBetween('lembar_kerjas.tanggal',[Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
        }else if($this->filtering_by == 'Monthly'){            
            $marketplaces->whereBetween('tanggal',[Carbon::now()->firstOfMonth(), Carbon::now()->lastOfMonth()]);
            $sales->whereBetween('tanggal',[Carbon::now()->firstOfMonth(), Carbon::now()->lastOfMonth()]);
            $purchase->whereBetween('tanggal',[Carbon::now()->firstOfMonth(), Carbon::now()->lastOfMonth()]);
            $sales_by_payment->whereBetween('tanggal',[Carbon::now()->firstOfMonth(), Carbon::now()->lastOfMonth()]);
        }else{            
            $marketplaces->where('tanggal', now())->get();
            $sales->where('tanggal', now())->get();
            $purchase->where('tanggal', now())->get();
            $sales_by_payment->where('tanggal', now())->get();
        }

        $products = $products->paginate(5);      
        $marketplaces = $marketplaces->limit(7)->get(); 
        $sales = $sales->sum(DB::raw('detail.qty * detail.sales_price'));     
        $purchase = $purchase->sum(DB::raw('detail.qty * detail.modal_price'));  
        $sales_by_payment = $sales_by_payment->paginate(5,['*'], 'payment');   
        
        return view('livewire.dashboard.index',[
            'products'=>$products,
            'marketplaces'=>$marketplaces,
            'sales'=>$sales,
            'sales_by_payment'=>$sales_by_payment,
            'purchase'=>$purchase,
        ]);
    }
}
