<?php

namespace App\Http\Livewire\Report\StockAkhir;

use App\Http\Livewire\Extender\Ancestor;

use App\Models\Master\Models;
use App\Models\Master\DetailItem;
use App\Models\Master\Size;
use App\Models\Master\Item;
use App\Models\DetailStock;


use Symfony\Component\HttpFoundation\Response;
use Gate;
use DB;
use PDF;
use Carbon\Carbon;

class Index extends Ancestor
{
    public $dateFrom,$dateTo;

    public function mount(){
        abort_if(Gate::denies('access_menu_stock_akhir'), Response::HTTP_FORBIDDEN, 'You Not Authorized');
    }

    public function generateReport($dateFrom = null,$dateTo = null,$search = null){
        // $model = Models::select('id','name');
        // $size = Size::select('id','name');
        // $detail = DetailItem::select('id','size_id','qty','item_id');
        // $item = Item::select('id','name','sku','model_id','deleted_at');

        // $date = 'SELECT MAX(created_at) from detail_stocks';

        
        // if ($dateFrom && !$dateTo) {            
        //     $date .= " where created_at between '".date("Y-m-d", strtotime($dateFrom.' 00:00:00'))." 00:00:00' and '".date("Y-m-d", strtotime($dateFrom.' 23:59:59'))." 23:59:59'";                     
        // }
        // if ($dateFrom && $dateTo) {
        //     $date .= " where created_at between '".date("Y-m-d", strtotime($dateFrom.' 00:00:00'))." 00:00:00' and '".date("Y-m-d", strtotime($dateTo.' 23:59:59'))." 23:59:59'";               
        // }

        // $date .= ' group by detail_item_id,model,size';

        // $stocks = DetailStock::select('items.name','items.sku','model','size',DB::raw('SUM(detail_stocks.stok_akhir) as total_akhir'),'created_at')
        // ->whereIn('detail_stocks.created_at',[DB::raw($date)])->LeftJoinSub($detail,'detail',function($join){
        //     $join->on('detail.id','=','detail_stocks.detail_item_id');
        // })->JoinSub($item,'items',function($join){
        //     $join->on('detail.item_id','=','items.id');
        // });


        // if ($dateFrom) {
        //     $stocks->whereDate('detail_stocks.created_at', '>=', date("Y-m-d", strtotime($this->dateFrom.' 00:00:00')));
        // }
        // if ($dateTo) {
        //     $stocks->whereDate('detail_stocks.created_at', '<=', date("Y-m-d", strtotime($this->dateTo.' 23:59:59')));
        // }

        // if($dateFrom == '' && $this->dateTo == ''){
        //     $stocks->whereDate('created_at',now());
        // }

        // if($search){
        //     $stocks->where(function($query){
        //         $query->orWhere('model.name','like','%'.$search.'%')->orWhere('size.name','like','%'.$search.'%');
        //     });
        // }

        // $stocks = $stocks->groupBy('detail_item_id','model','size')->orderBy('model')->get(); 
        $date1 = '';
        $q='';

        if ($dateFrom && !$dateTo) {            
            $date1 .= " where created_at between '".date("Y-m-d", strtotime($dateFrom.' 00:00:00'))." 00:00:00' and '".date("Y-m-d", strtotime($dateFrom.' 23:59:59'))." 23:59:59'";                     
        }
        if ($dateFrom && $dateTo) {
            $date1 .= " where created_at between '".date("Y-m-d", strtotime($dateFrom.' 00:00:00'))." 00:00:00' and '".date("Y-m-d", strtotime($dateTo.' 23:59:59'))." 23:59:59'";               
        }
        if($search){
            $q = "and model like '%".$search."%'";
        }

        $query = "select detail_stocks.*,items.name,items.sku,'created_at' from detail_stocks,
           (select id,detail_item_id,max(id) as created_at
                from detail_stocks ".$date1."
                group by detail_item_id) max_sales
             left join detail_items on detail_items.id = max_sales.detail_item_id 
             left join items on detail_items.item_id = items.id
             where detail_stocks.detail_item_id=max_sales.detail_item_id
             and detail_stocks.id=max_sales.created_at ".$q."";

        
        $stocks = DB::select($query); 


        $document_name = 'Report Stock Akhir '.date('Y-m-d h:i:s') . '.pdf';
        $pdfContent = PDF::loadView('pdf.downloads.stock_akhir', compact('stocks','dateTo','dateFrom'))->output();
       
        

        return response()->streamDownload(function()use($pdfContent){
            echo $pdfContent;
        },'Laporan Stock Akhir '.date('Y-m-d h:i:s') . '.pdf');
        
    }

    public function render()
    {

        // $model = Models::select('id','name');
        // $size = Size::select('id','name');
        // $detail = DetailItem::select('id','size_id','qty','item_id');
        // $item = Item::select('id','name','sku','model_id','deleted_at');
        // $date = 'SELECT MAX(created_at) from detail_stocks';

        $date1 = '';
        $q='';
        
        

        // $date .= ' group by detail_item_id';

        // $stocks = DetailStock::select('items.name','items.sku','model','size','stok_akhir')
        // ->whereIn('detail_stocks.created_at',[DB::raw($date)])->LeftJoinSub($detail,'detail',function($join){
        //     $join->on('detail.id','=','detail_stocks.detail_item_id');
        // })->JoinSub($item,'items',function($join){
        //     $join->on('detail.item_id','=','items.id');
        // });


        // if ($this->dateFrom) {
        //     $stocks->whereDate('detail_stocks.created_at', '>=', date("Y-m-d", strtotime($this->dateFrom.' 00:00:00')));
        //     // dd($stocks->dd());
        // }
        // if ($this->dateTo) {
        //     $stocks->whereDate('detail_stocks.created_at', '<=', date("Y-m-d", strtotime($this->dateTo.' 23:59:59')));
        // }

        // if($this->dateFrom == '' && $this->dateTo == ''){
        //     $stocks->whereDate('updated_at',now());
        // }

        // if($this->q){
        //     $stocks->where(function($query){
        //         $query->orWhere('model','like','%'.$this->q.'%')->orWhere('size.name','like','%'.$this->q.'%');
        //     });
        // }

        if ($this->dateFrom && !$this->dateTo) {            
            $date1 .= " where created_at between '".date("Y-m-d", strtotime($this->dateFrom.' 00:00:00'))." 00:00:00' and '".date("Y-m-d", strtotime($this->dateFrom.' 23:59:59'))." 23:59:59'";                     
        }
        if ($this->dateFrom && $this->dateTo) {
            $date1 .= " where created_at between '".date("Y-m-d", strtotime($this->dateFrom.' 00:00:00'))." 00:00:00' and '".date("Y-m-d", strtotime($this->dateTo.' 23:59:59'))." 23:59:59'";               
        }

        if($this->q){
            $q = "and model like '%".$this->q."%'";
        }

        $query = "select detail_stocks.*,items.name,items.sku,'created_at' from detail_stocks,
           (select id,detail_item_id,max(id) as created_at
                from detail_stocks ".$date1."
                group by detail_item_id) max_sales
             left join detail_items on detail_items.id = max_sales.detail_item_id 
             left join items on detail_items.item_id = items.id
             where detail_stocks.detail_item_id=max_sales.detail_item_id
             and detail_stocks.id=max_sales.created_at ".$q."";


        $stocks = DB::select($query);

        // $stocks = $stocks->groupBy('detail_item_id','model','size')->orderBy('updated_at')->paginate(10);        
        

        
        return view('livewire.report.stock-akhir.index',[
            'stocks'=>$stocks
        ]);
    }
}
