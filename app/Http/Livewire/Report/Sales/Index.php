<?php

namespace App\Http\Livewire\Report\Sales;

use App\Http\Livewire\Extender\Ancestor;

use App\Models\Transaction\LembarKerja;
use App\Models\Transaction\DetailLembarKerja;
use App\Models\Master\DetailItem;
use App\Models\Master\Marketplace;

use Symfony\Component\HttpFoundation\Response;
use Gate;
use DB;
use PDF;

class Index extends Ancestor
{
    public $dateFrom,$dateTo,$marketplace_filter_text,$marketplace_filter;

    public function mount(){
        abort_if(Gate::denies('access_menu_report_sales'), Response::HTTP_FORBIDDEN, 'You Not Authorized');         
    }

    public function generateSalesReport($dateFrom = null,$dateTo = null){
        $marketplaces = Marketplace::all();
         $detail = DetailLembarKerja::select('id','lembar_kerja_id','detail_item_id','qty','modal_price','total');
        $item = DetailItem::select('id','item_id');
        // $sales = LembarKerja::where('keterangan','jual')->withSum('detail','total')->with('detail')->orderByDesc('id');

        //  if($this->marketplace_filter){
        //     $sales->where('marketplace_id',$this->marketplace_filter);
        // }

        // if ($dateFrom) {
        //     $sales->whereDate('tanggal', '>=', date("Y-m-d", strtotime($dateFrom.' 00:00:00')));
        // }
        // if ($dateTo) {
        //     $sales->whereDate('tanggal', '<=', date("Y-m-d", strtotime($dateTo.' 23:59:59')));
        // }

        // $sales = $sales->withSum('detail','sales_price')->groupBy(DB::raw("DATE_FORMAT(tanggal, '%d-%m-%Y')"))->get();

        $sales = DB::table('lembar_kerjas')->joinSub($detail,'detail',function($join){
            $join->on('lembar_kerjas.id','=','detail.lembar_kerja_id');
        })->groupBy(DB::raw("DATE_FORMAT(tanggal, '%d-%m-%Y')"));

        if ($dateFrom) {
            $sales->whereDate('tanggal', '>=', date("Y-m-d", strtotime($dateFrom.' 00:00:00')));

        }
        if ($dateTo) {
            $sales->whereDate('tanggal', '<=', date("Y-m-d", strtotime($dateTo.' 23:59:59')));
        }

        $sales = $sales->where('keterangan','jual')->get();


        $document_name = 'Report Sales '.date('Y-m-d h:i:s') . '.pdf';
        $pdfContent = PDF::loadView('pdf.downloads.sales', compact('sales','dateTo','dateFrom','marketplaces'))->output();
       
        

        return response()->streamDownload(function()use($pdfContent){
            echo $pdfContent;
        },'Laporan Penjualan '.date('Y-m-d h:i:s') . '.pdf');
        
    }

    public function generateSalesReportDetail($dateFrom = null,$dateTo = null,$marketplace = null){
        $sales = LembarKerja::where('keterangan','jual')->withSum('detail','total')->with('detail')->orderByDesc('id');

         if($marketplace){
            $sales->where('marketplace_id',$marketplace);
        }

        if ($dateFrom) {
            $sales->whereDate('tanggal', '>=', date("Y-m-d", strtotime($dateFrom.' 00:00:00')));
        }
        if ($dateTo) {
            $sales->whereDate('tanggal', '<=', date("Y-m-d", strtotime($dateTo.' 23:59:59')));
        }

        $sales = $sales->withSum('detail','sales_price')->get();



        $document_name = 'Report Sales '.date('Y-m-d h:i:s') . '.pdf';
        $pdfContent = PDF::loadView('pdf.downloads.sales_detail', compact('sales','dateTo','dateFrom'))->setPaper('a4', 'landscape')->output();
       
        

        return response()->streamDownload(function()use($pdfContent){
            echo $pdfContent;
        },'Laporan Penjualan '.date('Y-m-d h:i:s') . '.pdf');
        
    }

    public function render()
    {
        // $marketplaces = Marketplace::all();
        //  $detail = DetailLembarKerja::select('id','lembar_kerja_id','detail_item_id','qty','modal_price','total');
        // $item = DetailItem::select('id','item_id');
        $sales = LembarKerja::where('keterangan','jual')->withSum('detail','total')->with('detail')->orderByDesc('id');

        if($this->marketplace_filter){
            $sales->where('marketplace_id',$this->marketplace_filter);
        }

        if ($this->dateFrom) {
            $sales->whereDate('tanggal', '>=', date("Y-m-d", strtotime($this->dateFrom.' 00:00:00')));
        }
        if ($this->dateTo) {
            $sales->whereDate('tanggal', '<=', date("Y-m-d", strtotime($this->dateTo.' 23:59:59')));
        }
        

        if($this->dateFrom == '' && $this->dateTo == ''){
            $sales->whereDate('created_at',now());  
        }

        // $sales = DB::table('lembar_kerjas')->joinSub($detail,'detail',function($join){
        //     $join->on('lembar_kerjas.id','=','detail.lembar_kerja_id');
        // })->groupBy(DB::raw("DATE_FORMAT(tanggal, '%d-%m-%Y')"));

        $sales = $sales->paginate(20);
        
        return view('livewire.report.sales.index',[
            'sales'=>$sales,
            // 'marketplaces'=>$marketplaces
        ]);
    }
}
