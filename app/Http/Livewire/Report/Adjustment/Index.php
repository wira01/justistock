<?php

namespace App\Http\Livewire\Report\Adjustment;

use App\Http\Livewire\Extender\Ancestor;

use App\Models\Transaction\Adjustment;
use Symfony\Component\HttpFoundation\Response;
use Gate;
use Auth;
use DB;
use PDF;

class Index extends Ancestor
{
    public $dateFrom,$dateTo,$q;

    public function mount(){
        abort_if(Gate::denies('access_menu_report_adjustment'), Response::HTTP_FORBIDDEN, 'You Not Authorized');         
    }

    public function generateAdjustmentReport($dateFrom = null,$dateTo = null){
        $adjustments = Adjustment::orderByDesc('created_at');

        if ($dateFrom) {
            $adjustments->whereDate('created_at', '>=', date("Y-m-d", strtotime($dateFrom.' 00:00:00')));
        }
        if ($dateTo) {
            $adjustments->whereDate('created_at', '<=', date("Y-m-d", strtotime($dateTo.' 23:59:59')));
        }

        $adjustments = $adjustments->get();


        $document_name = 'Report Adjustment '.date('Y-m-d h:i:s') . '.pdf';
        $pdfContent = PDF::loadView('pdf.downloads.adjustment', compact('adjustments','dateTo','dateFrom'))->output();
       
        

        return response()->streamDownload(function()use($pdfContent){
            echo $pdfContent;
        },'Laporan Adjustment '.date('Y-m-d h:i:s') . '.pdf');
        
    }
    public function render()
    {
        $adjustments = Adjustment::orderByDesc('created_at');

        if ($this->dateFrom) {
            $adjustments->whereDate('created_at', '>=', date("Y-m-d", strtotime($this->dateFrom.' 00:00:00')));
        }
        if ($this->dateTo) {
            $adjustments->whereDate('created_at', '<=', date("Y-m-d", strtotime($this->dateTo.' 23:59:59')));
        }

        if($this->dateFrom == '' && $this->dateTo == ''){
            $adjustments->whereDate('created_at',now());
        }

        if($this->q){
            $adjustments->where(function($query){
                $query->whereHas('detail',function($query){
                    $query->whereHas('detailItem',function($query){
                        $query->whereHas('item',function($query){
                            $query->where('name','like','%'.$this->q.'%');
                        });
                    });
                });
            });
        }


        $adjustments = $adjustments->get();

        return view('livewire.report.adjustment.index',[
            'adjustments'=>$adjustments
        ]);
    }
}
