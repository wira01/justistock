<?php

namespace App\Http\Livewire\Transaction\Adjustment;

use App\Http\Livewire\Extender\Ancestor;

use App\Models\Master\DetailItem;
use App\Models\Transaction\Adjustment;
use App\Models\Transaction\DetailAdjustment;
use App\Models\Transaction\Stock;
use Symfony\Component\HttpFoundation\Response;
use App\Models\DetailStock;
use Gate;
use Auth;
use DB;

class Index extends Ancestor
{

    public $keterangan,$model,$model_text,$product,$q,$product_text,$size,$size_text,$qty,$indexDetail,$detailTable = [];
    public $dateFrom,$dateTo;

    public function store(){
        $name = Auth::user()->username;
        $keterangan = '';

        $this->validate([                                
            'detailTable'=>'required',            
        ]);

        try {
            DB::beginTransaction();            
            $adjustment = Adjustment::create([
                'no_adjustment'=>'ADJ/'.getNoPesanan(),          
                'created_by'=>$name,           
            ]);   

            if($adjustment){
                foreach($this->detailTable as $data){                    
                    

                    $detail = DetailItem::find($data['size']);

                    $adjustment->detail()->create([
                        'adjustment_id'=>$adjustment->id,
                        'detail_item_id'=>$data['size'],
                        'keterangan'=>$data['keterangan'],
                        'qty'=>$data['qty'],
                        'bQty'=>$detail->qty                        
                    ]);

                     if($data['keterangan'] == 'kurang'){
                        DetailStock::create([
                            'detail_item_id'=>$data['size'],
                            'size'=>$detail->size->name,
                            'model'=>$detail->item->model->name,
                            'stok_awal'=>$detail->qty,
                            'stok_akhir'=>$detail->qty-$data['qty'],
                            'keterangan'=>'adjustments'                           
                        ]);
                        $detail->update([
                            'qty'=>$detail->qty-$data['qty']
                        ]);
                     }

                     if($data['keterangan'] == 'tambah'){
                        DetailStock::create([
                            'detail_item_id'=>$data['size'],
                            'size'=>$detail->size->name,
                            'model'=>$detail->item->model->name,
                            'stok_awal'=>$detail->qty,
                            'stok_akhir'=>$detail->qty+$data['qty'],
                            'keterangan'=>'adjustments'
                        ]);
                        $detail->update([
                            'qty'=>$detail->qty+$data['qty']
                        ]);
                     }

                     Stock::create([
                        'detail_item_id'=>$data['size'],
                        'out'=>
                            ($data['keterangan'] == 'kurang')?$data['qty']:0, 
                        'in'=>
                            ($data['keterangan'] == 'tambah')?$data['qty']:0, 
                        'created_by'=>$name, 
                     ]);
                }
            }

            DB::commit();

        $this->showCard = 'table';
        $this->resetData();        
        $this->emit('flash_message', 'Data has been saved!');

        } catch (\Exception $e) {
            $this->emit('flash_error', $e->getMessage());
            DB::rollBack();
        }
    }

    public function resetData(){
        $this->reset();
        $this->emit('resetDetail');
    }


    public function addDetail(){
        

        $this->validate([            
            'model'=>'required',
            'keterangan'=>'required',
            'product'=>'required',            
            'size'=>'required',            
            'qty'=>'required',            
        ]);

        $item = DetailItem::find($this->size);

        if($this->keterangan == 'kurang'){
            if($item && $item->qty < $this->qty){                      
                return $this->addError('qty','Qty melebihi batas stok');
            }
        }

        foreach($this->detailTable as $table){
            if($table['product'] == $this->product && $table['size'] == $this->size){
                return $this->addError('product','Produk dan Size sudah ada');
            }
        }

        $dataDetail = [
            'index'=>$this->indexDetail,
            'keterangan'=>$this->keterangan,
            'model'=>$this->model,
            'model_text'=>$this->model_text,
            'product'=>$this->product,
            'product_text'=>$this->product_text,
            'size'=>$this->size,
            'size_text'=>$this->size_text,
            'qty'=>$this->qty,                        
            'delete'=>false,
        ];    
            array_push($this->detailTable,$dataDetail);
            $this->indexDetail += 1; 
            $this->size = null;
            $this->size_text = null;
        }

        public function mount(){
        abort_if(Gate::denies('access_menu_adjustment'), Response::HTTP_FORBIDDEN, 'You Not Authorized');         
    }

    public function deleteDetail($index){                  
        unset($this->detailTable[$index]);
    } 

    public function render()
    {
        $adjustments = Adjustment::orderByDesc('created_at');

        if ($this->dateFrom) {
            $adjustments->whereDate('created_at', '>=', date("Y-m-d", strtotime($this->dateFrom.' 00:00:00')));
        }
        if ($this->dateTo) {
            $adjustments->whereDate('created_at', '<=', date("Y-m-d", strtotime($this->dateTo.' 23:59:59')));
        }

        if($this->dateFrom == '' && $this->dateTo == ''){
            $adjustments->whereDate('created_at',now());
        }

        if($this->q){
            $adjustments->where(function($query){
                $query->whereHas('detail',function($query){
                    $query->whereHas('detailItem',function($query){
                        $query->whereHas('item',function($query){
                            $query->where('name','like','%'.$this->q.'%');
                        });
                    });
                });
            });
        }

        $adjustments = $adjustments->paginate(10);
        return view('livewire.transaction.adjustment.index',[
            'adjustments'=>$adjustments
        ]);
    }
}
