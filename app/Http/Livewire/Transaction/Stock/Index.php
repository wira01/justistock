<?php

namespace App\Http\Livewire\Transaction\Stock;

use App\Http\Livewire\Extender\Ancestor;

use App\Models\Master\Models;
use App\Models\Master\DetailItem;
use App\Models\Master\Size;
use App\Models\Master\Item;

use Symfony\Component\HttpFoundation\Response;
use Gate;
use DB;

class Index extends Ancestor
{
    public $dateFrom,$dateTo,$sort = 'DESC';

    public function mount(){
        abort_if(Gate::denies('access_menu_stock'), Response::HTTP_FORBIDDEN, 'You Not Authorized');
    }

    public function sortBy($sort){        
        $this->sort = $sort === 'DESC'?'ASC':'DESC';
    }



    public function render()
    {
        $model = Models::select('id','name');
        $size = Size::select('id','name');
        $detail = DetailItem::select('id','size_id','qty','item_id');
        $item = Item::select('id','model_id','deleted_at');
        $stocks = DB::table('stocks')->select('model.id as model_id','model.name','items.id','size.name as size_name','detail.qty as detail_qty','detail.size_id',DB::raw('SUM(stocks.in) as in_qty'),DB::raw('SUM(stocks.out) as out_qty'))->LeftJoinSub($detail,'detail',function($join){
            $join->on('detail.id','=','stocks.detail_item_id');
        })->JoinSub($item,'items',function($join){
            $join->on('detail.item_id','=','items.id');
        })->JoinSub($model,'model',function($join){
            $join->on('model.id','=','items.model_id');
        })->JoinSub($size,'size',function($join){
            $join->on('size.id','=','detail.size_id');
        });

        if ($this->dateFrom) {
            $stocks->whereDate('stocks.created_at', '>=', date("Y-m-d", strtotime($this->dateFrom.' 00:00:00')));
        }
        if ($this->dateTo) {
            $stocks->whereDate('stocks.created_at', '<=', date("Y-m-d", strtotime($this->dateTo.' 23:59:59')));
        }

        if($this->dateFrom == '' && $this->dateTo == ''){
            $stocks->whereDate('created_at',now());
        }

        if($this->q){
            $stocks->where(function($query){
                $query->orWhere('model.name','like','%'.$this->q.'%')->orWhere('size.name','like','%'.$this->q.'%');
            });

        }

        
        if($this->sort){                              
            $stocks->orderByRaw('SUM(stocks.out) '.$this->sort);                      
        }
        

        $stocks = $stocks->whereNull('items.deleted_at')->groupBy('model.id','size.id')->orderBy('model.name')->paginate(10);



        
        return view('livewire.transaction.stock.index',[
            'stocks'=>$stocks
        ]);
    }
}
