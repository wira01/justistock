<?php

namespace App\Http\Livewire\Transaction\LembarKerja;

use App\Http\Livewire\Extender\Ancestor;

use App\Models\Master\DetailItem;
use App\Models\Master\Marketplace;
use App\Models\Transaction\LembarKerja;
use App\Models\Transaction\Stock;
use App\Models\Master\Customer;
use App\Models\DetailStock;

use Symfony\Component\HttpFoundation\Response;
use Gate;
use Auth;
use DB;
use Carbon\Carbon;

class Index extends Ancestor
{
    public $product,$product_text,$size,$size_text,$keterangan,$marketplace,$marketplace_text,$no_pesanan,$model,$model_text,$modal_price,$sales_price,$discount,$indexDetail,$detailTable = [],$qty,$contact,$offline = false,$dateKerja,$avaiableQty;

    public $dateFrom,$dateTo,$keterangan_filter,$marketplace_filter,$marketplace_filter_text;

    public function store(){
        $name = Auth::user()->username;
        $keterangan = '';
        $total = 0;

        $this->validate([            
            'dateKerja'=>'required',
            'keterangan'=>'required',
            'marketplace'=>'required',            
            'no_pesanan'=>'required',                                
            'detailTable'=>'required',            
        ]);

        try {
            DB::beginTransaction();
            if($this->offline){
                $customer = Customer::where('name','like','%'.$this->contact.'%')->first();
                if(!$customer){
                    Customer::create([
                    'name'=>$this->contact,
                    'created_by'=>$name
                ]);
                }
                
            }
            $lembar_kerja = LembarKerja::create([
                'no_pesanan'=>$this->no_pesanan,                        
                'tanggal'=>Carbon::parse($this->dateKerja),                        
                'keterangan'=>$this->keterangan,                                                        
                'marketplace_id'=>$this->marketplace,                                                        
                'contact'=>$this->contact,                                                                                                               
                'created_by'=>$name,           
            ]);   

            if($lembar_kerja){
                foreach($this->detailTable as $data){   
                    if($this->keterangan == 'jual'  || $this->keterangan == 'retur pembelian'){
                        $total = $data['qty']*$data['sales_price'];
                    }

                    if($this->keterangan == 'beli'  || $this->keterangan == 'retur penjualan'){
                        $total = $data['qty']*$data['modal_price'];
                    }

                    $lembar_kerja->detail()->create([
                        'lembar_kerja_id'=>$lembar_kerja->id,
                        'detail_item_id'=>$data['size'],
                        'qty'=>$data['qty'],
                        'discount'=>$data['discount']??0,
                        'modal_price'=>$data['modal_price'],
                        'sales_price'=>$data['sales_price'],
                        'total'=>$total,
                    ]);



                    $detail = DetailItem::find($data['size']);
                    

                     if($this->keterangan == 'jual'  || $this->keterangan == 'retur pembelian'){
                        DetailStock::create([
                            'detail_item_id'=>$data['size'],
                            'size'=>$detail->size->name,
                            'model'=>$detail->item->model->name,
                            'stok_awal'=>$detail->qty,
                            'stok_akhir'=>$detail->qty-$data['qty'],
                            'keterangan'=>$this->keterangan,
                            'created_at'=>Carbon::parse($this->dateKerja)
                        ]);

                        $detail->update([
                            'qty'=>$detail->qty-$data['qty']
                        ]);

                        
                     }

                     if($this->keterangan == 'beli'  || $this->keterangan == 'retur penjualan'){
                        DetailStock::create([
                            'detail_item_id'=>$data['size'],
                            'size'=>$detail->size->name,
                            'model'=>$detail->item->model->name,
                            'stok_awal'=>$detail->qty,
                            'stok_akhir'=>$detail->qty+$data['qty'],
                            'keterangan'=>$this->keterangan,
                            'created_at'=>Carbon::parse($this->dateKerja)
                        ]);

                        $detail->update([
                            'qty'=>$detail->qty+$data['qty']
                        ]);

                        
                     }

                     Stock::create([
                        'detail_item_id'=>$data['size'],
                        'out'=>
                            ($this->keterangan == 'jual'  || $this->keterangan == 'retur pembelian' && $data['qty'])?$data['qty']:0, 
                        'in'=>
                            ($this->keterangan == 'beli'  || $this->keterangan == 'retur penjualan' && $data['qty'])?$data['qty']:0, 
                        'created_by'=>$name, 
                     ]);
                }
            }

            DB::commit();

        $this->showCard = 'table';
        $this->resetData();        
        $this->emit('flash_message', 'Data has been saved!');

        } catch (\Exception $e) {
            $this->emit('flash_error', $e->getMessage());
            DB::rollBack();
        }
    }

    public function resetData(){
        $this->emit('resetDetail');
        $this->reset();
    }


    public function updatedSize($id){
        $item = DetailItem::find($id);
        if($item){
            $this->modal_price = $item->modal_price;
            $this->sales_price = $item->sales_price;
            $this->avaiableQty = $item->qty;
        }
    }

    public function updatedMarketplace($id){
        $market = Marketplace::find($id);
             
        if($market->type == 'offline'){
            $this->offline = true;
            $this->no_pesanan = getNoPesanan();
        }else{ 
            $this->offline = false;           
            $this->no_pesanan = null;
        }
    }

    public function addDetail(){
        

        $this->validate([            
            'model'=>'required',
            'keterangan'=>'required',
            'dateKerja'=>'required',
            'no_pesanan'=>'required',
            'marketplace'=>'required',            
            'product'=>'required',            
            'size'=>'required',            
            'qty'=>'required',            
        ]);

        $item = DetailItem::find($this->size);

        if($this->keterangan == 'jual'  || $this->keterangan == 'retur pembelian'){
            if($item && $item->qty < $this->qty){                      
                return $this->addError('qty','Qty melebihi batas stok');
            }
        }

        if($this->discount > $this->sales_price){
            return $this->addError('discount','Discount melebihi Price');
        }

        foreach($this->detailTable as $table){
            if($table['product'] == $this->product && $table['size'] == $this->size){
                return $this->addError('product','Produk dan Size sudah ada');
            }
        }

        $dataDetail = [
            'index'=>$this->indexDetail,
            'model'=>$this->model,
            'model_text'=>$this->model_text,
            'product'=>$this->product,
            'product_text'=>$this->product_text,
            'size'=>$this->size,
            'size_text'=>$this->size_text,
            'qty'=>$this->qty,            
            'discount'=>$this->discount,
            'modal_price'=>$this->modal_price,
            'sales_price'=>$this->sales_price??0,
            'delete'=>false,
        ];    
            array_push($this->detailTable,$dataDetail);
            $this->indexDetail += 1; 
        }

     public function deleteDetail($index){                  
        unset($this->detailTable[$index]);

    }   

    public function mount(){
        abort_if(Gate::denies('access_menu_lembar_kerja'), Response::HTTP_FORBIDDEN, 'You Not Authorized');         
    }
        

    public function render()
    {
        $lembar_kerjas = LembarKerja::orderByDesc('tanggal');

        if ($this->dateFrom) {
            $lembar_kerjas->whereDate('tanggal', '>=', date("Y-m-d", strtotime($this->dateFrom.' 00:00:00')));
        }
        if ($this->dateTo) {
            $lembar_kerjas->whereDate('tanggal', '<=', date("Y-m-d", strtotime($this->dateTo.' 23:59:59')));
        }

        if($this->dateFrom == '' && $this->dateTo == ''){
            $lembar_kerjas->whereDate('created_at',now());
        }

        if($this->keterangan_filter){
            $lembar_kerjas->where('keterangan',$this->keterangan_filter);
        }
        if($this->marketplace_filter){
            $lembar_kerjas->where('marketplace_id',$this->marketplace_filter);
        }

        if($this->q){
            $lembar_kerjas->where(function($query){
                $query->orWhere('no_pesanan','like','%'.$this->q.'%')
                ->orWhereHas('detail',function($query){
                $query->WhereHas('detailItem',function($query){
                    $query->WhereHas('item',function($query){
                        $query->where('name','like','%'.$this->q.'%')
                        ->orWhere('sku','like','%'.$this->q.'%')
                        ->orWhereHas('model',function($query){
                            $query->where('name','like','%'.$this->q.'%');
                        });
                    });
                });
                });
            });
        }

        $lembar_kerjas = $lembar_kerjas->paginate(10);
        

        
        return view('livewire.transaction.lembar-kerja.index',
            ['lembar_kerjas'=>$lembar_kerjas]
    );
    }
}
