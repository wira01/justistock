<?php

namespace App\Http\Livewire\Transaction\StockOpname;

use App\Http\Livewire\Extender\Ancestor;

use App\Models\Master\Models;
use App\Models\Master\DetailItem;
use App\Models\Master\Size;
use App\Models\Master\Item;
use App\Models\Master\Category;
use App\Models\Transaction\StockOpname;

use Symfony\Component\HttpFoundation\Response;
use Gate;
use DB;
use Auth;

class Index extends Ancestor
{
    public $dateFrom,$dateTo;   

    public $category_filter; 

    public function store($datas){
        $name = Auth::user()->username;
        
        try {
            DB::beginTransaction();                
                    StockOpname::create([
                        'no_opname'=>'S/O'.getNoPesanan(),
                        'detail_opname'=>json_encode($datas),
                        'created_by'=>$name
                    ]);
            DB::commit();
            $this->showCard = 'table';        
            $this->emit('flash_message', 'Data has been saved!');    
        } catch (\Exception $e) {
            DB::rollBack();
            $this->emit('flash_error', $e->getMessage());
        }
        
    }

    public function mount(){
        abort_if(Gate::denies('access_menu_stock_opname'), Response::HTTP_FORBIDDEN, 'You Not Authorized');                        
    }

    public function resetData(){

    }

    public function destroy(StockOpname $opname){
        $opname->delete();
        $this->emit('flash_message', 'Data has been deleted!');   
    }

    public function render()
    {        
        $stocks = [];
        
            
        $model = Models::select('id','name','category_id');
        $size = Size::select('id','name');
        $categories = Category::select('id','name');
        $detail = DetailItem::select('id','size_id','qty','item_id');        
        $stocks = DB::table('items')->select('model.id as model_id','model.name as model_name','categories.id as category_id','categories.name as category_name','size.id as size_id','size.name as size_name',DB::raw('SUM(detail.qty) as total'))->distinct()->JoinSub($detail,'detail',function($join){
            $join->on('detail.item_id','=','items.id');
        })->JoinSub($model,'model',function($join){
            $join->on('model.id','=','items.model_id');
        })->JoinSub($categories,'categories',function($join){
            $join->on('model.category_id','=','categories.id');
        })->JoinSub($size,'size',function($join){
            $join->on('size.id','=','detail.size_id');
        });

        if($this->category_filter){
            $category_filter = explode(',',$this->category_filter);
            $stocks->whereIn('categories.name',$category_filter);
        }

        $stocks = $stocks->whereNull('deleted')->groupBy('items.model_id','detail.size_id')->orderBy('categories.id')->get();
        
        $opnames = StockOpname::orderByDesc('id');

        if ($this->dateFrom) {
            $opnames->whereDate('created_at', '>=', date("Y-m-d", strtotime($this->dateFrom.' 00:00:00')));
        }
        if ($this->dateTo) {
            $opnames->whereDate('created_at', '<=', date("Y-m-d", strtotime($this->dateTo.' 23:59:59')));
        }

        if($this->dateFrom == '' && $this->dateTo == ''){
            $opnames->whereDate('created_at',now());
        }

        $opnames = $opnames->paginate(10);

        return view('livewire.transaction.stock-opname.index',[
            'stocks'=>$stocks,
            'opnames'=>$opnames
        ]);
    }
}
