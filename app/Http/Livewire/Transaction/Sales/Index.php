<?php

namespace App\Http\Livewire\Transaction\Sales;

use App\Http\Livewire\Extender\Ancestor;

use App\Models\Transaction\LembarKerja;
use App\Models\Transaction\DetailLembarKerja;
use App\Models\Master\Item;
use App\Models\Master\DetailItem;
use App\Models\Master\Models;
use App\Models\Master\Size;
use App\Models\Master\Marketplace;

use Symfony\Component\HttpFoundation\Response;
use Gate;
use DB;

class Index extends Ancestor
{
    public $dateFrom,$dateTo,$marketplace_filter_text,$marketplace_filter,$sales_status;

    public function mount(){
        abort_if(Gate::denies('access_menu_sales'), Response::HTTP_FORBIDDEN, 'You Not Authorized');         
    }

    public function complete($id){  
        $lembar_kerja = LembarKerja::find($id);      
        $lembar_kerja->update([
            'status'=>'SUCCESS'
        ]);

        $this->emit('flash_message', 'Orderan telah selesai!');
    }

    public function render()
    {
        $sales = LembarKerja::where('keterangan','jual')->with('detail')->withSum('detail','total')->whereNull('deleted_at')->orderByDesc('lembar_kerjas.id');

        if ($this->dateFrom) {
            $sales->whereDate('created_at', '>=', date("Y-m-d", strtotime($this->dateFrom.' 00:00:00')));
        }
        if ($this->dateTo) {
            $sales->whereDate('created_at', '<=', date("Y-m-d", strtotime($this->dateTo.' 23:59:59')));
        }

        if($this->dateFrom == '' && $this->dateTo == ''){
            $sales->whereDate('created_at',now());
        }

        if($this->marketplace_filter){
            $sales->where('marketplace_id',$this->marketplace_filter);
        }
        if($this->sales_status && $this->sales_status == 1){
            $sales->where('status','SUCCESS');
        }else{
            $sales->whereNull('status');
        }

        if($this->q){
            $sales->where(function($query){
                $query->where('no_pesanan','like','%'.$this->q.'%');                
            });
        }
        $sales = $sales->paginate(10);
        
        
        return view('livewire.transaction.sales.index',[
            'sales'=>$sales
        ]);
    }
}
