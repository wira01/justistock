<?php

namespace App\Http\Livewire\Transaction\Purchase;

use App\Http\Livewire\Extender\Ancestor;

use App\Models\Transaction\LembarKerja;

use Symfony\Component\HttpFoundation\Response;
use Gate;
use DB;

class Index extends Ancestor
{
    public $dateFrom,$dateTo,$marketplace_filter_text,$marketplace_filter;

    public function mount(){
        abort_if(Gate::denies('access_menu_purchase'), Response::HTTP_FORBIDDEN, 'You Not Authorized');         
    }

    public function render()
    {
        $purchases = LembarKerja::where('keterangan','beli')->orderByDesc('id');

        if ($this->dateFrom) {
            $purchases->whereDate('created_at', '>=', date("Y-m-d", strtotime($this->dateFrom.' 00:00:00')));
        }
        if ($this->dateTo) {
            $purchases->whereDate('created_at', '<=', date("Y-m-d", strtotime($this->dateTo.' 23:59:59')));
        }

        if($this->dateFrom == '' && $this->dateTo == ''){
            $purchases->whereDate('created_at',now());
        }

        if($this->marketplace_filter){
            $purchases->where('marketplace_id',$this->marketplace_filter);
        }

        if($this->q){
            $purchases->where(function($query){
                $query->where('no_pesanan','like','%'.$this->q.'%');                
            });
        }
        $purchases = $purchases->with('detail')->withSum('detail','total')->paginate(10);        

        return view('livewire.transaction.purchase.index',[
            'purchases'=>$purchases
        ]);
    }
}
