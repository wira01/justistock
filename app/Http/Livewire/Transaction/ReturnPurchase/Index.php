<?php

namespace App\Http\Livewire\Transaction\ReturnPurchase;

use App\Http\Livewire\Extender\Ancestor;

use App\Models\Transaction\LembarKerja;

use Symfony\Component\HttpFoundation\Response;
use Gate;
use DB;

class Index extends Ancestor
{
    public $dateFrom,$dateTo,$marketplace_filter_text,$marketplace_filter;


    public function mount(){
        abort_if(Gate::denies('access_menu_return_purchase'), Response::HTTP_FORBIDDEN, 'You Not Authorized');         
    }

    public function render()
    {
        $return_purchases = LembarKerja::where('keterangan','retur pembelian')->orderByDesc('id');

        if ($this->dateFrom) {
            $return_purchases->whereDate('created_at', '>=', date("Y-m-d", strtotime($this->dateFrom.' 00:00:00')));
        }
        if ($this->dateTo) {
            $return_purchases->whereDate('created_at', '<=', date("Y-m-d", strtotime($this->dateTo.' 23:59:59')));
        }

        if($this->dateFrom == '' && $this->dateTo == ''){
            $return_purchases->whereDate('created_at',now());
        }

        if($this->marketplace_filter){
            $return_purchases->where('marketplace_id',$this->marketplace_filter);
        }

        if($this->q){
            $return_purchases->where(function($query){
                $query->where('no_pesanan','like','%'.$this->q.'%');                
            });
        }
        $return_purchases = $return_purchases->paginate(10);
        return view('livewire.transaction.return-purchase.index',[
            'return_purchases'=>$return_purchases
        ]);
    }
}
