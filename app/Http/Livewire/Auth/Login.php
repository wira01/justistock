<?php

namespace App\Http\Livewire\Auth;

use Livewire\Component;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Hash;

class Login extends Component
{
    public $username,$password;


    public function login(){
        $validate = $this->validate([
            'username'=>'required',
            'password'=>'required',
        ]);      

        $user = User::where('username',$this->username)->first();
        if($user){
            if($user && Hash::check($this->password,$user->password)){
                Auth::login($user);  
                $this->emit('flash_message');
            }else{
                $this->addError('password','Password Invalid');
            }
        }else{
            $this->addError('username','Username not found');
        } 

        // if ($validate->fails()) {                   
        //     dd($validate->fails());
        // }else{    
            
        //     if($user && Hash::check($this->password,$user->password)){
        //     Auth::login($user);  

        //     $this->emit('flash_message');
        //     }
        // }
    }

    public function mount(){
        abort_if(Auth::check(),403);
    }

    public function render()
    {            
        return view('livewire.auth.login')->layout('livewire.layout.plain');
    }
}
