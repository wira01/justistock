<?php

namespace App\Http\Livewire\Extender;

use Livewire\Component;
use Livewire\WithPagination;

use Gate;
use Str;
use Carbon\Carbon;

class Ancestor extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $q;
    public $showCard = 'table';
    protected $queryString = [
        'q'
    ];

    public function setSlug($item = ''){
        return Str::slug($item,'_');
    }

    public function updatedCard($card){
        $this->showCard = $card;
    }
       
}
