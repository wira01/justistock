<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\Master\Role;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpFoundation\Response;

class CredentialAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // dd(1);
        $user = Auth::user(); 
      if ($user) {        
        // $userrole = User::where('id_user', $user->id_user)->pluck('id_role')->toArray();
        $userrole = Cache::remember('userrole'.$user->id, 60*60*24, function () use ($user) {
            return User::where('id', $user->id)->pluck('role_id')->toArray();
        });
        
          // $roles            = Role::with('permissions')->get();
          $roles = Cache::remember('rolespermission', 60*60*24, function () {
              return Role::with('permissions')->get();
          });
          
          $permissionsArray = [];

          foreach ($roles as $role) {
              foreach ($role->permissions as $permissions) {
                  $permissionsArray[$permissions->name][] = $role->id;
              }
          }          

          $a = [];
          foreach ($permissionsArray as $title => $roles) {
              Gate::define($title, function (\App\Models\User $user) use ($roles, $userrole) {
                
                  // return count(array_intersect($user->role->pluck('id_role')->toArray(), $roles)) > 0;                
                  return count(array_intersect($userrole, $roles)) > 0;
                  // return 1;
              });
                       
          }
          
      }

        return $next($request);
    }
}
