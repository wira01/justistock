<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Response;

use App\Models\Master\Menu;
use App\Models\Master\Category;
use App\Models\Master\Models;
use App\Models\Master\Marketplace;
use App\Models\Master\Size;
use App\Models\Master\Item;
use App\Models\Master\DetailItem;

use App\Models\Transaction\LembarKerja;


use Auth;
use DB;
use PDF;

class ResourceController extends Controller{
	public $search;
	public $limit = 15;

	public function getMenu(Request $request)
    {
        $search = $request->search;        

        if ($search == '') {
            $datas = Menu::orderby('name', 'asc')->select('id', 'name')->limit($this->limit)->get();
        } else {
            $datas = Menu::orderby('name', 'asc')->select('id', 'name')->where('name', 'like', '%' . $search . '%')->limit($this->limit)->get();
        }

        $response = array();
        foreach ($datas as $data) {
            $response[] = array(
                "id"  => $data->id,
                "text" => $data->name
            );
        }
        

        return json_encode($response);
    }

    public function getCategory(Request $request)
    {
        $search = $request->search;        

        if ($search == '') {
            $datas = Category::orderby('name', 'asc')->select('id', 'name')->limit($this->limit)->get();
        } else {
            $datas = Category::orderby('name', 'asc')->select('id', 'name')->where('name', 'like', '%' . $search . '%')->limit($this->limit)->get();
        }

        $response = array();
        foreach ($datas as $data) {
            $response[] = array(
                "id"  => $data->id,
                "text" => $data->name
            );
        }
        

        return json_encode($response);
    }

    public function getModel(Request $request)
    {
        $search = $request->search;        

        if ($search == '') {
            $datas = Models::orderby('name', 'asc')->select('id', 'name')->limit($this->limit)->get();
        } else {
            $datas = Models::orderby('name', 'asc')->select('id', 'name')->where('name', 'like', '%' . $search . '%')->limit($this->limit)->get();
        }

        $response = array();
        foreach ($datas as $data) {
            $response[] = array(
                "id"  => $data->id,
                "text" => $data->name
            );
        }
        

        return json_encode($response);
    }

    public function getMarketplace(Request $request)
    {
        $search = $request->search;        

        if ($search == '') {
            $datas = Marketplace::orderby('name', 'asc')->select('id', 'name')->limit($this->limit)->get();
        } else {
            $datas = Marketplace::orderby('name', 'asc')->select('id', 'name')->where('name', 'like', '%' . $search . '%')->limit($this->limit)->get();
        }

        $response = array();
        foreach ($datas as $data) {
            $response[] = array(
                "id"  => $data->id,
                "text" => $data->name
            );
        }
        

        return json_encode($response);
    } 

    public function getSize(Request $request)
    {
        $search = $request->search;        

        if ($search == '') {
            $datas = Size::orderby('name', 'asc')->select('id', 'name')->limit($this->limit)->get();
        } else {
            $datas = Size::orderby('name', 'asc')->select('id', 'name')->where('name', 'like', '%' . $search . '%')->limit($this->limit)->get();
        }

        $response = array();
        foreach ($datas as $data) {
            $response[] = array(
                "id"  => $data->id,
                "text" => $data->name
            );
        }
        

        return json_encode($response);
    } 

    public function getItem(Request $request)
    {
        $search = $request->search;        

        if ($search == '') {
            $datas = Item::orderby('name', 'asc')->select('id', 'name')->whereNull('deleted')->limit($this->limit)->get();
        } else {
            $datas = Item::orderby('name', 'asc')->select('id', 'name')->whereNull('deleted')->where('name', 'like', '%' . $search . '%')->limit($this->limit)->get();
        }

        $response = array();
        foreach ($datas as $data) {
            $response[] = array(
                "id"  => $data->id,
                "text" => $data->name
            );
        }
        

        return json_encode($response);
    }   

    public function getSizeByItem(Request $request)
    {
        $search = $request->search;        
        $id = $request->product;        

        if ($search == '') {
            $datas = DetailItem::where('item_id',$id)->limit($this->limit)->get();
        } else {
            $datas = DetailItem::with('size')->where('item_id',$id)->whereHas('size',function($query){
                $query->where('name','like', '%' . $search . '%');
            })->limit($this->limit)->get();
        }
        $response = array();
        foreach ($datas as $data) {
            $response[] = array(
                "id"  => $data->id,
                "text" => $data->size->name
            );
        }
        

        return json_encode($response);
    }     

    public function getItemByModel(Request $request)
    {
        $search = $request->search;        
        $id = $request->model;                    

        if ($search == '') {
            $datas = Item::orderby('name', 'asc')->select('id', 'name')->where('model_id',$id)->whereNull('deleted')->limit($this->limit)->get();
        } else {
            $datas = Item::orderby('name', 'asc')->select('id', 'name')->where('model_id',$id)->whereNull('deleted')->where('name', 'like', '%' . $search . '%')->limit($this->limit)->get();
        }
        $response = array();
        foreach ($datas as $data) {
            $response[] = array(
                "id"  => $data->id,
                "text" => $data->name
            );
        }
        

        return json_encode($response);
    }   

    public function generateStockOpnamePDF(){

        $document_name = 'Template Stock Opname';
        $model = Models::select('id','name','category_id');
        $size = Size::select('id','name');
        $categories = Category::select('id','name');
        $detail = DetailItem::select('id','size_id','qty','item_id');   

        $stocks = DB::table('items')->select('model.id as model_id','model.name as model_name','categories.id as category_id','categories.name as category_name','size.id as size_id','size.name as size_name',DB::raw('SUM(detail.qty) as total'))->distinct()->JoinSub($detail,'detail',function($join){
            $join->on('detail.item_id','=','items.id');
        })->JoinSub($model,'model',function($join){
            $join->on('model.id','=','items.model_id');
        })->JoinSub($categories,'categories',function($join){
            $join->on('model.category_id','=','categories.id');
        })->JoinSub($size,'size',function($join){
            $join->on('size.id','=','detail.size_id');
        });

        $stocks = $stocks->whereNull('deleted_at')->groupBy('items.model_id','detail.size_id')->orderBy('categories.id')->get();

        $pdf = PDF::loadView('pdf.downloads.opname', compact('stocks')); 
        $timeNow = date('Y-m-d H:i:s');       
        return $pdf->download($document_name . ' - ' . $timeNow . '.pdf');
    }

    public function generateSalesInvoice(Request $request){

        $sales = LembarKerja::withSum('detail','total')->findOrFail($request->id);
                
        $pdf = PDF::loadView('livewire.transaction.sales.pdf', compact(['sales']))->setPaper('A5','landscape');  
        return $pdf->stream();                
    }

    public function generatePurchase(Request $request){

        $purchase = LembarKerja::withSum('detail','total')->findOrFail($request->id);        
                
        $pdf = PDF::loadView('livewire.transaction.purchase.pdf', compact(['purchase']))->setPaper('A4','landscape');  
        return $pdf->stream();                
    }

    public function generateReturnSales(Request $request){

        $return_sales = LembarKerja::findOrFail($request->id);
                
        $pdf = PDF::loadView('livewire.transaction.return-sales.pdf', compact(['return_sales']))->setPaper('A4','portrait');  
        return $pdf->stream();                
    }
    
}