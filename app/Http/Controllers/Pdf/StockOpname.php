<?php

namespace App\Http\Controllers\Pdf;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use PDF;
use App\Models\Transaction\StockOpname as Opname;

class StockOpname extends Controller
{
    public function index($id){
        $opname = Opname::findOrFail($id);                        
        $pdf = PDF::loadView('pdf.opname', compact(['opname']))
            ->setPaper('a4','portrait');      
        return $pdf->stream();
    }
}
