<?php

namespace App\Imports;

use App\Models\Transaction\LembarKerja;
use App\Models\Master\Item;
use App\Models\Master\Models;
use App\Models\Master\Marketplace;
use App\Models\Master\Size;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\Importable;



use Auth;
use DB;

class LembarKerjaImport implements 
    ToCollection,
    WithHeadingRow
{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        return $rows;
    }   
    
}
