<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailStock extends Model
{
    protected $guarded = [];

    public function detailItem()
    {
        return $this->belongsTo(\App\Models\Master\DetailItem::class);
    }
}
