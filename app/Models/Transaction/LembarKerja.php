<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LembarKerja extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public function detail()
    {
        return $this->hasMany(DetailLembarKerja::class);
    }

    public function marketplace()
    {
        return $this->belongsTo(\App\Models\Master\Marketplace::class)->withTrashed();
    }
}
