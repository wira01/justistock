<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockOpname extends Model
{
    use HasFactory;

    protected $fillable = [
        'no_opname',
        'detail_opname',        
        'created_by',
        'updated_by', 
    ];
}
