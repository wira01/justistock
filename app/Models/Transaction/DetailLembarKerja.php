<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class DetailLembarKerja extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function detailItem()
    {
        return $this->belongsTo(\App\Models\Master\DetailItem::class);
    }
}
