<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Adjustment extends Model
{
    use HasFactory,SoftDeletes;

    protected $guarded = [];
    
    public function detail()
    {
        return $this->hasMany(DetailAdjustment::class);
    }
}
