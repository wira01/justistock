<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailAdjustment extends Model
{
    protected $guarded = [];

    use HasFactory;

    public function detailItem()
    {
        return $this->belongsTo(\App\Models\Master\DetailItem::class);
    }

}
