<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [        
        'title',
        'created_by',
        'updated_by',       
    ];

    public function permissions() {
        return $this->belongsToMany(Permission::class, PermissionRole::class, 'role_id');
    }


}
