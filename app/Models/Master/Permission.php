<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [        
        'name',
        'permission',
        'menu_id',       
        'created_by',
        'updated_by',   
    ];

    public function menu(){
        return $this->belongsTo(Menu::class);
    }

    public function roles() {
        return $this->belongsToMany(Role::class, PermissionRole::class, 'id_permission', 'id_role');
    }
}
