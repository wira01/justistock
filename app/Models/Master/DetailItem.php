<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class DetailItem extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function size(){
        return $this->belongsTo(Size::class);
    }

    public function item(){
        return $this->belongsTo(Item::class);
    }
}
