<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Item extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public function detail()
    {
        return $this->hasMany(DetailItem::class);
    }

    public function model(){
        return $this->belongsTo(Models::class)->withTrashed();
    }

}
