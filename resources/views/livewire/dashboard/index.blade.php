<div>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v3</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="d-flex justify-content-end">
            <div class="form-group">
                <select class="form-control" name="filtering_by" id="filtering_by" wire:model="filtering_by">
                    <option value="Today">Today</option>
                    <option value="Weekly">Weekly</option>
                    <option value="Monthly">Monthly</option>                    
                </select>
            </div>
        </div> 
        <div class="row">
          <div class="col-lg-6">
            <div class="card">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Total Penjualan</h3>
                </div>
              </div>
              <div class="card-body">
                <h2 class="title">{{formatRupiah($sales)}}</h2>
              </div>
            </div>
            <!-- /.card -->

            <div class="card" style="min-height:300px">
              <div class="card-header border-0">
                <h3 class="card-title">Penjualan by Marketplace</h3>                
              </div>
              <div class="card-body table-responsive p-0">
                <table class="table table-striped table-valign-middle">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Qty</th>
                    
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($marketplaces as $key => $marketplace)
                        <tr>
                            <td>{{$marketplace->name}}</td>
                            <td>{{$marketplace->total_qty}}</td>
                        </tr>
                    @endforeach
                  
                  </tbody>
                </table>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
          <div class="col-lg-6">
            <div class="card">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Total Pembelian</h3>
                </div>
              </div>
              <div class="card-body">               
                <h2 class="title">{{formatRupiah($purchase)}}</h2>
              </div>
            </div>
            <!-- /.card -->
            <div class="card" style="min-height:300px">
              <div class="card-header border-0">
                <h3 class="card-title">Produk Mencapai Minimum Qty</h3>                
              </div>
              <div class="card-body table-responsive p-0">
                <table class="table table-striped table-valign-middle">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Size</th>
                    <th>Qty</th>                    
                    <th>Minimum</th>
                  </tr>
                  </thead>
                  <tbody>

                  @foreach($products as $key => $product)
                    <tr>
                    <td>                      
                      <strong>{{$product->item->sku}}</strong> - {{$product->item->model->name}} - {{$product->item->name}}
                    </td>
                    <td>
                      {{$product->size->name}}
                    </td>
                    <td>{{$product->qty}}</td>
                    <td>{{$product->min_qty}}</td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
                <div class="float-right my-3 pr-3">
                {{$products->links()}}
            </div> 
              </div>
            </div>
            
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-6">
                
            <div class="card" style="min-height:300px">
              <div class="card-header border-0">
                <h3 class="card-title">Pembayaran belum lunas</h3>                
              </div>
              <div class="card-body table-responsive p-0">
                <table class="table table-striped table-valign-middle">
                  <thead>
                  <tr>
                    <th>No Pesanan</th>
                    <th>Customer</th>
                    <th>Tanggal</th>
                  </tr>
                  </thead>
                  <tbody>

                  @foreach($sales_by_payment as $key => $payment)
                    <tr>
                    <td>                      
                      {{$payment->no_pesanan}}
                    </td>
                    <td>
                      {{$payment->contact??$payment->marketplace->name}}
                    </td>
                    <td>
                        {{Carbon\Carbon::parse($payment->tanggal)->format('Y-m-d')}}
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
                <div class="float-right my-3 pr-3">
                {{$sales_by_payment->links()}}
            </div> 
              </div>
            </div>
            </div>
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
