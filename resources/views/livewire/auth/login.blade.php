<div>
    <div class="login-box">
  <div class="login-logo">
    <a href="{{route('home')}}"><img width="300" src="{{url('images/logo_justi.png')}}" alt="logo justiboy"></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form action="" wire:submit.prevent="login">
        <div class="input-group mb-3">
          <input type="text" class="form-control @error('username') is-invalid @enderror" id="username"  placeholder="Username" wire:model.defer="username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          @error('username')
            <div class="invalid-feedback"> {{$message}} </div>
          @enderror
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control @error('password') is-invalid @enderror" wire:model.defer="password" placeholder="Password">          
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          @error('password')
            <div class="invalid-feedback"> {{$message}} </div>
          @enderror
        </div>
        <div class="row">
          <div class="col-8">
            
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
     
      <!-- /.social-auth-links -->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
@push('scripts')
    <script>
        window.livewire.on('flash_message', (message) => {
            Swal.fire(
                'Login Success!',
            message,
                'success'
            ).then((result) => {
                window.location.href = "{{route('home')}}";
            });        
        });
    </script>
@endpush
</div>
