
<div id="wrapper" x-data="{card_name:  @entangle('showCard'),...test,filtering_by:false}">
   @section('title')
    Justiboy - Laporan Pembelian
   @endsection
   @push('styles')
   <style type="text/css">
    .table th{
        vertical-align: middle!important;
    }
   </style>
   @endpush
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Laporan Pembelian</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Laporan Pembelian</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card" x-show.transition.origin.top.left="card_name == 'table'">
        
        <div class="card-body p-0">
          <div class="row mt-3">
            <div class="col-md-12">
            <div class="card">                        
              <div class="card-body">
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row align-items-center">
                            <label for="q" class="col-sm-2 col-form-label">Date :</label> 
                          <div class="input-group col-sm-8 py-3" wire:ignore>      
                            <x-input.date class="mr-3" id="dateFrom" placeholder="{{now()->format('Y-m-d')}}"/>
                            <x-input.date class="mr-3" id="dateTo" placeholder="{{now()->format('Y-m-d')}}"/>
                           </div>
                           </div>
                           <div class="form-group row">
                            <label for="marketplace_filter" class="col-sm-2 col-form-label">Marketplace :</label>
                            <div class="col-md-8">
                            <x-input.select2 placeholder="Pilih Marketplace" id="marketplace_filter" route="res.marketplace" name="marketplace_filter"/>
                        </div>
                      </div>  
                  </div>
                  
              </div>
          </div>   
          </div>           
          </div>
          </div>
         @if($dateFrom || $dateTo)
          <table class="table table-bordered text-center">
              <thead>
                  <tr>
                      
                      <th rowspan="2" style="width: 1%">
                          #
                      </th>                      
                      <th rowspan="2" style="width:2%">
                          Tanggal
                      </th>   
                      <th rowspan="2" style="width:1%">
                          Marketplace
                      </th>
                      <th rowspan="2" style="width:3%">
                          No Pesanan
                      </th>
                      <th rowspan="2" style="width:1%">
                          Supplier
                      </th>
                       <th rowspan="2" style="width:3%">
                          Total
                      </th>
                      <th colspan="7" style="width:60%">Detail</th>                      
                  </tr>
                  <tr>
                      <th style="width: 3%;">
                          SKU
                      </th>
                      <th style="width: 2%;">
                          Model
                      </th>
                      <th style="width: 5%;">
                          Produk
                      </th>
                      <th style="width: 1%;">
                          Size
                      </th>
                      <th style="width: 1%;">
                          Qty
                      </th>
                      <th style="width: 3%;">
                          Price
                      </th>
                      <th style="width: 3%;">
                          Total
                      </th>
                  </tr>                                  
              </thead>
              <tbody>
                @php
                    $total = 0
                @endphp
                @foreach($purchases as $key => $purchase)
                    @php
                        $rowspan = $purchase->detail->count();                                            
                    @endphp
                    @php
                            $total += $purchase->detail_sum_total;
                        @endphp
                    @foreach($purchase->detail as $k => $kerja)
                        
                        <tr>
                            @if($k == 0)
                                <td rowspan="{{$rowspan}}">{{$key+1}}</td>
                                <td rowspan="{{$rowspan}}">{{Carbon\Carbon::parse($purchase->tanggal)->format('Y-m-d')}}</td>
                                <td rowspan="{{$rowspan}}">{{$purchase->marketplace->name}}</td>
                                <td rowspan="{{$rowspan}}" style="word-break:break-all;">{{$purchase->no_pesanan}}</td>
                                <td rowspan="{{$rowspan}}">{{$purchase->contact??$purchase->marketplace->name}}</td>
                                <td rowspan="{{$rowspan}}">{{formatRupiah($purchase->detail_sum_total)}}</td>
                                <td>{{$kerja->detailItem->item->sku}}</td>  
                                <td>{{$kerja->detailItem->item->model->name}}</td>  
                                <td style="word-break:break-all;">{{$kerja->detailItem->item->name}}</td>                                   
                                <td>{{$kerja->detailItem->size->name}}</td>  
                                <td>{{$kerja->qty}}</td>           
                                <td>{{formatRupiah($kerja->modal_price)}}</td>  
                                <td>{{formatRupiah($kerja->total)}}</td>  
                            @else
                                <td>{{$kerja->detailItem->item->sku}}</td>  
                                <td>{{$kerja->detailItem->item->model->name}}</td>  
                                <td>{{$kerja->detailItem->item->name}}</td>                                   
                                <td>{{$kerja->detailItem->size->name}}</td>  
                                <td>{{$kerja->qty}}</td> 
                                <td>{{formatRupiah($kerja->modal_price)}}</td>                                  
                                <td>{{formatRupiah($kerja->total)}}</td>                                  
                            @endif
                        </tr>
                    @endforeach
                @endforeach
                  
              </tbody>
              <tfoot>
                  <tr>
                      <td colspan="2">Total Purchase</td>
                      <td colspan="10">{{formatRupiah($total)}}</td>
                  </tr>
              </tfoot>     
          </table>  
            <div class="p-3 my-3">
               <button class="btn btn-primary mr-2" type="button" wire:click.prevent="generatePurchaseReport('{{$dateFrom}}','{{$dateTo}}')">Report per Date</button><button class="btn btn-primary" type="button" wire:click.prevent="generatePurchaseReportDetail('{{$dateFrom}}','{{$dateTo}}','{{$marketplace_filter}}')">Report Detail</button>
           </div> 
          @endif         
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->


    </section>
    <!-- /.content -->

@push('scripts')

    <script type="text/javascript">
        
        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        const test = {
            getData(id){
                this.card_name = 'form'
                @this.call('getData',id)
            },
            cancel(){
                this.card_name = 'table'
                @this.call('resetData');
                
            }
        }

         $("#model").select2({            
            allowClear: true,
            placeholder: "Pilih Produk",
            ajax: {
                dataType: "json",
                url: "{{route('res.model')}}",
                delay: 300,
                data: function (params) {
                    return {
                        _token: CSRF_TOKEN,
                        search: params.term,
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data,
                    };
                },
            },
        }).on('select2:select', function (evt) {
                    var text = $("#model option:selected").text();
                    var id = $("#model option:selected").val();
                    @this.set('model', id);
                    @this.set('model_text', text);
                    Livewire.emit('setProduct',id);
                });


        window.livewire.on('setProduct', (model) => {
            $('#product').val('').trigger('change');
            $('#size').val('').trigger('change');
            $("#product").select2({
            allowClear: true,
            placeholder: "Pilih Produk",
            ajax: {
                dataType: "json",
                url: "{{route('res.item_by_model')}}",
                delay: 300,
                data: function (params) {
                    return {
                        _token: CSRF_TOKEN,
                        search: params.term,
                        model
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data,
                    };
                },
            },
        }).on('select2:select', function (evt) {
                    var text = $("#product option:selected").text();
                    var id = $("#product option:selected").val();
                    @this.set('product', id);
                    @this.set('product_text', text);
                    Livewire.emit('setSize',id);
                });

                });


        window.livewire.on('setSize', (product) => {            
            $("#size").select2({
            allowClear: true,
            placeholder: "Pilih Size",
            ajax: {
                dataType: "json",
                url: "{{route('res.size_by_item')}}",
                delay: 300,
                data: function (params) {
                    return {
                        _token: CSRF_TOKEN,
                        search: params.term,
                        product
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data,
                    };
                },
            },
        }).on('select2:select', function (evt) {
                    var text = $("#size option:selected").text();
                    var id = $("#size option:selected").val();
                    @this.set('size', id);                  
                    @this.set('size_text', text);                  
                });
        });
        

        function deleteData(id){
            Swal.fire({
        title: 'Yakin dihapus?',
        text: 'Data akan dihapus!',
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Delete!'
    }).then((result) => {
        if (result.value) {
            @this.call('destroy',id)
            responseAlert({title: session('message'), type: 'success'});
        } else {
            responseAlert({
                title: 'Operation Cancelled!',
                type: 'success'
            });
        }
    });
        }

        window.livewire.on('flash_message', (message) => {
            Swal.fire(
                'Success!',
            message,
                'success'
            )        
        });

        window.livewire.on('flash_error', (message) => {
            Swal.fire(
                'Oops...!', 
            message,
                'error'
            )
        });
        
    </script>
@endpush
</div>