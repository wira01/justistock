
<div id="wrapper" x-data="{card_name:  @entangle('showCard'),...test}">
   @section('title')
    Justiboy - Import Item
   @endsection
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Import Item</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Import Item</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card" x-show.transition.origin.top.left="card_name == 'table'">
        <div class="card-header">
          <h3 class="card-title">Import Item</h3>          
        </div>
        <div class="card-body">   
        <form action="" wire:submit.prevent="importNow">
            @if(isset($errors) && $errors->any())
                <div class="alert alert-danger">
                Error: {{$errors->first()}}
                </div>
            @endif
          <div class="form-group">                    
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="import" wire:model.defer="import">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                    </div>                   
                  </div>   
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">Import</button>
                  </div>      
        </form>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->

@push('scripts')

    <script type="text/javascript">
        
        
        const test = {
            getData(id){
                this.card_name = 'form'
                @this.call('getData',id)
            },
            cancel(){
                this.card_name = 'table'
                @this.call('resetData');
            }
        }

        function deleteData(id){
            Swal.fire({
        title: 'Yakin dihapus?',
        text: 'Data akan dihapus!',
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Delete!'
    }).then((result) => {
        if (result.value) {
            @this.call('destroy',id)
            responseAlert({title: session('message'), type: 'success'});
        } else {
            responseAlert({
                title: 'Operation Cancelled!',
                type: 'success'
            });
        }
    });
        }

        window.livewire.on('flash_message', (message) => {
            Swal.fire(
                'Success!',
            message,
                'success'
            ).then(e=>{
                window.location.href = "{{route('items')}}";
            })        
        });

        window.livewire.on('flash_error', (message) => {
            Swal.fire(
                'Oops...!', 
            message,
                'error'
            )
        });
        
    </script>
@endpush
</div>