
<div id="wrapper" x-data="{card_name:  @entangle('showCard'),...test,filtering_by : false}">
   @section('title')
    Justiboy - Items
   @endsection
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Items</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Items</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card" x-show.transition.origin.top.left="card_name == 'table'">
        <div class="card-header align-items-center">
          <h3 class="card-title">Filter By : <button type="button" class="btn btn-tool" @click="filtering_by = !filtering_by">
              <i class="fas fa-filter" :class="filtering_by == true?'text-primary':''"></i>
            </button></h3>

          <div class="card-tools">
            <a href="{{route('res.generateStockOpnamePDF')}}" class="btn btn-secondary mr-2" >Template Opname</a>
            <a href="{{route('import.item')}}" class="btn btn-success mr-2">Import</a>
            <button type="button" class="btn btn-primary" @click="card_name = 'form'">
              <span>Add Items</span>
            </button>                           
          </div>
        </div>
        <div class="card-body p-0">
          <div class="row mt-3">
            <div class="col-md-12">
            <div class="card">                        
              <div class="card-body" x-show.transition.origin.top.left="filtering_by == true" x-cloak>
                  <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group row">
                            <label for="sku_filter" class="col-sm-5 col-form-label">SKU</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control @error('sku_filter') is-invalid @enderror" id="sku_filter" placeholder="SKU" wire:model="sku_filter">
                            </div>
                      </div>
                      <div class="form-group row">
                            <label for="q" class="col-sm-5 col-form-label">Name</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control @error('q') is-invalid @enderror" id="q" placeholder="Name" wire:model="q">
                            </div>
                      </div>
                          <div class="form-group row">
                            <label for="category_filter" class="col-sm-5 col-form-label">Category :</label>
                            <div class="col-md-7">
+                                <x-input.select2 placeholder="Pilih Category" id="category_filter" route="res.categories" name="category_filter"/>
                             </div>
                            </div>
                      </div> 
                      <div class="form-group row">
                            <label for="model_filter" class="col-sm-5 col-form-label">Model :</label>
                            <div class="col-md-7">
                            <x-input.select2 placeholder="Pilih Model" id="model_filter" route="res.model" name="model_filter"/>
                        </div>
                      </div> 
                      
                      <div class="form-group row">
                            <label for="size_filter" class="col-sm-5 col-form-label">Size :</label>
                            <div class="col-md-7">
                            <x-input.select2 placeholder="Pilih Size" id="size_filter" route="res.size" name="size_filter"/>
                        </div>
                      </div> 
                       
                      </div>
                      <div class="offset-md-2">
                      </div>
                      <div class="col-md-4">
                        <div class="form-group row">
                            <label for="qty_filter" class="col-sm-5 col-form-label">Qty :</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <select name="qty_cacl" class="form-control" id="" wire:model="qty_cacl">
                                            <option value="">Pilih</option>
                                            <option value="=">=</option>
                                            <option value=">">></option>
                                            <option value="<"><</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control @error('qty_filter') is-invalid @enderror" id="qty_filter" placeholder="Qty" wire:model="qty_filter">
                                    </div>
                                </div>
                            </div>
                      </div>
                          <div class="form-group row">
                            <label for="modal_price_filter" class="col-sm-5 col-form-label">Modal Price :</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <select name="modal_cacl" class="form-control" id="" wire:model="modal_cacl">
                                            <option value="">Pilih</option>
                                            <option value="=">=</option>
                                            <option value=">">></option>
                                            <option value="<"><</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control @error('modal_price_filter') is-invalid @enderror" id="modal_price_filter" placeholder="Modal Price" wire:model="modal_price_filter">
                                    </div>
                                </div>
                            </div>
                      </div>
                      <div class="form-group row">
                            <label for="sales_price_filter" class="col-sm-5 col-form-label">Sales Price :</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <select name="sales_cacl" class="form-control" id="" wire:model="sales_cacl">
                                            <option value="">Pilih</option>
                                            <option value="=">=</option>
                                            <option value=">">></option>
                                            <option value="<"><</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control @error('sales_price_filter') is-invalid @enderror" id="sales_price_filter" placeholder="Sales Price" wire:model="sales_price_filter">
                                    </div>
                                </div>
                            </div>
                      </div>
                      
                      </div>
                  </div>
                  
              </div>
          </div>   
          </div>           
          </div>
          <table class="table">
              <thead>
                  <tr>
                      
                      <th style="width: 1%">
                          #
                      </th>
                      <th style="width: 10%">
                          Image
                      </th>
                      <th style="width: 10%">
                          Name
                      </th> 
                      <th style="width: 10%">
                          Category
                      </th>
                      <th style="width: 10%">
                          Model
                      </th>                      
                      <th style="width: 10%">
                          Warehouse
                      </th>                         
                      <th style="width: 1%">
                          Unit
                      </th>  
                      <th style="width: 1%">
                          Qty
                      </th>   
                      <th style="width: 10%">
                          Action
                      </th>
                  </tr>
              </thead>
              <tbody>
                @foreach($items as $key => $item)
                    <tr wire:key="{{$loop->index}}">
                        <td>{{$key+1}}</td>
                        <td>@if($item->images) <img width="100" x-on:click="openModal('{{$item->images}}')" src="{{ url('storage/'.$item->images) }}" alt=""> @else no image @endif</td>
                        <td>{{$item->sku}} - {{$item->name}}</td>
                        <td>{{$item->model->category->name??'-'}}</td>
                        <td>{{$item->model->name??'-'}}</td>
                        <td>{{$item->warehouse}} - {{$item->rack}}</td>                        
                        <td>{{$item->unit}}</td>
                        <td>{{$item->detail_sum_qty}}</td>
                        <td>
                            <button class="btn btn-primary btn-xs" x-on:click="getData({{$item->id}})"><i class="fas fa-pen"></i></button>
                            <button class="btn btn-danger btn-xs" x-on:click="deleteData({{$item->id}})"><i class="fas fa-trash"></i></button><button class="btn btn-secondary btn-xs ml-1" wire:click="clicktable({{$item->id}})"><i class="fas fa-search"></i></button></td>
                    </tr>
                    <tr class="detailChild{{$item->id}} {{in_array($item->id,$selectedItem)?'':'d-none'}}">
                        <td colspan="9">                            
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th colspan="3"></th>   
                                    <th></th>                                                                             
                                    <th></th>                                                                             
                                    <th>Size</th>
                                    <th>Qty</th>
                                    <th>I.Qty</th>
                                    <th>Modal Price</th>
                                    <th>Sales Price</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @forelse($item->detail()->orderBy(\App\Models\Master\Size::select('index')->whereColumn('sizes.id','detail_items.size_id'))->get()??[] as $k => $detail)
                                    <tr>   
                                        <td colspan="3"></td>    
                                        <td></td>                                 
                                        <td></td>                                 
                                        <td>{{$detail->size->name}}</td>
                                        <td><p class="{{$detail->min_qty <> 0 && $detail->qty < $detail->min_qty?'text-danger':'' }}">{{$detail->qty}}</p></td>
                                        <td>{{$detail->iQty}}</td>
                                        <td>{{formatRupiah($detail->modal_price)}}</td>
                                        <td>{{formatRupiah($detail->sales_price)}}</td>
                                    </tr>
                                    @empty
                                    @endforelse
                                </tbody>

                            </table>
                        </td>
                    </tr>
                @endforeach
                  
              </tbody>
          </table> 
          <div class="float-right my-3 pr-3">
              {{$items->links()}}
            </div>         
        </div>
        <!-- /.card-body -->      

      <!-- Default box -->
      <div class="card" x-show.transition.origin.top.left="card_name == 'form'" x-cloak>
        <div class="card-header">
          <h3 class="card-title">Add Items</h3>
        </div>
        <form action="" wire:submit.prevent="{{$idItem?'update':'store'}}">
        <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Nama Barang" wire:model.defer="name">
                        @error('name')
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div>  
                      <div class="form-group">
                        <label for="sku">SKU</label>
                        <input type="text" class="form-control @error('sku') is-invalid @enderror" id="sku" placeholder="SKU" wire:model.defer="sku">
                        @error('sku')
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div> 
                      <div class="form-group {{$idItem?'d-none':''}}" >
                        <div wire:ignore>
                        <label for="model">Model</label>
                        <select class="form-control @error('model') is-invalid @enderror" id="model" placeholder="model">
                            <option value="">Pilih</option>
                        </select>
                        </div>
                        @error('model')
                            <div class="is-invalid"></div>
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div> 
                      <div class="form-group {{$idItem?'d-none':''}}">
                        <label for="unit">Unit</label>
                        <select class="form-control @error('unit') is-invalid @enderror" id="unit" placeholder="unit" wire:model.defer="unit">
                            <option value="">Pilih</option>
                            <option value="pcs">Pcs</option>
                            <option value="dz">Dz</option>
                        </select>
                        @error('unit')
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div> 
                              
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                        <label for="image">Image</label>
                        <div wire:loading wire:target="image">
                            <img src="{{asset("images/Spin-1s-200px.gif")}}" width="20" alt=""> Loading ...
                        </div>
                        
                        @if ($image_temp) 
                        <div class="mb-3">
                        <img width="150" class="temp" src="{{ url('storage/'.$image_temp) }}">
                        </div>
                        @endif
                        @if ($image) 
                            <div class="mb-3">
                            <img width="150" src="{{ $image->temporaryUrl() }}">
                            </div>
                        @endif
                        
                        <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="image" wire:model="image">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                    </div>
                        @error('image')
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div> 
                      
                      <div class="row">
                          <div class="col-sm-6">
                              <div class="form-group">
                        <label for="warehouse">Warehouse</label>
                        <select class="form-control @error('warehouse') is-invalid @enderror" id="warehouse" placeholder="Warehouse" wire:model="warehouse">
                            <option value="">Pilih</option>
                            @foreach($warehouses??[] as $ware)
                                <option value="{{$ware}}">{{$ware}}</option>
                            @endforeach
                        </select>
                        @error('warehouse')
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div> 
                      
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">
                        <label for="rack">Rack</label>
                        <select class="form-control @error('rack') is-invalid @enderror" id="rack" placeholder="rack" wire:model.defer="rack">
                            <option value="">Pilih</option>
                            @foreach($racks??[] as $r)
                                <option value="{{$r}}">{{$r}}</option>
                            @endforeach
                        </select>
                        @error('rack')
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div> 
                          </div>
                      </div>
                      <div class="form-group">
                        <label for="notes">Note</label>
                        <textarea class="form-control @error('notes') is-invalid @enderror" id="notes" placeholder="Notes" wire:model.defer="notes"></textarea>
                        @error('notes')
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div> 
                      
                    </div>
                </div>
                <div class="row pb-3 {{$idItem?'d-none':''}}" >
                    <div class="col-sm-12"><h3 class="card-title">Detail Item</h3></div>
                </div>
                <div class="row {{$idItem?'d-none':''}}">
                    <div class="col-sm-6">                        
                      <div class="form-group">  
                        <div wire:ignore>
                        <label for="size">Size</label>                          
                        <select class="form-control @error('size') is-invalid @enderror" id="size" placeholder="Size">
                            <option value="">Pilih</option>                            
                        </select>
                        </div>
                        @error('size')
                             <div class="is-invalid"></div>
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div> 
                      <div class="form-group">   
                        <label for="modal_price">Modal Price</label>                       
                        <input type="number" class="form-control @error('modal_price') is-invalid @enderror" id="modal_price" placeholder="Model Price" wire:model.defer="modal_price">
                        @error('modal_price')
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div>
                      <div class="form-group">   
                        <label for="sales_price">Sales Price</label>                       
                        <input type="number" class="form-control @error('sales_price') is-invalid @enderror" id="sales_price" placeholder="Model Price" wire:model.defer="sales_price">
                        @error('sales_price')
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div>
                    </div>
                    <div class="col-sm-6">                        
                      <div class="form-group">   
                      <label for="qty">Qty</label>                         
                        <input type="number" class="form-control @error('qty') is-invalid @enderror" id="qty" placeholder="Qty" wire:model.defer="qty">
                        @error('qty')
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div> 
                      <div class="form-group">   
                      <label for="min_qty">Minimum Qty</label>                         
                        <input type="number" class="form-control @error('min_qty') is-invalid @enderror" id="min_qty" placeholder="Minimum Qty" wire:model.defer="min_qty">
                        @error('min_qty')
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div>  
                      <div class="form-group">   
                      <label for="iqty">I.Qty</label>                         
                        <input type="number" class="form-control @error('iqty') is-invalid @enderror" id="iqty" placeholder="I.Qty" wire:model.defer="iqty">
                        @error('iqty')
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div>                       
                    </div>
                    <div class="col-sm-12">
                        <button class="btn btn-primary btn float-right" wire:click.prevent="addDetail()">Add Detail</button>
                    </div>
                </div>                
                <div class="row mt-3 {{$idItem?'d-none':''}}">
                    <div class="col-sm-12">
                        @error('detailTable') <div class="is-invalid"></div><div class="invalid-feedback pb-2">Detail Item cannot be left blank</div>  @enderror
                        <table class="table">
                            <thead>
                                <th>#</th>
                                <th>Size</th>
                                <th>Qty</th>
                                <th>Minimum Qty</th>
                                <th>I.Qty</th>
                                <th>Price</th>
                                <th>Sales Price</th>
                                <th>#</th>
                            </thead>
                            <tbody>
                                @foreach($detailTable??[] as $key => $item)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>{{$item['size_text']}}</td>
                                        <td>{{$item['qty']}}</td>
                                        <td>{{$item['min_qty']}}</td>
                                        <td>{{$item['iqty']}}</td>
                                        <td>{{formatRupiah($item['modal_price'])}}</td>
                                        <td>{{formatRupiah($item['sales_price'])}}</td>
                                        <td><button class="btn btn-danger btn-xs" wire:click.prevent="deleteDetail({{$item['size']}})"><i class="fas fa-minus"></i></button></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @if($idItem)
                <div class="row mt-3">
                    <div class="col-sm-12">
                        @error('detailTable') <div class="is-invalid"></div><div class="invalid-feedback pb-2">Detail Item cannot be left blank</div>  @enderror
                        <table class="table">
                            <thead>                                
                                <th>Size</th>
                                <th>Qty</th>
                                <th>Minimum Qty</th>
                                <th>I.Qty</th>                                                         
                                <th>Price</th>
                                <th>Sales Price</th>
                                <th>#</th>
                            </thead>
                            <tbody>
                                @foreach($detailTable??[] as $key => $item)
                                    <tr>                                        
                                        <td>{{$item['size_text']}}</td>
                                        <td>{{$item['qty']}}</td>
                                        <td>
                                            <input type="number" class="form-control" class="min_qty" wire:model.defer="tempItems.{{$item['item_id']}}.min_qty" required>
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" class="iqty" wire:model.defer="tempItems.{{$item['item_id']}}.iqty" required>
                                        </td>                                        
                                        <td>
                                            <input type="number" class="form-control" class="price" wire:model.defer="tempItems.{{$item['item_id']}}.price" required>
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" class="sales_price" wire:model.defer="tempItems.{{$item['item_id']}}.sales_price" required>
                                        </td>
                                        <td><input type="hidden" class="item_id" value="{{$item['item_id']}}"></td>
                                    </tr>
                                @endforeach
                                @foreach($detailItems??[] as $key => $detailItem)
                                    <tr>                                        
                                        <td>{{$detailItem['size_text']}}</td>
                                        <td>{{$detailItem['qty']}}</td>
                                        <td>
                                            <input type="number" class="form-control" value="{{$detailItem['min_qty']}}" readonly>
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" value="{{$detailItem['iqty']}}" readonly>
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" value="{{$detailItem['modal_price']}}" readonly>
                                        </td>
                                        <td>
                                           <input type="number" class="form-control" value="{{$detailItem['sales_price']}}" readonly>
                                        </td>
                                        <td><button class="btn btn-danger btn-xs" wire:click.prevent="deleteItem({{$detailItem['size']}})"><i class="fas fa-minus"></i></button></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="form-group">
                        <button type="button" wire:click.prevent="modelSize" class="btn btn-primary">Add Size</button>
                        </div>
                    </div>
                </div>
                @endif
                
                              
                  
        <!-- /.card-body -->
        <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <button type="submit" class="btn btn-default float-right" x-on:click.prevent="cancel()">Cancel</button>
                </div>
                </form>
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->

    {{-- add modal --}}
    <div wire:ignore:self class="modal fade" id="modal-image">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">              
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <img class="imgBox w-100" src="" alt="">
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>              
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      {{-- add modal --}}
    <div wire:ignore.self class="modal fade" id="modal-add-size" tabindex="-1" role="dialog" aria-hidden="true" >
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">              
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                    <div class="col-sm-6">                        
                      <div class="form-group">  
                        <div wire:ignore>
                        <label for="size">Size</label>                          
                        <select class="form-control @error('size') is-invalid @enderror" id="size_item" placeholder="Size">
                            <option value="">Pilih</option>                            
                        </select>
                        </div>
                        @error('size')
                             <div class="is-invalid"></div>
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div> 
                      <div class="form-group">   
                        <label for="modal_price">Modal Price</label>                       
                        <input type="number" class="form-control @error('modal_price') is-invalid @enderror" id="modal_price" placeholder="Model Price" wire:model.defer="modal_price">
                        @error('modal_price')
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div>
                      <div class="form-group">   
                        <label for="sales_price">Sales Price</label>                       
                        <input type="number" class="form-control @error('sales_price') is-invalid @enderror" id="sales_price" placeholder="Model Price" wire:model.defer="sales_price">
                        @error('sales_price')
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div>
                    </div>
                    <div class="col-sm-6">                        
                      <div class="form-group">   
                      <label for="qty">Qty</label>                         
                        <input type="number" class="form-control @error('qty') is-invalid @enderror" id="qty" placeholder="Qty" wire:model.defer="qty">
                        @error('qty')
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div> 
                      <div class="form-group">   
                      <label for="min_qty">Minimum Qty</label>                         
                        <input type="number" class="form-control @error('min_qty') is-invalid @enderror" id="min_qty" placeholder="Minimum Qty" wire:model.defer="min_qty">
                        @error('min_qty')
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div>
                      <div class="form-group">   
                      <label for="iqty">I.Qty</label>                         
                        <input type="number" class="form-control @error('iqty') is-invalid @enderror" id="iqty" placeholder="I.Qty" wire:model.defer="iqty">
                        @error('iqty')
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div>                       
                    </div>
                </div>     
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-primary" wire:click.prevent="addSize">Add</button>              
              <button type="button" class="btn btn-danger" wire:click.prevent="closeModalSize">Close</button>              
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

@push('scripts')

    <script type="text/javascript">
        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function(){

            $(document).on('click','.detailItem',function(){  

                id = $(this).attr('id');                            
                if($(this).parent().parent().parent().find(`.detailChild${id}`).hasClass('d-none')){
                    $(this).parent().parent().parent().find(`.detailChild${id}`).removeClass('d-none')
                }else{
                    $(this).parent().parent().parent().find(`.detailChild${id}`).addClass('d-none');
                }
            })

            $("#model").select2({
        allowClear: true,
        placeholder: "Pilih Model",
        ajax: {
            dataType: "json",
            url: "{{route('res.model')}}",
            delay: 300,
            data: function (params) {
                return {
                    _token: CSRF_TOKEN,
                    search: params.term,
                };
            },
            processResults: function (data, page) {
                return {
                    results: data,
                };
            },
        },
    }).on('select2:select', function (evt) {
                var text = $("#model option:selected").text();
                var id = $("#model option:selected").val();
                @this.set('model', id);
            });
            

    $("#size").select2({
        allowClear: true,
        placeholder: "Pilih Size",
        ajax: {
            dataType: "json",
            url: "{{route('res.size')}}",
            delay: 300,
            data: function (params) {
                return {
                    _token: CSRF_TOKEN,
                    search: params.term,
                };
            },
            processResults: function (data, page) {
                return {
                    results: data,
                };
            },
        },
    }).on('select2:select', function (evt) {
                var text = $("#size option:selected").text();
                var id = $("#size option:selected").val();
                @this.set('size', id);
                @this.set('size_text', text);
            });

    $("#size_item").select2({
        allowClear: true,
        placeholder: "Pilih Size",
        ajax: {
            dataType: "json",
            url: "{{route('res.size')}}",
            delay: 300,
            data: function (params) {
                return {
                    _token: CSRF_TOKEN,
                    search: params.term,
                };
            },
            processResults: function (data, page) {
                return {
                    results: data,
                };
            },
        },
    }).on('select2:select', function (evt) {
                var text = $("#size_item option:selected").text();
                var id = $("#size_item option:selected").val();
                @this.set('size', id);
                @this.set('size_text', text);
            });

        })

        

        function openModal(image){
            $('#modal-image').modal('show'); 
            $('.imgBox').attr('src','{{url('storage/')}}/'+image)
        }


        window.livewire.on('getData',(data,tipe)=>{
          // set for select2
          if(data){
             var dataMenu = {
            id: data.id,
            text: data.name
          };
          var newOption = new Option(dataMenu.text, dataMenu.id, false, false);
          $(`#${tipe}`).append(newOption).trigger('change');
          $(`#${tipe}`).val(dataMenu.id).trigger('change');    
          }
             

        })

        const test = {
            getData(id){
                this.card_name = 'form'
                @this.call('getData',id)
            },
            cancel(){
                this.card_name = 'table'
                @this.call('resetData');
            }
        }


        function deleteData(id){
            Swal.fire({
        title: 'Yakin dihapus?',
        text: 'Data akan dihapus!',
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Delete!'
    }).then((result) => {
        if (result.value) {
            @this.call('destroy',id)
            responseAlert({title: session('message'), type: 'success'});
        } else {
            responseAlert({
                title: 'Operation Cancelled!',
                type: 'success'
            });
        }
    });
        }

        window.livewire.on('callAddSize',()=>{
            $('#modal-add-size').modal('show'); 
        })

        window.livewire.on('closeModalSize',()=>{
            $('#modal-add-size').modal('hide'); 
        })

        window.livewire.on('resetDetail', (message) => {
            $('#model').val('').trigger('change');
            $('#product').val('').trigger('change');
            $('#size').val('').trigger('change');
        })

        window.livewire.on('flash_message', (message) => {
            Swal.fire(
                'Success!',
            message,
                'success'
            )        
        });

        window.livewire.on('flash_error', (message) => {
            Swal.fire(
                'Oops...!', 
            message,
                'error'
            )
        });
        
    </script>
@endpush
</div>