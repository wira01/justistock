
<div id="wrapper" x-data="{card_name:  @entangle('showCard'),...test}">
   @section('title')
    Justiboy - Model
   @endsection
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Model</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Model</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card" x-show.transition.origin.top.left="card_name == 'table'">
        <div class="card-header">
          <h3 class="card-title">Model</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-primary" @click="card_name = 'form'">
              <span>Add Model</span>
            </button>                           
          </div>
        </div>
        <div class="card-body p-0">
          <div class="row mt-3 pr-3 pl-3">
            <div class="col-sm-4">
                <div class="form-group">
                    <div wire:ignore>
                    <select type="text" id="category" class="form-control">
                        <option value="">Pilih</option>
                    </select>
                    </div>                    
                  </div>  
            </div>
            <div class="offset-sm-4"></div>
              <div class="col-sm-4">
              <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                    </div>
                    <input type="text" class="form-control" id="search" placeholder="Cari" wire:model="q">
                </div>
                </div>
          </div>
          <table class="table table-striped">
              <thead>
                  <tr>
                      
                      <th style="width: 1%">
                          #
                      </th>
                      <th style="width: 20%">
                          Name
                      </th>    
                      <th style="width: 20%">
                          Category
                      </th>                       
                      <th style="width: 1%">
                          Action
                      </th>
                  </tr>
              </thead>
              <tbody>
                @foreach($models as $key => $model)                    
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$model->name}}</td>                        
                        <td>{{$model->category->name??''}}</td>                        
                        <td>
                            <button class="btn btn-primary btn-xs" x-on:click="getData({{$model->id}})"><i class="fas fa-pen"></i></button>
                            <button class="btn btn-danger btn-xs" x-on:click="deleteData({{$model->id}})"><i class="fas fa-trash"></i></button></td>
                    </tr>
                @endforeach
                  
              </tbody>
          </table> 
          <div class="float-right my-3 pr-3">
              {{$models->links()}}
            </div>         
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

      <!-- Default box -->
      <div class="card" x-show.transition.origin.top.left="card_name == 'form'" x-cloak>
        <div class="card-header">
          <h3 class="card-title">Add Model</h3>
        </div>
        <form action="" wire:submit.prevent="store">
        <div class="card-body">
            
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Justiboy" wire:model.defer="name">
                    @error('name')
                         <div class="invalid-feedback"> {{$message}} </div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <div wire:ignore>
                    <label for="category">Category</label>
                    <select type="text" id="category_id" class="form-control">
                        <option value="">Pilih</option>
                    </select>
                    </div>
                    @error('category')
                        <div class="is-invalid"></div>
                         <div class="invalid-feedback"> {{$message}} </div>
                    @enderror
                  </div>               
                  
        <!-- /.card-body -->
        <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <button type="submit" class="btn btn-default float-right" x-on:click.prevent="cancel()">Cancel</button>
                </div>
                </form>
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->

@push('scripts')

    <script type="text/javascript">
        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function(){

            $("#category_id").select2({
        allowClear: true,
        placeholder: "Pilih Category",
        ajax: {
            dataType: "json",
            url: "{{route('res.categories')}}",
            delay: 300,
            data: function (params) {
                return {
                    _token: CSRF_TOKEN,
                    search: params.term,
                };
            },
            processResults: function (data, page) {
                return {
                    results: data,
                };
            },
        },
    }).on('select2:select', function (evt) {
                var text = $("#category_id option:selected").text();
                var id = $("#category_id option:selected").val();
                @this.set('category_id', id);
            });

    $("#category").select2({
        allowClear: true,
        placeholder: "Filter Category",
        ajax: {
            dataType: "json",
            url: "{{route('res.categories')}}",
            delay: 300,
            data: function (params) {
                return {
                    _token: CSRF_TOKEN,
                    search: params.term,
                };
            },
            processResults: function (data, page) {
                return {
                    results: data,
                };
            },
        },
    }).on('select2:select', function (evt) {
                var id = $("#category option:selected").val();
                @this.set('category', id);
            });
        })
        
        const test = {
            getData(id){
                this.card_name = 'form'
                @this.call('getData',id)
            },
            cancel(){
                this.card_name = 'table'
                @this.call('resetData');
                $('#category_id').val('').trigger('change');
            }
        }

        function deleteData(id){
            Swal.fire({
        title: 'Yakin dihapus?',
        text: 'Data akan dihapus!',
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Delete!'
    }).then((result) => {
        if (result.value) {
            @this.call('destroy',id)
            responseAlert({title: session('message'), type: 'success'});
        } else {
            responseAlert({
                title: 'Operation Cancelled!',
                type: 'success'
            });
        }
    });
        }

        window.livewire.on('getCategory',data=>{
          // set for select2
          if(data){
             var dataCategory = {
            id: data.id,
            text: data.name
          };
          var newOption = new Option(dataCategory.text, dataCategory.id, false, false);
          $('#category_id').append(newOption).trigger('change');
          $('#category_id').val(dataCategory.id).trigger('change');    
          }else{
            $('#category_id').val('').trigger('change');
          }
             

        })

        window.livewire.on('flash_message', (message) => {
            Swal.fire(
                'Success!',
            message,
                'success'
            )        
            $('#category_id').val('').trigger('change');
        });
        
    </script>
@endpush
</div>