
<div id="wrapper" x-data="{card_name:  @entangle('showCard'),...test}">
   @section('title')
    Justiboy - Customer
   @endsection
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Customer</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Customer</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card" x-show.transition.origin.top.left="card_name == 'table'">
        <div class="card-header">
          <h3 class="card-title">Customer</h3>

          {{-- <div class="card-tools">
            <button type="button" class="btn btn-primary" @click="card_name = 'form'">
              <span>Add Customer</span>
            </button>                           
          </div> --}}
        </div>
        <div class="card-body p-0">
          <div class="row mt-3 pr-3">
            <div class="offset-sm-8"></div>
              <div class="col-sm-4">
              <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                    </div>
                    <input type="text" class="form-control" id="search" placeholder="Cari" wire:model="q">
                </div>
                </div>
          </div>
          <table class="table table-striped">
              <thead>
                  <tr>
                      
                      <th style="width: 1%">
                          #
                      </th>
                      <th style="width: 20%">
                          Name
                      </th>                      
                      <th style="width: 1%">
                          Action
                      </th>
                  </tr>
              </thead>
              <tbody>
                @foreach($customers as $key => $customer)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$customer->name}}</td>
                        <td>
                            {{-- <button class="btn btn-primary btn-xs" x-on:click="getData({{$customer->id}})"><i class="fas fa-pen"></i></button> --}}
                            <button class="btn btn-danger btn-xs" x-on:click="deleteData({{$customer->id}})"><i class="fas fa-trash"></i></button></td>
                    </tr>
                @endforeach
                  
              </tbody>
          </table> 
          <div class="float-right my-3 pr-3">
              {{$customers->links()}}
            </div>         
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

      <!-- Default box -->
      {{-- <div class="card" x-show.transition.origin.top.left="card_name == 'form'" x-cloak>
        <div class="card-header">
          <h3 class="card-title">Add Role</h3>
        </div>
        <form action="" wire:submit.prevent="store">
        <div class="card-body">
            
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror"" id="name" placeholder="Role Name" wire:model.defer="name">
                    @error('name')
                         <div class="invalid-feedback"> {{$message}} </div>
                    @enderror
                  </div>    
                  <div class="select2-primary">
                    <label for="permission">Permissions</label>
                    <select class="form-control @error('permission') is-invalid @enderror" id="permission" placeholder="Role Permission" wire:model.defer="permission" multiple>
                        @foreach($permissions as $id => $permission)
                            <option value="{{ $id }}" {{ in_array($id, old('id', [])) ? 'selected' : '' }}>{{ $permission }}</option>
                        @endforeach
                    </select>
                    @error('permission')
                         <div class="invalid-feedback"> {{$message}} </div>
                    @enderror
                  </div>                             
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <button type="submit" class="btn btn-default float-right" x-on:click.prevent="cancel()">Cancel</button>
                </div>
                </form>
      </div> --}}
      <!-- /.card -->

    </section>
    <!-- /.content -->

@push('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            $('#permission').select2();
            $('#permission').select2({
             tags: true,
            }).on('change', function(e){
                var value = $('#permission').val();
               @this.set('permission', value);
            });

            Livewire.hook('message.processed', (message, component) => {
              $('#permission').select2();
    });
        })
        
        const test = {
            getData(id){
                this.card_name = 'form'
                @this.call('getData',id)
            },
            cancel(){
                this.card_name = 'table'
                @this.call('resetData');
                $('#permission').val('').trigger('change')
            }
        }

        function deleteData(id){
            Swal.fire({
        title: 'Yakin dihapus?',
        text: 'Data akan dihapus!',
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Delete!'
    }).then((result) => {
        if (result.value) {
            @this.call('destroy',id)
            responseAlert({title: session('message'), type: 'success'});
        } else {
            responseAlert({
                title: 'Operation Cancelled!',
                type: 'success'
            });
        }
    });
        }

        window.livewire.on('flash_message', (message) => {
            Swal.fire(
                'Success!',
            message,
                'success'
            )   
            $('#permission').val('').trigger('change')     
        });
        
    </script>
@endpush
</div>