
<div id="wrapper" x-data="{card_name:  @entangle('showCard'),...test}">
   @section('title')
    Justiboy - Menu
   @endsection
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Menu</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Menu</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card" x-show.transition.origin.top.left="card_name == 'table'">
        <div class="card-header">
          <h3 class="card-title">Menu</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-primary" @click="card_name = 'form'">
              <span>Add Menu</span>
            </button>                           
          </div>
        </div>
        <div class="card-body p-0">
          <div class="row mt-3 pr-3">
            <div class="offset-sm-8"></div>
              <div class="col-sm-4">
              <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                    </div>
                    <input type="text" class="form-control" id="search" placeholder="Cari" wire:model="q">
                </div>
                </div>
          </div>
          <table class="table table-striped">
              <thead>
                  <tr>
                      
                      <th style="width: 1%">
                          #
                      </th>
                      <th style="width: 20%">
                          Title
                      </th>    
                      <th style="width: 20%">
                          Alias
                      </th>         
                      <th style="width: 20%">
                          URL
                      </th>     
                      <th style="width: 20%">
                          Icon
                      </th>                      
                      <th style="width: 1%">
                          Action
                      </th>
                  </tr>
              </thead>
              <tbody>                
                  @foreach($menus as $key => $menu)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$menu->name}}</td>
                        <td>{{$menu->alias}}</td>
                        <td>{{$menu->url}}</td>
                        <td><i class="{{$menu->icon}}"></i></td>
                        <td>
                            <button class="btn btn-primary btn-xs" x-on:click="getData({{$menu->id}})"><i class="fas fa-pen"></i></button>
                            <button class="btn btn-danger btn-xs" x-on:click="deleteData({{$menu->id}})"><i class="fas fa-trash"></i></button></td>
                    </tr>
                @endforeach
              </tbody>
          </table> 
          <div class="float-right my-3 pr-3">
                @if($menus)
                    {{$menus->links()}}
                @endif
            </div>         
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

      <!-- Default box -->
      <div class="card" x-show.transition.origin.top.left="card_name == 'form'" x-cloak>
        <div class="card-header">
          <h3 class="card-title">Add Menu</h3>
        </div>
        <form action="" wire:submit.prevent="store">
        <div class="card-body">
            
                <div class="form-group">
                    <label for="name">Title</label>
                    <input type="text" id="name" class="form-control @error('title') is-invalid @enderror"" id="title" placeholder="Sales" wire:model.defer="title">
                    @error('title')
                         <div class="invalid-feedback"> {{$message}} </div>
                    @enderror
                  </div>   
                  <div class="form-group">
                    <label for="alias">Alias</label>
                    <input type="text" id="alias" class="form-control @error('alias') is-invalid @enderror"" id="alias" placeholder="sales" wire:model.defer="alias">
                    @error('alias')
                         <div class="invalid-feedback"> {{$message}} </div>
                    @enderror
                  </div> 
                  <div class="form-group">
                    <label for="url">URL</label>
                    <input type="text" id="url" class="form-control @error('url') is-invalid @enderror"" id="url" placeholder="/sales" wire:model.defer="url">
                    @error('url')
                         <div class="invalid-feedback"> {{$message}} </div>
                    @enderror
                  </div>   
                  <div class="form-group">
                    <label for="icon">Icon</label>
                    <input type="text" id="icon" class="form-control @error('icon') is-invalid @enderror"" id="icon" placeholder="fas fa-icon" wire:model.defer="icon">
                    @error('icon')
                         <div class="invalid-feedback"> {{$message}} </div>
                    @enderror
                  </div>   
                  <div class="form-group">
                    <label for="position">Position</label>
                    <input type="number" id="position" class="form-control @error('position') is-invalid @enderror"" id="position" placeholder="1" wire:model.defer="position">
                    @error('position')
                         <div class="invalid-feedback"> {{$message}} </div>
                    @enderror
                  </div>   
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="parent_check" wire:model="parent_check">
                    <label class="form-check-label" for="parent_check">Parent</label>
                  </div> 
                  <div class="{{$parent_check?:'d-none'}}">
                  <div class="form-group" wire:ignore>
                    <select type="text" id="parent_id" class="form-control">
                        <option value="">Pilih</option>
                    </select>
                    </div>                    
                    @error('parent_id')
                         <div class="invalid-feedback"> {{$message}} </div>
                    @enderror

                  </div>                                              
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <button type="submit" class="btn btn-default float-right" x-on:click.prevent="cancel()">Cancel</button>
                </div>
                </form>
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->

@push('scripts')    
    <script type="text/javascript">
        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $(document).ready(function(){
            $("#parent_id").select2({
        allowClear: true,
        placeholder: "Pilih Menu",
        ajax: {
            dataType: "json",
            url: "{{route('res.menus')}}",
            delay: 300,
            data: function (params) {
                return {
                    _token: CSRF_TOKEN,
                    search: params.term,
                };
            },
            processResults: function (data, page) {
                return {
                    results: data,
                };
            },
        },
    }).on('select2:select', function (evt) {
                var text = $("#parent_id option:selected").text();
                var id = $("#parent_id option:selected").val();
                @this.set('parent_id', id);
            });
        })
        
        const test = {
            getData(id){
                this.card_name = 'form'
                @this.call('getData',id)
            },
            cancel(){
                this.card_name = 'table'
                @this.call('resetData');
                $('#parent_id').val('').trigger('change');
            }
        }

        function deleteData(id){
            Swal.fire({
        title: 'Yakin dihapus?',
        text: 'Data akan dihapus!',
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Delete!'
    }).then((result) => {
        if (result.value) {
            @this.call('destroy',id)
            responseAlert({title: session('message'), type: 'success'});
        } else {
            responseAlert({
                title: 'Operation Cancelled!',
                type: 'success'
            });
        }
    });
        }

        window.livewire.on('getMenu',data=>{
          // set for select2
          if(data){
             var dataMenu = {
            id: data.id,
            text: data.name
          };
          var newOption = new Option(dataMenu.text, dataMenu.id, false, false);
          $('#parent_id').append(newOption).trigger('change');
          $('#parent_id').val(dataMenu.id).trigger('change');    
          }
             

        })

        window.livewire.on('flash_message', (message) => {
            Swal.fire(
                'Success!',
            message,
                'success'
            )     
            $('#parent_id').val('').trigger('change');   
        });
        
    </script>
@endpush
</div>