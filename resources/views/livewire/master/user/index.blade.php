
<div id="wrapper" x-data="{card_name:  @entangle('showCard'),...test}">
   @section('title')
    Justiboy - Users
   @endsection
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Users</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Users</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card" x-show.transition.origin.top.left="card_name == 'table'">
        <div class="card-header">
          <h3 class="card-title">User</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-primary" @click="card_name = 'form'">
              <span>Add User</span>
            </button>                           
          </div>
        </div>
        <div class="card-body p-0">
          <div class="row mt-3 pr-3">
            <div class="offset-sm-8"></div>
              <div class="col-sm-4">
              <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                    </div>
                    <input type="text" class="form-control" id="search" placeholder="Cari" wire:model="q">
                </div>
                </div>
          </div>
          <table class="table table-striped">
              <thead>
                  <tr>
                      
                      <th style="width: 1%">
                          #
                      </th>
                      <th style="width: 20%">
                          Username
                      </th>   
                      <th style="width: 20%">
                          Name
                      </th>   
                      <th style="width: 20%">
                          Email
                      </th>                      
                      <th style="width: 1%">
                          Role
                      </th>
                      <th style="width: 1%">
                          Action
                      </th>
                  </tr>
              </thead>
              <tbody>
                @foreach($users as $key => $user)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$user->username}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->role->title??'-'}}</td>
                        <td>
                            <button class="btn btn-primary btn-xs" x-on:click="getData({{$user->id}})"><i class="fas fa-pen"></i></button>
                            <button class="btn btn-danger btn-xs" x-on:click="deleteData({{$user->id}})"><i class="fas fa-trash"></i></button></td>
                    </tr>
                @endforeach
                  
              </tbody>
          </table> 
          <div class="float-right my-3 pr-3">
              {{$users->links()}}
            </div>         
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

      <!-- Default box -->
      <div class="card" x-show.transition.origin.top.left="card_name == 'form'" x-cloak>
        <div class="card-header">
          <h3 class="card-title">Add User</h3>
        </div>
        <form action="" wire:submit.prevent="store">
        <div class="card-body">
            
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Budi Indah" wire:model.defer="name">
                    @error('name')
                         <div class="invalid-feedback"> {{$message}} </div>
                    @enderror
                  </div> 
                  <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control @error('username') is-invalid @enderror" id="username" placeholder="budi.indah" wire:model.defer="username">
                    @error('username')
                         <div class="invalid-feedback"> {{$message}} </div>
                    @enderror
                  </div>    
                  <div class="form-group">
                    <label for="name">Password</label>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Password" wire:model.defer="password">
                    @error('password')
                         <div class="invalid-feedback"> {{$message}} </div>
                    @enderror
                  </div> 
                  <div class="form-group {{$this->idUser ? 'd-none' : ''}}">
                    <label for="name">Password Confirmation</label>
                    <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" id="password_confirmation" placeholder="Password Confirmation" wire:model.defer="password_confirmation">
                    @error('password_confirmation')
                         <div class="invalid-feedback"> {{$message}} </div>
                    @enderror
                  </div> 
                  <div class="form-group">
                    <label for="name">Email</label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Email" wire:model.defer="email">
                    @error('email')
                         <div class="invalid-feedback"> {{$message}} </div>
                    @enderror
                  </div>    
                  <div class="select2-primary">
                    <label for="role">Role</label>
                    <select class="form-control @error('role') is-invalid @enderror" id="role" placeholder="Role" wire:model.defer="role" >
                        <option value="">Pilih</option>
                        @foreach($roles as $id => $role)
                            <option value="{{ $id }}" {{ in_array($id, old('id', [])) ? 'selected' : '' }}>{{ $role }}</option>
                        @endforeach
                    </select>
                    @error('role')
                         <div class="invalid-feedback"> {{$message}} </div>
                    @enderror
                  </div>                             
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <button type="submit" class="btn btn-default float-right" x-on:click.prevent="cancel()">Cancel</button>
                </div>
                </form>
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->

@push('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            $('#role').select2();
            $('#role').select2({
             tags: false,
            }).on('change', function(e){
                var value = $('#role').val();
               @this.set('role', value);
            });

            Livewire.hook('message.processed', (message, component) => {
              $('#role').select2();
    });
        })
        
        const test = {
            getData(id){
                this.card_name = 'form'
                @this.call('getData',id)
            },
            cancel(){
                this.card_name = 'table'
                @this.call('resetData')
                $('#role').val('').trigger('change')
            }
        }

        function deleteData(id){
            Swal.fire({
        title: 'Yakin dihapus?',
        text: 'Data akan dihapus!',
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Delete!'
    }).then((result) => {
        if (result.value) {
            @this.call('destroy',id)
            responseAlert({title: session('message'), type: 'success'});
        } else {
            responseAlert({
                title: 'Operation Cancelled!',
                type: 'success'
            });
        }
    });
        }

        window.livewire.on('flash_message', (message) => {
            Swal.fire(
                'Success!',
            message,
                'success'
            )  
            $('#role').val('').trigger('change')      
        });
        
    </script>
@endpush
</div>