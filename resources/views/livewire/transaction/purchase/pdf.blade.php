<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ public_path('css/pdf_style.css') }}" rel="stylesheet">
    <title>Justiboy - Pembelian</title>
</head>
<body>
    <main style="page-break-after: all">
    <div class="headerPdf" style="text-align: center;">
        <h1>Pembelian</h1>
            <h5>#{{$purchase->no_pesanan}}</h5>
    </div>
    <div class="inline">
        <div class="inline_data">
            <table class="table">
              <thead>
                  <tr>
                      
                      <th rowspan="2" style="width: 1%">
                          #
                      </th>                      
                      <th rowspan="2" style="width:10%">
                          Tanggal
                      </th>   
                      <th rowspan="2" style="width:5%">
                          Marketplace
                      </th>
                      <th rowspan="2" style="width:15%">
                          No Pesanan
                      </th>
                      <th rowspan="2" style="width:15%">
                          Supplier
                      </th>
                      <th rowspan="2" style="width:15%">
                          Total
                      </th>                       
                      <th colspan="7">Detail</th>                      
                  </tr>
                  <tr>
                      <th>
                          SKU
                      </th>
                      <th>
                          Model
                      </th>
                      <th>
                          Produk
                      </th>
                      <th>
                          Size
                      </th>
                      <th>
                          Qty
                      </th>
                      <th>
                          Price
                      </th>
                      <th>
                          Total Price
                      </th>
                  </tr>                                  
              </thead>
              <tbody>
                @php
                    $total = 0
                @endphp
                
                    @foreach($purchase->detail as $k => $kerja)
                        @php
                            $rowspan = $purchase->detail->count(); 
                            $total += $kerja->modal_price*$kerja->qty;
                        @endphp
                        <tr>
                            @if($k == 0)
                                <td rowspan="{{$rowspan}}">{{$k+1}}</td>
                                <td rowspan="{{$rowspan}}">{{Carbon\Carbon::parse($purchase->tanggal)->format('Y-m-d')}}</td>
                                <td rowspan="{{$rowspan}}">{{$purchase->marketplace->name}}</td>
                                <td rowspan="{{$rowspan}}">{{$purchase->no_pesanan}}</td>
                                <td rowspan="{{$rowspan}}">{{$purchase->contact??$purchase->marketplace->name}}</td>                      
                                <td rowspan="{{$rowspan}}">{{formatRupiah($purchase->detail_sum_total)}}</td>                      
                                <td>{{$kerja->detailItem->item->sku}}</td>  
                                <td>{{$kerja->detailItem->item->model->name}}</td>  
                                <td>{{$kerja->detailItem->item->name}}</td>                                   
                                <td>{{$kerja->detailItem->size->name}}</td>  
                                <td>{{$kerja->qty}}</td>           
                                <td>{{formatRupiah($kerja->modal_price)}}</td>  
                                <td>{{formatRupiah($kerja->total)}}</td>                                  
                                                      
                            @else
                                <td>{{$kerja->detailItem->item->sku}}</td>  
                                <td>{{$kerja->detailItem->item->model->name}}</td>  
                                <td>{{$kerja->detailItem->item->name}}</td>                                   
                                <td>{{$kerja->detailItem->size->name}}</td>  
                                <td>{{$kerja->qty}}</td> 
                                <td>{{formatRupiah($kerja->modal_price)}}</td>                                  
                                <td>{{formatRupiah($kerja->total)}}</td>                            
                            @endif
                        </tr>
                    @endforeach
                
                  
              </tbody>
              <tfoot>
                  <tr>
                      <td colspan="2">Total purchases</td>
                      <td colspan="11">{{formatRupiah($total)}}</td>
                  </tr>
              </tfoot>     
          </table>  
        </div>
    </div>
    </main>
</body>