
<div id="wrapper" x-data="{card_name:  @entangle('showCard'),...test,filtering_by:false}">
   @section('title')
    Justiboy - Retur Pembelian
   @endsection
   @push('styles')
   <style type="text/css">
    .table th{
        vertical-align: middle!important;
    }
   </style>
   @endpush
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Retur Pembelian</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Retur Pembelian</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card" x-show.transition.origin.top.left="card_name == 'table'">
        <div class="card-header">
          <h3 class="card-title">Filter By : <button type="button" class="btn btn-tool" @click="filtering_by = !filtering_by">
              <i class="fas fa-filter" :class="filtering_by == true?'text-primary':''"></i>
            </button></h3>         
        </div>
        <div class="card-body p-0">
          <div class="row mt-3">
            <div class="col-md-12">
            <div class="card">                        
              <div class="card-body" x-show.transition.origin.top.left="filtering_by == true" x-cloak>
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row align-items-center">
                            <label for="q" class="col-sm-2 col-form-label">Date :</label> 
                          <div class="input-group col-sm-8 py-3" wire:ignore>      
                            <x-input.date class="mr-3" id="dateFrom" placeholder="{{now()->format('Y-m-d')}}"/>
                            <x-input.date class="mr-3" id="dateTo" placeholder="{{now()->format('Y-m-d')}}"/>
                           </div>
                           </div>  
                           <div class="form-group row">
                            <label for="marketplace_filter" class="col-sm-2 col-form-label">Marketplace :</label>
                            <div class="col-md-8">
                            <x-input.select2 placeholder="Pilih Marketplace" id="marketplace_filter" route="res.marketplace" name="marketplace_filter"/>
                        </div>
                      </div>                                                 
                      <div class="form-group row">
                            <label for="q" class="col-sm-2 col-form-label">Search :</label>
                            <div class="col-md-8">
                            <input type="text" class="form-control" id="q" placeholder="Search" wire:model="q">
                        </div>
                      </div>
                  </div>
                  
              </div>
          </div>   
          </div>           
          </div>
          </div>
          <table class="table table-bordered text-center">
              <thead>
                  <tr>
                      
                      <th rowspan="2" style="width: 1%">
                          #
                      </th>                      
                      <th rowspan="2" style="width:10%">
                          Tanggal
                      </th>   
                      <th rowspan="2" style="width:5%">
                          Marketplace
                      </th>
                      <th rowspan="2" style="width:15%">
                          No Pesanan
                      </th>
                      <th rowspan="2" style="width:15%">
                          Supplier
                      </th>
                      <th colspan="5">Detail</th>                      
                      
                  </tr>
                  <tr>
                      <th>
                          SKU
                      </th>
                      <th>
                          Model
                      </th>
                      <th>
                          Produk
                      </th>
                      <th>
                          Size
                      </th>
                      <th>
                          Qty
                      </th>
                  </tr>                                  
              </thead>
              <tbody>

                @foreach($return_purchases as $key => $return_purchase)
                    @php
                        $rowspan = $return_purchase->detail->count();                                            
                    @endphp
                    @foreach($return_purchase->detail as $k => $kerja)

                        <tr>
                            @if($k == 0)
                                <td rowspan="{{$rowspan}}">{{$key+1}}</td>
                                <td rowspan="{{$rowspan}}">{{$return_purchase->created_at->format('Y-m-d')}}</td>
                                <td rowspan="{{$rowspan}}">{{$return_purchase->marketplace->name}}</td>
                                <td rowspan="{{$rowspan}}">{{$return_purchase->no_pesanan}}</td>
                                <td rowspan="{{$rowspan}}">{{$return_purchase->contact??$return_purchase->marketplace->name}}</td>
                                <td>{{$kerja->detailItem->item->sku}}</td>  
                                <td>{{$kerja->detailItem->item->model->name}}</td>  
                                <td>{{$kerja->detailItem->item->name}}</td>                                   
                                <td>{{$kerja->detailItem->size->name}}</td>  
                                <td>{{$kerja->qty}}</td>                                  
                            @else
                                <td>{{$kerja->detailItem->item->sku}}</td>  
                                <td>{{$kerja->detailItem->item->model->name}}</td>  
                                <td>{{$kerja->detailItem->item->name}}</td>                                   
                                <td>{{$kerja->detailItem->size->name}}</td>  
                                <td>{{$kerja->qty}}</td> 
                            @endif
                        </tr>
                    @endforeach
                @endforeach
                  
              </tbody>
          </table> 
          <div class="float-right my-3 pr-3">
              {{$return_purchases->links()}}
            </div>         
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->


    </section>
    <!-- /.content -->

@push('scripts')

    <script type="text/javascript">
        
        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        const test = {
            getData(id){
                this.card_name = 'form'
                @this.call('getData',id)
            },
            cancel(){
                this.card_name = 'table'
                @this.call('resetData');
                
            }
        }

         $("#model").select2({            
            allowClear: true,
            placeholder: "Pilih Produk",
            ajax: {
                dataType: "json",
                url: "{{route('res.model')}}",
                delay: 300,
                data: function (params) {
                    return {
                        _token: CSRF_TOKEN,
                        search: params.term,
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data,
                    };
                },
            },
        }).on('select2:select', function (evt) {
                    var text = $("#model option:selected").text();
                    var id = $("#model option:selected").val();
                    @this.set('model', id);
                    @this.set('model_text', text);
                    Livewire.emit('setProduct',id);
                });


        window.livewire.on('setProduct', (model) => {
            $('#product').val('').trigger('change');
            $('#size').val('').trigger('change');
            $("#product").select2({
            allowClear: true,
            placeholder: "Pilih Produk",
            ajax: {
                dataType: "json",
                url: "{{route('res.item_by_model')}}",
                delay: 300,
                data: function (params) {
                    return {
                        _token: CSRF_TOKEN,
                        search: params.term,
                        model
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data,
                    };
                },
            },
        }).on('select2:select', function (evt) {
                    var text = $("#product option:selected").text();
                    var id = $("#product option:selected").val();
                    @this.set('product', id);
                    @this.set('product_text', text);
                    Livewire.emit('setSize',id);
                });

                });


        window.livewire.on('setSize', (product) => {            
            $("#size").select2({
            allowClear: true,
            placeholder: "Pilih Size",
            ajax: {
                dataType: "json",
                url: "{{route('res.size_by_item')}}",
                delay: 300,
                data: function (params) {
                    return {
                        _token: CSRF_TOKEN,
                        search: params.term,
                        product
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data,
                    };
                },
            },
        }).on('select2:select', function (evt) {
                    var text = $("#size option:selected").text();
                    var id = $("#size option:selected").val();
                    @this.set('size', id);                  
                    @this.set('size_text', text);                  
                });
        });
        

        function deleteData(id){
            Swal.fire({
        title: 'Yakin dihapus?',
        text: 'Data akan dihapus!',
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Delete!'
    }).then((result) => {
        if (result.value) {
            @this.call('destroy',id)
            responseAlert({title: session('message'), type: 'success'});
        } else {
            responseAlert({
                title: 'Operation Cancelled!',
                type: 'success'
            });
        }
    });
        }

        window.livewire.on('flash_message', (message) => {
            Swal.fire(
                'Success!',
            message,
                'success'
            )        
        });

        window.livewire.on('flash_error', (message) => {
            Swal.fire(
                'Oops...!', 
            message,
                'error'
            )
        });
        
    </script>
@endpush
</div>