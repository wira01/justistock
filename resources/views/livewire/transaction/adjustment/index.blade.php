
<div id="wrapper" x-data="{card_name:  @entangle('showCard'),...test,filtering_by:false}">
   @section('title')
    Justiboy - Adjustment
   @endsection
   @push('styles')
   <style type="text/css">
    .table th{
        vertical-align: middle!important;
    }
   </style>
   @endpush
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Adjustment</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Adjustment</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card" x-show.transition.origin.top.left="card_name == 'table'">
        <div class="card-header">
          <h3 class="card-title">Filter By : <button type="button" class="btn btn-tool" @click="filtering_by = !filtering_by">
              <i class="fas fa-filter" :class="filtering_by == true?'text-primary':''"></i>
            </button></h3>

          <div class="card-tools">
            <button type="button" class="btn btn-primary" @click="card_name = 'form'">
              <span>Add Adjustment</span>
            </button>                           
          </div>
        </div>
        <div class="card-body p-0">
          <div class="row mt-3">
            <div class="col-md-12">
            <div class="card">                        
              <div class="card-body" x-show.transition.origin.top.left="filtering_by == true" x-cloak>
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row align-items-center">
                            <label for="q" class="col-sm-2 col-form-label">Date :</label> 
                          <div class="input-group col-sm-8 py-3" wire:ignore>      
                            <x-input.date class="mr-3" id="dateFrom" placeholder="{{now()->format('Y-m-d')}}"/>
                            <x-input.date class="mr-3" id="dateTo" placeholder="{{now()->format('Y-m-d')}}"/>
                           </div>
                           </div>
                           <div class="form-group row">
                            <label for="q" class="col-sm-2 col-form-label">Search :</label>
                            <div class="col-md-8">
                            <input type="text" class="form-control" id="q" placeholder="Search" wire:model="q">
                        </div>
                      </div>
                                                 
                  </div>
                  
              </div>
          </div>   
          </div>           
          </div>
          </div>
          <table class="table table-bordered text-center">
              <thead>
                  <tr>
                      
                      <th rowspan="2" style="width: 1%">
                          #
                      </th>                    
                      <th rowspan="2" style="width:10%">
                          Tanggal
                      </th>                         
                      <th rowspan="2" style="width:15%">
                          No Adjustment
                      </th>
                      <th colspan="7">Detail</th>
                  </tr>
                  <tr>
                      <th>
                          Keterangan
                      </th>
                      <th>
                          SKU
                      </th>
                      <th>
                          Model
                      </th>
                      <th>
                          Produk
                      </th>
                      <th>
                          Size
                      </th>
                      <th>
                          Qty
                      </th>
                      <th>
                          bQty
                      </th>
                  </tr>
              </thead>
              <tbody>                
                  @foreach($adjustments as $key => $adjustment)
                    @php
                        $rowspan = $adjustment->detail->count();                                            
                    @endphp
                    @foreach($adjustment->detail as $k => $adjust)

                        <tr>
                            @if($k == 0)
                                <td rowspan="{{$rowspan}}">{{$key+1}}</td>                                
                                <td rowspan="{{$rowspan}}">{{$adjustment->created_at->format('Y-m-d')}}</td>
                                <td rowspan="{{$rowspan}}">{{$adjustment->no_adjustment}}</td>                                
                                <td>{{$adjust->keterangan}}</td>  
                                <td>{{$adjust->detailItem->item->sku}}</td>  
                                <td>{{$adjust->detailItem->item->model->name}}</td>  
                                <td>{{$adjust->detailItem->item->name}}</td>                                   
                                <td>{{$adjust->detailItem->size->name}}</td>  
                                <td>{{$adjust->qty}}</td>  
                                <td>{{$adjust->bQty??'-'}}</td>  
                            @else
                                <td>{{$adjust->keterangan}}</td>  
                                <td>{{$adjust->detailItem->item->sku}}</td>  
                                <td>{{$adjust->detailItem->item->model->name}}</td>  
                                <td>{{$adjust->detailItem->item->name}}</td>                                   
                                <td>{{$adjust->detailItem->size->name}}</td>  
                                <td>{{$adjust->qty}}</td> 
                                <td>{{$adjust->bQty??'-'}}</td>  
                            @endif
                        </tr>
                    @endforeach
                @endforeach
              </tbody>
          </table> 
          <div class="float-right my-3 pr-3">
              {{$adjustments->links()}}
            </div>         
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

      <!-- Default box -->
      <div class="card" x-show.transition.origin.top.left="card_name == 'form'" x-cloak>
        <div class="card-header">
          <h3 class="card-title">Add Adjustment</h3>
        </div>
        <form action="" wire:submit.prevent="store">
        <div class="card-body">
                                                                
                <div class="row">
                    <div class="col-sm-6"> 
                      <div class="form-group">
                    <label for="keterangan">Keterangan</label>
                    <select class="form-control @error('keterangan') is-invalid @enderror" id="keterangan" placeholder="" wire:model.defer="keterangan">
                        <option value="">Pilih Keterangan</option>
                        <option value="tambah">tambah</option>
                        <option value="kurang">kurang</option>
                    </select>
                    @error('keterangan')
                         <div class="invalid-feedback"> {{$message}} </div>
                    @enderror
                  </div>                        
                      <div class="form-group">  
                        <div wire:ignore>
                        <label for="model">Model</label>                          
                        <select class="form-control @error('model') is-invalid @enderror" id="model" placeholder="model">
                            <option value="">Pilih</option>                            
                        </select>
                        </div>
                        @error('model')
                             <div class="is-invalid"></div>
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div> 
                      <div class="form-group">  
                        <div wire:ignore>
                        <label for="product">Product</label>                          
                        <select class="form-control @error('product') is-invalid @enderror" id="product" placeholder="product">
                            <option value="">Pilih</option>                            
                        </select>
                        </div>
                        @error('product')
                             <div class="is-invalid"></div>
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div>                       
                      
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">  
                        <div wire:ignore>
                        <label for="size">Size</label>                          
                        <select class="form-control @error('size') is-invalid @enderror" id="size" placeholder="size">
                            <option value="">Pilih</option>                            
                        </select>
                        </div>
                        @error('size')
                             <div class="is-invalid"></div>
                             <div class="invalid-feedback"> {{$message}} </div>
                        @enderror
                      </div> 
                        <div class="form-group">
                    <label for="qty">Qty</label>
                    <input type="text" class="form-control @error('qty') is-invalid @enderror" name="qty" wire:model="qty" placeholder="Qty">
                    @error('qty')
                         <div class="invalid-feedback"> {{$message}} </div>
                    @enderror
                  </div>                  
                    </div>
                    <div class="col-sm-12 mb-3">
                        <button class="btn btn-primary btn float-right" wire:click.prevent="addDetail()">Add Detail</button>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-12">
                        @error('detailTable') <div class="is-invalid"></div><div class="invalid-feedback pb-2">Detail Item cannot be left blank</div>  @enderror
                        <table class="table">
                            <thead>
                                <th>#</th>
                                <th>Keterangan</th>
                                <th>Model</th>
                                <th>Produk</th>
                                <th>Size</th>
                                <th>Qty</th>                                                              
                                <th>#</th>
                            </thead>
                            <tbody>
                                @foreach($detailTable??[] as $key => $item)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>{{$item['keterangan']}}</td>
                                        <td>{{$item['model_text']}}</td>
                                        <td>{{$item['product_text']}}</td>
                                        <td>{{$item['size_text']}}</td>
                                        <td>{{$item['qty']}}</td>                                                                                         
                                        <td><button class="btn btn-danger btn-xs" wire:click.prevent="deleteDetail({{$key}})"><i class="fas fa-minus"></i></button></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                  
        <!-- /.card-body -->
        <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <button type="submit" class="btn btn-default float-right" x-on:click.prevent="cancel()">Cancel</button>
                </div>
                </form>
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->

@push('scripts')

    <script type="text/javascript">
        
        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        const test = {
            getData(id){
                this.card_name = 'form'
                @this.call('getData',id)
            },
            cancel(){
                this.card_name = 'table'
                @this.call('resetData');
                
            }
        }

         $("#model").select2({            
            allowClear: true,
            placeholder: "Pilih Produk",
            ajax: {
                dataType: "json",
                url: "{{route('res.model')}}",
                delay: 300,
                data: function (params) {
                    return {
                        _token: CSRF_TOKEN,
                        search: params.term,
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data,
                    };
                },
            },
        }).on('select2:select', function (evt) {
                    var text = $("#model option:selected").text();
                    var id = $("#model option:selected").val();
                    @this.set('model', id);
                    @this.set('model_text', text);
                    Livewire.emit('setProduct',id);
                });


        window.livewire.on('setProduct', (model) => {
            $('#product').val('').trigger('change');
            $('#size').val('').trigger('change');
            $("#product").select2({
            allowClear: true,
            placeholder: "Pilih Produk",
            ajax: {
                dataType: "json",
                url: "{{route('res.item_by_model')}}",
                delay: 300,
                data: function (params) {
                    return {
                        _token: CSRF_TOKEN,
                        search: params.term,
                        model
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data,
                    };
                },
            },
        }).on('select2:select', function (evt) {
                    var text = $("#product option:selected").text();
                    var id = $("#product option:selected").val();
                    @this.set('product', id);
                    @this.set('product_text', text);
                    Livewire.emit('setSize',id);
                });

                });


        window.livewire.on('setSize', (product) => {  
         $('#size').val('').trigger('change');          
            $("#size").select2({
            allowClear: true,
            placeholder: "Pilih Size",
            ajax: {
                dataType: "json",
                url: "{{route('res.size_by_item')}}",
                delay: 300,
                data: function (params) {
                    return {
                        _token: CSRF_TOKEN,
                        search: params.term,
                        product
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data,
                    };
                },
            },
        }).on('select2:select', function (evt) {
                    var text = $("#size option:selected").text();
                    var id = $("#size option:selected").val();
                    @this.set('size', id);                  
                    @this.set('size_text', text);                  
                });
        });
        

        function deleteData(id){
            Swal.fire({
        title: 'Yakin dihapus?',
        text: 'Data akan dihapus!',
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Delete!'
    }).then((result) => {
        if (result.value) {
            @this.call('destroy',id)
            responseAlert({title: session('message'), type: 'success'});
        } else {
            responseAlert({
                title: 'Operation Cancelled!',
                type: 'success'
            });
        }
    });
        }

        window.livewire.on('resetDetail',()=>{
            $("#model").val('').trigger('change');
            $('#product').val('').trigger('change');
            $('#size').val('').trigger('change');
        })

        window.livewire.on('flash_message', (message) => {
            Swal.fire(
                'Success!',
            message,
                'success'
            )        
        });

        window.livewire.on('flash_error', (message) => {
            Swal.fire(
                'Oops...!', 
            message,
                'error'
            )
        });
        
    </script>
@endpush
</div>