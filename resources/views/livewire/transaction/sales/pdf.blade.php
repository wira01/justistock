<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">    
    <title>Justiboy - Invoice #{{$sales->no_pesanan}}</title>
    <style>
        @charset "UTF-8";

        @page {
                margin: 25px 35px;

            }

        body{
            font-family: Tahoma,sans-serif;
            padding: 0;
            margin: 0;
            font-size: 12px;            
        }

        header {                
                position: fixed;                        
                left: 0cm;
                right: 0cm;
                top:0;     
                height: 100px;
                /** Extra personal styles **/                
            }          

        footer {
            text-align: center;
            position: fixed; 
                width: 100%;
                bottom: 30px; 
                left: 0px; 
                right: 0px;
        }

        .inline .column{
            display: inline-block;            
            width: 48%;
            vertical-align: middle;
        }
        .column-25{
            width: 75%;
        }
        .column-75{
            width: 24%;
        }
        .alamat_customer{
            text-align: right;
        }

        .header_title{
            font-size: 25px;
        }
        .pre_title{            
            margin-top: -25px;
            font-size: 20px;
        }

        main {
                padding-top:100px;                
            }

        .form-group{
            margin-bottom: 5px;
        }

        .form-group span{
            display: inline-block;
            margin-left: 5px;
        }

        .form-group span.label{
            margin-left: 0;
            width: 200px;
        }

        table{
            text-align: left;
            width: 100%;
            border-collapse: collapse;
        }

        table thead{            
            border-bottom: 1px solid;
            border-top: 1px solid; 
        }

        table thead tr th,table tbody tr td{
            padding: 8px;
            text-align: left;
        }

        table tfoot td{
            padding:8px;
            border: 1px solid;
        }
        table tfoot td:first-child{
            border-left: none;            
        }
        table tfoot td:last-child{
            border-right: none;            
        }

    </style>
</head>
<body>
    <header>
        <div class="inline">
            <div class="column">
                <div class="alamat_toko">
                    <div class="logo_toko">
                    <img width="250px" style="position: relative;top:10px" src="{{public_path('images/logo_justi.png')}}" alt="logo">
                    <h3><strong></strong></h3>
                    </div>                    
                </div>
            </div>
            <div class="column">
                <div class="alamat_customer">
                <h3 class="header_title"><strong>INVOICE</strong></h3>
                <p class="pre_title">#{{$sales->no_pesanan}}</p>               
            </div>
        </div>
        </div>
    </header>
    <footer>        
        <p style="margin-top:-10px">Terima kasih telah berbelanja. Semoga tetap menjadi langganan. </p>
    </footer>

    <main style="page-break-after: all">    
    <div class="inline">
        <div class="column column-75">
        </div>
        <div class="column column-25">
            <div style="margin-top:15px"></div>            
            <div class="form-group">
                <span class="label">Pembeli</span>
                <span class="dot">:</span>
                <span class="field">{{$sales->contact??$sales->marketplace->name}}</span>
            </div>
            <div class="form-group">
                <span class="label">Tanggal Pembelian</span>
                <span class="dot">:</span>
                <span class="field">{{Carbon\Carbon::parse($sales->tanggal)->format('d F Y')}}</span>
            </div>
        </div>
    </div>
    <div class="inline">
        <table>
            <thead>
                <tr>
                <th>Info Produk</th>
                <th>Jumlah</th>
                <th>Harga Satuan</th>
                <th>Total Harga</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $total = 0;
                @endphp
                @foreach($sales->detail as $key => $item)                
                <tr>
                    <td>
                        <strong>{{$item->detailItem->item->model->name}} - {{$item->detailItem->item->name}} - {{$item->detailItem->size->name}}</strong><br>
                        {{$item->detailItem->item->sku}}
                    </td>
                    <td>{{$item->qty}}</td>
                    <td>{{formatRupiah($item->sales_price)}}</td>
                    <td>{{formatRupiah($item->total)}}</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3"><strong>Total Pembelian</strong></td>
                    <td>{{formatRupiah($sales->detail_sum_total)}}</td>
                </tr>
            </tfoot>
        </table>
    </div>      

    
    </main>
</body>