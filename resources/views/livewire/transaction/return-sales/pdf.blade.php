<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ public_path('css/pdf_style.css') }}" rel="stylesheet">
    <title>Justiboy - Retur Penjualan</title>
</head>
<body>
    <main style="page-break-after: all">
    <div class="headerPdf" style="text-align: center;">
        <h1>Retur Penjualan</h1>
            <h5>#{{$return_sales->no_pesanan}}</h5>
    </div>
    <div class="inline">
        <div class="inline_data">
            <table class="table">
              <thead>
                  <tr>
                      
                      <th rowspan="2" style="width: 1%">
                          #
                      </th>                      
                      <th rowspan="2" style="width:10%">
                          Tanggal
                      </th>   
                      <th rowspan="2" style="width:5%">
                          Marketplace
                      </th>
                      <th rowspan="2" style="width:15%">
                          No Pesanan
                      </th>
                      <th rowspan="2" style="width:15%">
                          Customer
                      </th>                       
                      <th colspan="5">Detail</th>                      
                  </tr>
                  <tr>
                      <th>
                          SKU
                      </th>
                      <th>
                          Model
                      </th>
                      <th>
                          Produk
                      </th>
                      <th>
                          Size
                      </th>
                      <th>
                          Qty
                      </th>                     
                  </tr>                                  
              </thead>
              <tbody>
                
                    @foreach($return_sales->detail as $k => $kerja)
                      @php
                        $rowspan = $return_sales->detail->count();                                            
                    @endphp
                        <tr>
                            @if($k == 0)
                                <td rowspan="{{$rowspan}}">{{$k+1}}</td>
                                <td rowspan="{{$rowspan}}">{{$return_sales->created_at->format('Y-m-d')}}</td>
                                <td rowspan="{{$rowspan}}">{{$return_sales->marketplace->name}}</td>
                                <td rowspan="{{$rowspan}}">{{$return_sales->no_pesanan}}</td>
                                <td rowspan="{{$rowspan}}">{{$return_sales->contact??$return_sales->marketplace->name}}</td>
                                <td>{{$kerja->detailItem->item->sku}}</td>  
                                <td>{{$kerja->detailItem->item->model->name}}</td>  
                                <td>{{$kerja->detailItem->item->name}}</td>                                   
                                <td>{{$kerja->detailItem->size->name}}</td>  
                                <td>{{$kerja->qty}}</td>                                 
                            @else
                                <td>{{$kerja->detailItem->item->sku}}</td>  
                                <td>{{$kerja->detailItem->item->model->name}}</td>  
                                <td>{{$kerja->detailItem->item->name}}</td>                                   
                                <td>{{$kerja->detailItem->size->name}}</td>  
                                <td>{{$kerja->qty}}</td> 
                            @endif
                        </tr>
                    @endforeach                
              </tbody>                 
          </table>  
        </div>
    </div>
    </main>
</body>