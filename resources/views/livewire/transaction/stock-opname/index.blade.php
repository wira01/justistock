
<div id="wrapper" x-data="{card_name:  @entangle('showCard'),...test,filtering_by:false}">
   @section('title')
    Justiboy - Laporan Stock Opname
   @endsection
   @push('styles')
   <style type="text/css">
    .table th{
        vertical-align: middle!important;
    }
   </style>
   @endpush
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Laporan Stock Opname</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Laporan Stock Opname</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card" x-show.transition.origin.top.left="card_name == 'table'">
        <div class="card-header">
          <h3 class="card-title">Filter By : <button type="button" class="btn btn-tool" @click="filtering_by = !filtering_by">
              <i class="fas fa-filter" :class="filtering_by == true?'text-primary':''"></i>
            </button></h3> 
            <div class="card-tools">
            <button type="button" class="btn btn-primary" @click="card_name = 'form'">
              <span>Add Stock Opname</span>
            </button>                           
          </div>        
        </div>
        <div class="card-body p-0">
          <div class="row mt-3">
            <div class="col-md-12">
            <div class="card">                        
              <div class="card-body" x-show.transition.origin.top.left="filtering_by == true" x-cloak>
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row align-items-center">
                            <label for="q" class="col-sm-2 col-form-label">Date :</label> 
                          <div class="input-group col-sm-8 py-3" wire:ignore>      
                            <x-input.date class="mr-3" id="dateFrom" placeholder="{{now()->format('Y-m-d')}}"/>
                            <x-input.date class="mr-3" id="dateTo" placeholder="{{now()->format('Y-m-d')}}"/>
                           </div>
                           </div>                                                                         
                  </div>
                  
              </div>
          </div>   
          </div>           
          </div>
          </div>
          <table class="table table-bordered text-center">
              <thead>
                  <tr>
                      
                      <th style="width: 1%">
                          #
                      </th>
                      <th style="width:5%">
                          S.N
                      </th>                      
                      <th style="width:10%">
                          Date
                      </th>                         
                      <th style="width:10%">
                          Action
                      </th>                                        
                  </tr>                  
              </thead>
              <tbody>
                 @foreach($opnames as $key => $opname)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$opname->no_opname}}</td>
                        <td>{{$opname->created_at->format('Y-m-d')}}</td>
                        <td><button class="btn btn-danger btn-xs" x-on:click="deleteData({{$opname->id}})"><i class="fas fa-trash"></i></button><a class="btn btn-primary btn-xs ml-1" target="_blank" href="{{route('pdf.opname',['id'=>$opname->id])}}"><i class="fas fa-eye"></i></a></td>
                    </tr>
                 @endforeach
              </tbody>
          </table> 
          <div class="float-right my-3 pr-3">
              {{$opnames->links()}}
            </div>         
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
      <div class="card" x-show.transition.origin.top.left="card_name == 'form'" x-cloak>
        <div class="card-header">
          <h3 class="card-title">Add Stock Opname</h3>
          
        </div>
        <form action="" x-on:submit.prevent="save">
        <div class="card-body">

            <div>
                <div class="form-group row mt-3">
                    <div class="col-6">
                        <div class="row">
                    <label class="col-sm-2" for="">Category :</label>
                    <div class="col-sm-5"> <input class="form-control" type="text" wire:model="category_filter">   </div>
                    </div>
                    </div>
          </div>
                <table class="table table-bordered text-center">
              <thead>
                  <tr>
                      
                      <th style="width: 1%">
                          #
                      </th>
                      <th style="width:5%">
                          Category
                      </th>                      
                      <th style="width:10%">
                          Model
                      </th>  
                      <th style="width:10%">
                          Size
                      </th>   
                      <th style="width:10%">
                          Qty
                      </th>  
                      <th style="width:10%">
                          Inventory Stock
                      </th>                         
                      <th style="width:10%">
                          Checklist
                      </th>                                        
                  </tr>                  
              </thead>
              <tbody>                
                 @foreach($stocks as $s => $stock)
                    <tr>
                        <td >{{$s+1}}</td>
                        <td class="category_name">{{$stock->category_name}}</td>
                        <td class="model_name">{{$stock->model_name}}</td>
                        <td class="size_name">{{$stock->size_name}}</td>
                        <td class=""><input class="form-control input_qty" type="number" required></td>
                        <td class="total">{{$stock->total}}</td>
                        <td class="input_status"></td>
                    </tr>
                 @endforeach
              </tbody>
          </table> 
            </div>              
        <!-- /.card-body -->
        <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <button type="submit" class="btn btn-default float-right" x-on:click.prevent="cancel()">Cancel</button>
                </div>
                </form>
      </div>
      <!-- /.card -->


    </section>
    <!-- /.content -->

@push('scripts')

    <script type="text/javascript">
        
        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        const test = {
            getData(id){
                this.card_name = 'form'
                @this.call('getData',id)
            },
            cancel(){
                this.card_name = 'table'
                @this.call('resetData');
                
            }
        }

        $(document).on('input','.input_qty',function(){

            status = '';
            total = $(this).parent().parent().find('td.total').text();
            input_status = $(this).parent().parent().find('td.input_status'); 
            input_status.find('i').remove();           
            if($(this).val() > total){
                input_status.append('<i class="fas fa-times text-danger"></i>');
            }else if($(this).val() < total){                
                input_status.append('<i class="fas fa-times text-danger"></i>');
            }else{
                input_status.append('<i class="fas fa-check text-success"></i>');
            }
        })

         $("#model").select2({            
            allowClear: true,
            placeholder: "Pilih Produk",
            ajax: {
                dataType: "json",
                url: "{{route('res.model')}}",
                delay: 300,
                data: function (params) {
                    return {
                        _token: CSRF_TOKEN,
                        search: params.term,
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data,
                    };
                },
            },
        }).on('select2:select', function (evt) {
                    var text = $("#model option:selected").text();
                    var id = $("#model option:selected").val();
                    @this.set('model', id);
                    @this.set('model_text', text);
                    Livewire.emit('setProduct',id);
                });


        window.livewire.on('setProduct', (model) => {
            $('#product').val('').trigger('change');
            $('#size').val('').trigger('change');
            $("#product").select2({
            allowClear: true,
            placeholder: "Pilih Produk",
            ajax: {
                dataType: "json",
                url: "{{route('res.item_by_model')}}",
                delay: 300,
                data: function (params) {
                    return {
                        _token: CSRF_TOKEN,
                        search: params.term,
                        model
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data,
                    };
                },
            },
        }).on('select2:select', function (evt) {
                    var text = $("#product option:selected").text();
                    var id = $("#product option:selected").val();
                    @this.set('product', id);
                    @this.set('product_text', text);
                    Livewire.emit('setSize',id);
                });

                });


        window.livewire.on('setSize', (product) => {            
            $("#size").select2({
            allowClear: true,
            placeholder: "Pilih Size",
            ajax: {
                dataType: "json",
                url: "{{route('res.size_by_item')}}",
                delay: 300,
                data: function (params) {
                    return {
                        _token: CSRF_TOKEN,
                        search: params.term,
                        product
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data,
                    };
                },
            },
        }).on('select2:select', function (evt) {
                    var text = $("#size option:selected").text();
                    var id = $("#size option:selected").val();
                    @this.set('size', id);                  
                    @this.set('size_text', text);                  
                });
        });
        

        function deleteData(id){
            Swal.fire({
        title: 'Yakin dihapus?',
        text: 'Data akan dihapus!',
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Delete!'
    }).then((result) => {
        if (result.value) {
            @this.call('destroy',id)
            responseAlert({title: session('message'), type: 'success'});
        } else {
            responseAlert({
                title: 'Operation Cancelled!',
                type: 'success'
            });
        }
    });
        }

        window.livewire.on('flash_message', (message) => {
            Swal.fire(
                'Success!',
            message,
                'success'
            )        
        });

        window.livewire.on('flash_error', (message) => {
            Swal.fire(
                'Oops...!', 
            message,
                'error'
            )
        });

        function save(){
            array = [];
            $('.input_qty').each(function(index){
                array.push({
                    'category':$('.category_name').eq(index).text(),
                    'model':$('.model_name').eq(index).text(),
                    'size':$('.size_name').eq(index).text(),
                    'qty':$('.total').eq(index).text(),
                    'input':$(this).val(),
            });
        });
            @this.call('store',array);
        }


        
    </script>
@endpush
</div>