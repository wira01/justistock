<div>
    <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('home')}}" class="brand-link">      
      <span class="brand-text font-weight-light">Welcome , {{auth()->user()->username??''}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">      
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{route('home')}}" class="nav-link {{$mmenu == 'dashboard' ? 'active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard                
              </p>
            </a>            
          </li>   
          @foreach ($menus as $menu)
                    @if ($menu->parent_id == 0)
                      <li class="nav-item @if ($mmenu === $menu->alias) menu-is-opening menu-open @endif">
                          <a href="{{ url($menu->url) }}" class="nav-link {{$mmenu == $menu->alias ? 'active' : '' }}">
                              <i class="nav-icon {{$menu->icon}}"></i>
                              <p >{{$menu->name}}</p>
                              @if (isset($menu->children))
                                <i class="fas fa-angle-left right"></i>
                              @endif
                          </a>
                          @if (isset($menu->children))                                                    
                            <ul class="nav nav-treeview" @if ($mmenu === $menu->alias) style="display: block" @endif>
                              @foreach ($menu->children as $child)
                                @if (isset($child->children))
                                  <li class="{{$submenu == $child->alias ? 'active' : '' }}">
                                    <a href="#" class="nav-item">
                                        <p>{{$child->name}}</p>
                                        <i class="fas fa-angle-left right"></i>
                                    </a>
                                      <ul @if ($submenu === $child->alias) style="display: block" @endif>
                                        @foreach ($child->children as $subchild)
                                          <li class="nav-item"><a href="{{ url($subchild->url) }}" class="nav-link {{$submenu2 == $subchild->alias ? 'active' : '' }}"><p>{{$subchild->name}}</p></a></li>
                                        @endforeach
                                      </ul>
                                  </li>
                                  @else                                    
                                  <li  class="nav-item">
                                      <a href="{{ url($child->url) }}" class="nav-link {{$submenu == $child->alias ? 'active' : '' }}"><i class="far fa-circle nav-icon"></i><p>{{$child->name}}</p></a>
                                  </li>                              
                                @endif
                              @endforeach
                            </ul>
                          @endif
                      </li>
                    @endif
                  @endforeach       
          <li class="nav-item">
            <a href="{{route('logout')}}" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
</div>
