<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ public_path('css/pdf_style.css') }}" rel="stylesheet">
    <title>Justiboy - Report Stock Opname - {{$opname->no_opname}}</title>
</head>
<body>
    <main style="page-break-after: all">
    <div class="headerPdf" style="text-align: center;">
        <h1>Report Stock Opname - {{$opname->no_opname}}</h1>
            <h5>{{$opname->created_at->format('d F Y')}}</h5>
    </div>
    <div class="inline">
        <div class="inline_data">
            <table class="table" align="center">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Category</th>
                        <th>Model</th>
                        <th>Size</th>
                        <th>Qty</th>
                        <th>Inventory Qty</th>
                        <th>Checklist</th>
                    </tr>
                </thead>
                @php
                  $opnames =  json_decode($opname->detail_opname);                  
                @endphp    
                @foreach($opnames as $key => $op)
                    @php
                        $image = ($op->input == $op->qty)?'check.jpg':'close.jpg';
                    @endphp
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$op->category}}</td>
                        <td>{{$op->model}}</td>
                        <td>{{$op->size}}</td>
                        <td>{{$op->input}}</td>
                        <td>{{$op->qty}}</td>
                        <td><img width="15" style="padding:5px" src="{{public_path('images/'.$image)}}" alt=""></td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    </main>
</body>