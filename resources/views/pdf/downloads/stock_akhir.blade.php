<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ public_path('css/report_style.css') }}" rel="stylesheet">
    <title>Justiboy - Laporan Stok Akhir</title>
</head>
<body>
    <main style="page-break-after: all">
    <div class="headerPdf" style="text-align: center;">
        <h1>Laporan Stok Akhir</h1>
            <h5>Periode {{$dateTo?$dateFrom.' - '.$dateTo:$dateFrom}}</h5>
    </div>
    <div class="inline">
        <div class="inline_data">
            <table class="table" border="1">              
                  <thead>
                  <tr>
                      
                      <th style="width: 1%">
                          #
                      </th><th style="width: 5%">
                          Name
                      </th>
                      <th style="width:5%">
                          Model
                      </th>                      
                      <th style="width:10%">
                          Size
                      </th>   
                      <th style="width:5%">
                          Qty
                      </th>                                   
                  </tr>                  
              </thead>
              <tbody>
                @foreach($stocks as $key => $stock)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$stock->name}} - {{$stock->sku}}</td>
                        <td>{{$stock->model}}</td>                        
                        <td>{{$stock->size}}</td>                        
                        <td>{{$stock->stok_akhir}}</td>                        
                    </tr>
                  @endforeach
              </tbody>              
          </table>  
        </div>
    </div>
    </main>
</body>