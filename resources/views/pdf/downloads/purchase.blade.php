<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ public_path('css/report_style.css') }}" rel="stylesheet">
    <title>Justiboy - Laporan Pembelian</title>
</head>
<body>
    <main style="page-break-after: all">
    <div class="headerPdf" style="text-align: center;">
        <h1>Laporan Pembelian</h1>
            <h5>Periode {{$dateTo?$dateFrom.' - '.$dateTo:$dateFrom}}</h5>
    </div>
    <div class="inline">
        <div class="inline_data">
            <table class="table table-report" width="50%" border="1">              
                  <tr>                    
                      <th style="width:10%">
                          Tanggal
                      </th>   
                      <th style="width:5%">
                          Marketplace
                      </th>
                      <th style="width:5%">
                          Qty
                      </th>
                  </tr>                                  
              <tbody>
                @php
                        $alltotal = 0;
                @endphp
                @foreach($purchases as $purchase)
                    @php
                        $total = 0;
                    @endphp
                      @foreach($marketplaces as $m => $marketplace)
                        @php
                            $total += gotQuantity($marketplace->id,$purchase->tanggal,'beli')->sum_qty??0;
                            $alltotal += gotQuantity($marketplace->id,$purchase->tanggal,'beli')->sum_qty??0;
                        @endphp
                        <tr>
                            
                            <td>@if($m == 0){{Carbon\Carbon::parse($purchase->tanggal)->format('Y-m-d')}}@endif</td>
                            
                            <td>{{$marketplace->name}}</td>
                            <td align="right">{{gotQuantity($marketplace->id,$purchase->tanggal,'beli')->sum_qty??0}}</td>
                        </tr>
                      @endforeach
                      <tr>
                          <td colspan="2">Total</td>
                          <td align="right" >{{$total}}</td>
                      </tr>
                @endforeach 
              </tbody>
              <tfoot>
                  <tr>
                        <td colspan="2">Total Qty : </td>  
                        <td align="right"><strong>{{$alltotal}}</strong></td>
                  </tr>
              </tfoot>
          </table>  
        </div>
    </div>
    </main>
</body>