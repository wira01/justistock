<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ public_path('css/pdf_style.css') }}" rel="stylesheet">
    <title>Justiboy - Template Stock Opname</title>
</head>
<body>
    <main style="page-break-after: all">
    <div class="headerPdf" style="text-align: center;">
        <h1>Template Stock Opname</h1>
            <h5>{{now()->format('d F Y')}}</h5>
    </div>
    <div class="inline">
        <div class="inline_data">
            <table class="table" align="center">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Category</th>
                        <th>Model</th>
                        <th>Size</th>
                        <th>Qty</th>
                        <th>Inventory Qty</th>
                        <th>Checklist</th>
                    </tr>
                </thead>
                
                @foreach($stocks as $s => $stock)
                    <tr>
                        <td >{{$s+1}}</td>
                        <td class="category_name">{{$stock->category_name}}</td>
                        <td class="model_name">{{$stock->model_name}}</td>
                        <td class="size_name">{{$stock->size_name}}</td>
                        <td class=""><input class="form-control input_qty" type="number" required></td>
                        <td class="total">{{$stock->total}}</td>
                        <td class="input_status"></td>
                    </tr>
                 @endforeach
            </table>
        </div>
    </div>
    </main>
</body>