<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ public_path('css/pdf_style.css') }}" rel="stylesheet">
    <title>Justiboy - Laporan Pembelian</title>
</head>
<body>
    <main>
    <div class="headerPdf" style="text-align: center;">
        <h1>Laporan Pembelian</h1>
            <h5>Periode {{$dateTo?$dateFrom.' - '.$dateTo:$dateFrom}}</h5>
    </div>
    <div class="inline">
        <div class="inline_data">
            <table class="table table-detail">
              
                  <tr>
                      <th rowspan="2" style="width: 1%">
                          #
                      </th>                      
                      <th rowspan="2" style="width:1%">
                          Tanggal
                      </th>   
                      <th rowspan="2" style="width:1%">
                          Marketplace
                      </th>
                      <th rowspan="2" style="width:2%">
                          No Pesanan
                      </th>
                      <th rowspan="2" style="width:1%">
                          Supplier
                      </th>
                       <th rowspan="2" style="width:2%">
                          Total
                      </th >
                      <th colspan="6" style="width:40%">Detail</th>                 
                  </tr>
                  <tr>
                      <th style="width: 1%;">
                          SKU
                      </th>
                      <th style="width:1%">
                          Model
                      </th>
                      <th style="width:3%">
                          Produk
                      </th>
                      <th style="width:1%">
                          Size
                      </th>
                      <th style="width:1%">
                          Qty
                      </th>
                      <th style="width:3%">
                          Price
                      </th>
                  </tr>                                                
              <tbody>
                @php
                    $total = 0;
                    $alltotal = 0;
                @endphp
                @foreach($purchases as $key => $purchase)
                    @php
                        $rowspan = $purchase->detail->count();                                            
                    @endphp
                    @php
                            $total += $purchase->detail_sum_total;
                        @endphp
                    @foreach($purchase->detail as $k => $kerja)
                        @php
                            $alltotal += $kerja->qty;
                        @endphp
                        <tr>
                            @if($k == 0)
                                <td rowspan="{{$rowspan}}">{{$key+1}}</td>
                                <td rowspan="{{$rowspan}}">{{Carbon\Carbon::parse($purchase->tanggal)->format('d-m-y')}}</td>
                                <td rowspan="{{$rowspan}}">{{$purchase->marketplace->name}}</td>
                                <td rowspan="{{$rowspan}}">{{wordwrap($purchase->no_pesanan, 15, "\n",true)}}</td>
                                <td rowspan="{{$rowspan}}">{{$purchase->contact??$purchase->marketplace->name}}</td>
                                <td rowspan="{{$rowspan}}">{{formatRupiah($purchase->detail_sum_total)}}</td>
                                <td>{{$kerja->detailItem->item->sku}}</td>  
                                <td>{{$kerja->detailItem->item->model->name}}</td>  
                                <td>{{wordwrap($kerja->detailItem->item->name,50,"\n",true)}}</td>                                   
                                <td>{{$kerja->detailItem->size->name}}</td>  
                                <td>{{$kerja->qty}}</td>           
                                <td>{{formatRupiah($kerja->total)}}</td>  
                                                      
                            @else
                                <td>{{$kerja->detailItem->item->sku}}</td>  
                                <td>{{$kerja->detailItem->item->model->name}}</td>  
                                <td>{{$kerja->detailItem->item->name}}</td>                                   
                                <td>{{$kerja->detailItem->size->name}}</td>  
                                <td>{{$kerja->qty}}</td> 
                                <td>{{formatRupiah($kerja->total)}}</td>                                  
                            @endif
                        </tr>
                    @endforeach
                @endforeach
                  
              </tbody>
              <tfoot>
                  <tr>
                      <td colspan="2">Total Purchase</td>
                      <td colspan="4">{{formatRupiah($total)}}</td>
                      <td colspan="2">Total Qty</td>
                      <td colspan="4">{{$alltotal}}</td>
                  </tr>
              </tfoot>     
          </table>  
        </div>
    </div>
    </main>
</body>