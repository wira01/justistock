<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ public_path('css/pdf_style.css') }}" rel="stylesheet">
    <title>Justiboy - Laporan Adjustment</title>
</head>
<body>
    <main style="page-break-after: all">
    <div class="headerPdf" style="text-align: center;">
        <h1>Laporan Adjustment</h1>
            <h5>Periode {{$dateTo?$dateFrom.' - '.$dateTo:$dateFrom}}</h5>
    </div>
    <div class="inline">
        <div class="inline_data">
            <table class="table">
              <thead>
                  <tr>
                      
                      <th rowspan="2" style="width: 1%">
                          #
                      </th>                    
                      <th rowspan="2" style="width:10%">
                          Tanggal
                      </th>                         
                      <th rowspan="2" style="width:15%">
                          No Adjustment
                      </th>
                      <th colspan="6">Detail</th>
                  </tr>
                  <tr>
                      <th>
                          Keterangan
                      </th>
                      <th>
                          SKU
                      </th>
                      <th>
                          Model
                      </th>
                      <th>
                          Produk
                      </th>
                      <th>
                          Size
                      </th>
                      <th>
                          Qty
                      </th>
                  </tr>
              </thead>
              <tbody>                
                  @foreach($adjustments as $key => $adjustment)
                    @php
                        $rowspan = $adjustment->detail->count();                                            
                    @endphp
                    @foreach($adjustment->detail as $k => $adjust)

                        <tr>
                            @if($k == 0)
                                <td rowspan="{{$rowspan}}">{{$key+1}}</td>                                
                                <td rowspan="{{$rowspan}}">{{$adjustment->created_at->format('Y-m-d')}}</td>
                                <td rowspan="{{$rowspan}}">{{$adjustment->no_adjustment}}</td>                                
                                <td>{{$adjust->keterangan}}</td>  
                                <td>{{$adjust->detailItem->item->sku}}</td>  
                                <td>{{$adjust->detailItem->item->model->name}}</td>  
                                <td>{{$adjust->detailItem->item->name}}</td>                                   
                                <td>{{$adjust->detailItem->size->name}}</td>  
                                <td>{{$adjust->qty}}</td>  
                            @else
                                <td>{{$adjust->keterangan}}</td>  
                                <td>{{$adjust->detailItem->item->sku}}</td>  
                                <td>{{$adjust->detailItem->item->model->name}}</td>  
                                <td>{{$adjust->detailItem->item->name}}</td>                                   
                                <td>{{$adjust->detailItem->size->name}}</td>  
                                <td>{{$adjust->qty}}</td> 
                            @endif
                        </tr>
                    @endforeach
                @endforeach
              </tbody>
          </table> 
        </div>
    </div>
    </main>
</body>