<div wire:ignore class="{{$attributes['class']}}">
    @push('styles')
        <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
    @endpush
    <input type="text" class="form-control" id="{{$attributes['id']}}" placeholder="{{$attributes['placeholder']}}" wire:model.defer="{{$attributes['id']}}">
    @push('scripts')
        <script src="{{asset('plugins/moment/moment.min.js')}}"></script>
        <script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
        <script>
            $('#{{$attributes['id']}}').daterangepicker({
                singleDatePicker: true,
            }).on('change',function(e){

                var text = $('#{{$attributes['id']}}').val();                
                @this.set('{{$attributes['id']}}',text);                
            })
        </script>
    @endpush
</div>
