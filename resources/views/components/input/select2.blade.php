<div wire:ignore>
    <select class="form-control" id="{{$attributes['id']}}" name="{{$attributes['name']}}">
        <option value="">-Pilih-</option>
    </select> 
    @push('scripts')
        <script>
            $("#{{$attributes['id']}}").select2({
        allowClear: true,
        placeholder: "{{$attributes['placeholder']}}",
        ajax: {
            dataType: "json",
            url: "{{route($attributes['route'])}}",
            delay: 300,
            data: function (params) {
                return {
                    _token: CSRF_TOKEN,
                    search: params.term,
                };
            },
            processResults: function (data, page) {
                return {
                    results: data,
                };
            },
        },
    }).on('select2:select', function (evt) {
                var text = $("#{{$attributes['id']}} option:selected").text();
                var id = $("#{{$attributes['id']}} option:selected").val();
                @this.set('{{$attributes['name']}}', id);
            });
        
        </script>
    @endpush
</div>