<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ResourceController;

/*
|--------------------------------------------------------------------------
| Resource Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>['auth','auth.cred']], function () {    
    Route::get('/menus', [ResourceController::class,'getMenu'])->name('res.menus');    
    Route::get('/categories', [ResourceController::class,'getCategory'])->name('res.categories');    
    Route::get('/model', [ResourceController::class,'getModel'])->name('res.model');    
    Route::get('/marketplace', [ResourceController::class,'getMarketplace'])->name('res.marketplace');    
    Route::get('/size', [ResourceController::class,'getSize'])->name('res.size');    
    Route::get('/item', [ResourceController::class,'getItem'])->name('res.item');    
    Route::get('/size_by_item', [ResourceController::class,'getSizeByItem'])->name('res.size_by_item');    
    Route::get('/item_by_model', [ResourceController::class,'getItemByModel'])->name('res.item_by_model');  

    Route::get('/generateStockOpnamePDF', [ResourceController::class,'generateStockOpnamePDF'])->name('res.generateStockOpnamePDF');       
    Route::get('/sales/{id}/invoice', [ResourceController::class,'generateSalesInvoice'])->name('res.generateSalesInvoice');       
    Route::get('/purchase/{id}/generate', [ResourceController::class,'generatePurchase'])->name('res.generatePurchase');       
    Route::get('/return_sales/{id}/generate', [ResourceController::class,'generateReturnSales'])->name('res.generateReturnSales');       
});
