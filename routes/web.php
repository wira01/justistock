<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login',App\Http\Livewire\Auth\Login::class)->name('login');


Route::group(['middleware'=>['auth','auth.cred']], function () {
    Route::get('/', App\Http\Livewire\Dashboard\Index::class)->name('home');

    Route::group(['prefix'=>'master'],function(){
        Route::get('/roles', App\Http\Livewire\Master\Role\Index::class)->name('roles');
        Route::get('/permissions', App\Http\Livewire\Master\Permission\Index::class)->name('permissions');
        Route::get('/users', App\Http\Livewire\Master\User\Index::class)->name('users');
        Route::get('/menus', App\Http\Livewire\Master\Menu\Index::class)->name('menus');
        Route::get('/items', App\Http\Livewire\Master\Item\Index::class)->name('items');
        Route::get('/marketplace', App\Http\Livewire\Master\Marketplace\Index::class)->name('marketplace');
        Route::get('/warehouse', App\Http\Livewire\Master\Warehouse\Index::class)->name('warehouse');        
        Route::get('/categories', App\Http\Livewire\Master\Kategori\Index::class)->name('categories');
        Route::get('/models', App\Http\Livewire\Master\Model\Index::class)->name('models');
        Route::get('/sizes', App\Http\Livewire\Master\Size\Index::class)->name('sizes');        
        Route::get('/customer', App\Http\Livewire\Master\Customer\Index::class)->name('customer');        
    });

    Route::group(['prefix'=>'import'],function(){
        Route::get('/item', App\Http\Livewire\Import\Item\Index::class)->name('import.item'); 
        Route::get('/lembar-kerja', App\Http\Livewire\Import\LembarKerja\Index::class)->name('import.lembar_kerja'); 
    });



    Route::group(['prefix'=>'transaction'],function(){
        Route::get('/lembar-kerja', App\Http\Livewire\Transaction\LembarKerja\Index::class)->name('lembar_kerja');            
        Route::get('/adjustment', App\Http\Livewire\Transaction\Adjustment\Index::class)->name('adjustment');            
        Route::get('/sales', App\Http\Livewire\Transaction\Sales\Index::class)->name('sales');            
        Route::get('/purchase', App\Http\Livewire\Transaction\Purchase\Index::class)->name('purchase');            
        Route::get('/return-purchase', App\Http\Livewire\Transaction\ReturnPurchase\Index::class)->name('return_purchase');            
        Route::get('/return-sales', App\Http\Livewire\Transaction\ReturnSales\Index::class)->name('return_sales');            
    });

    Route::group(['prefix'=>'report'],function(){
        Route::get('/sales', App\Http\Livewire\Report\Sales\Index::class)->name('sales');
        Route::get('/purchase', App\Http\Livewire\Report\Purchase\Index::class)->name('purchase');
        Route::get('/adjustment', App\Http\Livewire\Report\Adjustment\Index::class)->name('adjustment');
        Route::get('/stocks', App\Http\Livewire\Transaction\Stock\Index::class)->name('stocks');     
        Route::get('/stock-akhir', App\Http\Livewire\Report\StockAkhir\Index::class)->name('stock.akhir');     
        Route::get('/stock-opname', App\Http\Livewire\Transaction\StockOpname\Index::class)->name('stock.opname');  
        Route::get('/stock-opname/generate/{id}', [App\Http\Controllers\Pdf\StockOpname::class,'index'])->name('pdf.opname');  

    });

    Route::get('/logout',function(){
        Auth::logout();
        Cache::flush();
        return redirect('login');
    })->name('logout');
});
