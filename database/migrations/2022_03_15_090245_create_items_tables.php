<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();            
            $table->foreignId('model_id')->constrained()->onUpdate('cascade')->onDelete('cascade');            
            $table->string('name');            
            $table->string('sku')->nullable();
            $table->string('warehouse')->nullable();
            $table->string('rack')->nullable();
            $table->string('unit');                                   
            $table->string('images')->nullable();                                
            $table->string('marketplace')->nullable();
            $table->text('notes')->nullable();
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->integer('deleted')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
