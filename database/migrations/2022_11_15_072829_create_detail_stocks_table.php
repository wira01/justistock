<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_stocks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('detail_item_id');
            $table->string('model');
            $table->string('size');
            $table->unsignedInteger('stok_awal');
            $table->unsignedInteger('stok_akhir');
            $table->string('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_stocks');
    }
}
