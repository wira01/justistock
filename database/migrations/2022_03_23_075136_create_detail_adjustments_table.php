<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_adjustments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('adjustment_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->string('keterangan');
            $table->foreignId('detail_item_id');
            $table->unsignedInteger('qty');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_adjustments');
    }
}
