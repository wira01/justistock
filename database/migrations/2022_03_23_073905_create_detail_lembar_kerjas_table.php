<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailLembarKerjasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_lembar_kerjas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('lembar_kerja_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('detail_item_id');
            $table->unsignedInteger('qty');
            $table->integer('modal_price');
            $table->integer('sales_price')->nullable();
            $table->integer('discount')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_lembar_kerjas');
    }
}
