<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Master\Menu;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Master
        Menu::create(['name'=>'Master','alias'=>'master','url'=>"#",'icon'=>'fas fa-master','parent_id'=>0,'created_by'=>'SYSTEM','index'=>0]);   
        Menu::create(['name'=>'Items','alias'=>'items','url'=>"/master/items",'icon'=>'fas fa-master','parent_id'=>$this->findParent('master'),'created_by'=>'SYSTEM','index'=>0]);   
        Menu::create(['name'=>'Permission','alias'=>'permission','url'=>"/master/permissions",'icon'=>'fas fa-master','parent_id'=>$this->findParent('master'),'created_by'=>'SYSTEM','index'=>1]);   
        Menu::create(['name'=>'Roles','alias'=>'roles','url'=>"/master/roles",'icon'=>'fas fa-master','parent_id'=>$this->findParent('master'),'created_by'=>'SYSTEM','index'=>2]);   
        Menu::create(['name'=>'Menu','alias'=>'menus','url'=>"/master/menus",'icon'=>'fas fa-master','parent_id'=>$this->findParent('master'),'created_by'=>'SYSTEM','index'=>3]);   
        Menu::create(['name'=>'User','alias'=>'users','url'=>"/master/users",'icon'=>'fas fa-master','parent_id'=>$this->findParent('master'),'created_by'=>'SYSTEM','index'=>4]);   
        Menu::create(['name'=>'Marketplace','alias'=>'marketplaces','url'=>"/master/marketplace",'icon'=>'fas fa-master','parent_id'=>$this->findParent('master'),'created_by'=>'SYSTEM','index'=>5]);   
        Menu::create(['name'=>'Gudang','alias'=>'warehouse','url'=>"/master/warehouse",'icon'=>'fas fa-master','parent_id'=>$this->findParent('master'),'created_by'=>'SYSTEM','index'=>6]);   
        Menu::create(['name'=>'Category','alias'=>'categories','url'=>"/master/categories",'icon'=>'fas fa-master','parent_id'=>$this->findParent('master'),'created_by'=>'SYSTEM','index'=>7]);   
        Menu::create(['name'=>'Model','alias'=>'models','url'=>"/master/models",'icon'=>'fas fa-master','parent_id'=>$this->findParent('master'),'created_by'=>'SYSTEM','index'=>8]);   
        Menu::create(['name'=>'Size','alias'=>'sizes','url'=>"/master/sizes",'icon'=>'fas fa-master','parent_id'=>$this->findParent('master'),'created_by'=>'SYSTEM','index'=>9]);   
        Menu::create(['name'=>'Customer/Supplier','alias'=>'customer','url'=>"/master/customer",'icon'=>'fas fa-master','parent_id'=>$this->findParent('master'),'created_by'=>'SYSTEM','index'=>9]); 

          // transaction
        Menu::create(['name'=>'Transaction','alias'=>'transaction','url'=>"#",'icon'=>'fas fa-master','parent_id'=>0,'created_by'=>'SYSTEM','index'=>0]); 
        Menu::create(['name'=>'Lembar Kerja','alias'=>'lembar_kerja','url'=>"/transaction/lembar-kerja",'icon'=>'fas fa-master','parent_id'=>$this->findParent('transaction'),'created_by'=>'SYSTEM','index'=>0]); 
        Menu::create(['name'=>'Adjustment','alias'=>'adjustment','url'=>"/transaction/adjustment",'icon'=>'fas fa-master','parent_id'=>$this->findParent('transaction'),'created_by'=>'SYSTEM','index'=>1]); 
        Menu::create(['name'=>'Penjualan','alias'=>'sales','url'=>"/transaction/sales",'icon'=>'fas fa-master','parent_id'=>$this->findParent('transaction'),'created_by'=>'SYSTEM','index'=>2]); 
        Menu::create(['name'=>'Pembelian','alias'=>'purchase','url'=>"/transaction/purchase",'icon'=>'fas fa-master','parent_id'=>$this->findParent('transaction'),'created_by'=>'SYSTEM','index'=>3]); 
        Menu::create(['name'=>'Retur Pembelian','alias'=>'return_purchase','url'=>"/transaction/return-purchase",'icon'=>'fas fa-master','parent_id'=>$this->findParent('transaction'),'created_by'=>'SYSTEM','index'=>4]); 
        Menu::create(['name'=>'Retur Penjualan','alias'=>'return_sales','url'=>"/transaction/return-sales",'icon'=>'fas fa-master','parent_id'=>$this->findParent('transaction'),'created_by'=>'SYSTEM','index'=>5]); 

        // report
        Menu::create(['name'=>'Report','alias'=>'report','url'=>"#",'icon'=>'fas fa-master','parent_id'=>0,'created_by'=>'SYSTEM','index'=>0]); 
        Menu::create(['name'=>'Penjualan','alias'=>'report_sales','url'=>"/report/sales",'icon'=>'fas fa-master','parent_id'=>$this->findParent('report'),'created_by'=>'SYSTEM','index'=>0]); 
        Menu::create(['name'=>'Pembelian','alias'=>'report_purchase','url'=>"/report/purchase",'icon'=>'fas fa-master','parent_id'=>$this->findParent('report'),'created_by'=>'SYSTEM','index'=>1]); 
        Menu::create(['name'=>'Adjustment','alias'=>'report_adjustment','url'=>"/report/adjustment",'icon'=>'fas fa-master','parent_id'=>$this->findParent('report'),'created_by'=>'SYSTEM','index'=>2]); 
        Menu::create(['name'=>'Stock','alias'=>'stock','url'=>"/report/stocks",'icon'=>'fas fa-master','parent_id'=>$this->findParent('report'),'created_by'=>'SYSTEM','index'=>3]); 
        Menu::create(['name'=>'Stock Opname','alias'=>'stock_opname','url'=>"/report/stock-opname",'icon'=>'fas fa-master','parent_id'=>$this->findParent('report'),'created_by'=>'SYSTEM','index'=>4]); 


    }

    private function findParent($name){
        return (Menu::whereAlias($name)->first()?Menu::whereAlias($name)->first()->id:0);
    }
}
