<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Master\Permission;
use App\Models\Master\Menu;
use DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prefixs = ['access','create','update','delete'];
        $menus = ['master','items','permission','roles','users','customer','marketplaces','warehouse','categories','models','sizes','transaction','adjustment','lembar_kerja','purchase','return_purchase','sales','return_sales','stock','stock_opname','report','menus','report_sales','report_adjustment','report_purchase'];

        foreach ($prefixs as $key => $prefix) {
            foreach ($menus as $menu) {
                if(in_array($menu, ['master','transaction','report']) && $prefix <> 'access'){
                    continue;
                }

                $permission = Permission::create([
                    'name'=>$prefix.'_menu_'.$menu,
                    'permission'=>$prefix,
                    'menu_id'=>$this->findMenu($menu),
                    'created_by'=>'SYSTEM'
                ]);

                DB::table('role_permissions')->insert([
                    'role_id'=>1,
                    'permission_id'=>$permission->id
                ]);
            }
        }
    }

    private function findMenu($alias){
        return (Menu::whereAlias($alias)->first()?Menu::whereAlias($alias)->first()->id:0);
    }
}
