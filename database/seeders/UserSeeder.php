<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Master\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //        
        $user = User::count();

        if(!$user){
            $roles = Role::create([
            'title'=>'admin',
            'created_by'=>'SYSTEM'
            ]);
            User::create([
            'name'=>'Admin Toko',
            'username'=>'adminku',
            'password'=>Hash::make('admin123'),
            'email'=>'admin@justiboy.com',
            'role_id'=>$roles->id
        ]);            
        }
    }
}
